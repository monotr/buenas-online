﻿using UnityEngine;

public static class MobileUtilities
{
    /// <summary>
    /// Returns the keyboard height ratio.
    /// </summary>
    public static float GetKeyboardHeightRatio(bool includeInput)
    {
        return Mathf.Clamp01((float)GetKeyboardHeight(includeInput) / Display.main.systemHeight);
    }

    /// <summary>
    /// Returns the keyboard height in display pixels.
    /// </summary>
    public static int GetKeyboardHeight(bool includeInput)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        using (var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            var unityPlayer = unityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer");
            var view = unityPlayer.Call<AndroidJavaObject>("getView");

            var result = 0;

            if (view != null)
            {
                using (var rect = new AndroidJavaObject("android.graphics.Rect"))
                {
                    view.Call("getWindowVisibleDisplayFrame", rect);
                    result = Display.main.systemHeight - rect.Call<int>("height");
                }

                if (includeInput)
                {
                    var dialog = unityPlayer.Get<AndroidJavaObject>("mSoftInputDialog");
                    var decorView = dialog?.Call<AndroidJavaObject>("getWindow").Call<AndroidJavaObject>("getDecorView");

                    if (decorView != null)
                    {
                        var decorHeight = decorView.Call<int>("getHeight");
                        result += decorHeight;
                    }
                }
            }

            return result;
        }
#else
            //var height = Mathf.RoundToInt(TouchScreenKeyboard.area.height);
            //return height >= Display.main.systemHeight ? 0 : height;
            return 0;
#endif
    }
}
