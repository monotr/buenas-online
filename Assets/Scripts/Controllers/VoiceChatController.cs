﻿using UnityEngine;
using UnityEngine.Android;

public class VoiceChatController : LoteriaOnlineElement
{
    private VivoxVoiceManager _vivoxVoiceManager;

    private bool PermissionsDenied;

    private void Awake()
    {
        _vivoxVoiceManager = VivoxVoiceManager.Instance;
        _vivoxVoiceManager.OnUserLoggedInEvent += OnUserLoggedIn;
        _vivoxVoiceManager.OnUserLoggedOutEvent += OnUserLoggedOut;
    }

    private void OnDestroy()
    {
        _vivoxVoiceManager.OnUserLoggedInEvent -= OnUserLoggedIn;
        _vivoxVoiceManager.OnUserLoggedOutEvent -= OnUserLoggedOut;
    }


    public void LoginToVivoxService(string displayName, string fbId)
    {
        if (Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            // The user authorized use of the microphone.
            LoginToVivox(displayName, fbId);
        }
        else
        {
            // Check if the users has already denied permissions
            if (PermissionsDenied)
            {
                PermissionsDenied = false;
                LoginToVivox(displayName, fbId);
            }
            else
            {
                PermissionsDenied = true;
                // We do not have permission to use the microphone.
                // Ask for permission or proceed without the functionality enabled.
                Permission.RequestUserPermission(Permission.Microphone);
            }
        }
    }

    private void LoginToVivox(string displayName, string fbId)
    {
        //LoginButton.interactable = false;

        if (string.IsNullOrEmpty(displayName))
        {
            Debug.LogError("Please enter a display name.");
            return;
        }
        _vivoxVoiceManager.Login(displayName, fbId);
    }

    public void LogoutOfVivoxService()
    {
        _vivoxVoiceManager.DisconnectAllChannels();
        _vivoxVoiceManager.Logout();
    }

    #region Vivox Callbacks

    private void OnUserLoggedIn()
    {
        //HideLoginUI();
        print("Logged in");

        // Init global chat
        if(app == null)
            app = FindObjectOfType<LoteriaOnlineApplication>();

        // Global chat
        app.globalChatComponent.Init();
        // GameNotifications
        app.gameNotificationsComponent.Init();
        // Start friends load
        app.friendsComponent.Init();
    }

    private void OnUserLoggedOut()
    {
        //ShowLoginUI();
        print("Logged out");
    }

    #endregion
}
