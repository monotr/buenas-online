﻿using Firebase.Database;
using Firebase.Storage;
using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ProfileController : LoteriaOnlineElement
{
    internal bool isOtherProfile;
    internal Globals.ProfileOrigin profileOrigin;

    [Header("Profile pic")]
    public Image profileImage;
    public Button uploadPhotoButton;
    public GameObject deletePhotoButton;
    public Sprite defaultProfileSprite;
    public GameObject uploadingImagePanel;
    RectTransform spinnerRecT;
    int MAX_IMAGE_SIZE = 1024;

    [Header("Player nickname")]
    public TMP_Text playerNicknameText;
    public TMP_InputField nicknameInput;
    TMP_InputField current_nicknameInput;
    public Button changeNameButton;
    public GameObject editNicknameButton;

    [Header("Level Experience")]
    public TMP_Text levelText;
    public Slider experienceSlider;
    public Image experienceSliderFillImage;

    [Header("Friend buttons")]
    public GameObject sendCoinsButton;
    public GameObject addFriendButton;
    public GameObject waitingFriendButton;
    public GameObject removeFriendButton;
    public GameObject sendMessageButton;

    [Header("Player stats")]
    public GameObject playedGamesObj;
    public GameObject wonGamesObj;
    //public TMP_Text lostGamesText;
    //public TMP_Text abandonedGamesText;
    public GameObject totalCoinsEarnedObj;
    public GameObject maxStreakObj;
    public GameObject hostTimeObj;

    [Header("Others")]
    public GameObject infoButton;
    public GameObject closeSessionButton;

    public class PlayerStatPanel
    {
        public TMP_Text statValueText;
        public GameObject rankingMedalObject;
        public TMP_Text rankingMedalPlaceText;

        public PlayerStatPanel(GameObject obj)
        {
            statValueText = obj.transform.GetChild(1).GetComponent<TMP_Text>();
            rankingMedalObject = obj.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
            rankingMedalPlaceText = rankingMedalObject.GetComponentInChildren<TMP_Text>();

            rankingMedalObject.SetActive(false);
        }
    }


    private void Start()
    {
        changeNameButton.onClick.AddListener(delegate { OnChangeName(nicknameInput); });

        spinnerRecT = uploadingImagePanel.transform.GetChild(0).GetComponent<RectTransform>();

        sendCoinsButton.SetActive(false);
        addFriendButton.SetActive(false);
        waitingFriendButton.SetActive(false);
        sendMessageButton.SetActive(false);
    }

    private void Update()
    {
        if (app.mainController.profileWindow.activeSelf && !isOtherProfile)
        {
            changeNameButton.interactable = nicknameInput.text != app.mainController.myPlayer.name;

            if (uploadingImagePanel.activeSelf)
            {
                spinnerRecT.Rotate(Vector3.forward, Time.deltaTime * 180);
            }
        }
    }

    public void OpenProfileWindow()
    {
        isOtherProfile = false;

        // Show loading
        app.mainController.ShowLoading("Obteniedo tu información!");

        // Show profile window
        uploadingImagePanel.SetActive(false);
        app.mainController.profileWindow.SetActive(true);

        // photo buttons
        uploadPhotoButton.gameObject.SetActive(true);
        // photo
        app.profileController.profileImage.sprite = app.mainController.playerInfo_photoImage.sprite;

        // nickname - button
        editNicknameButton.SetActive(true);
        nicknameInput.gameObject.SetActive(false);
        changeNameButton.gameObject.SetActive(false);
        playerNicknameText.gameObject.SetActive(true);

        // friend buttons
        sendCoinsButton.SetActive(false);
        addFriendButton.SetActive(false);
        waitingFriendButton.SetActive(false);
        removeFriendButton.SetActive(false);
        sendMessageButton.SetActive(false);

        //other buttons
        infoButton.SetActive(true);
        closeSessionButton.SetActive(true);

        // Fill stats
        StartCoroutine(OnUpdatePlayerStats(null, null));

        // Disable upload button if last upload was less than 24 hours
        uploadPhotoButton.interactable = string.IsNullOrEmpty(app.mainController.myPlayer.lastPhotoUploadDateTime) ? true :
            (DateTime.Now - DateTime.Parse(app.mainController.myPlayer.lastPhotoUploadDateTime)).TotalMinutes > 24 * 60;

        // Show/hide delete photo button
        deletePhotoButton.SetActive(!string.IsNullOrEmpty(app.mainController.myPlayer.profileImageUrl));
    }

    public void CloseProfileWindow()
    {
        app.mainController.profileWindow.SetActive(false);

        if (isOtherProfile)
        {
            switch (profileOrigin)
            {
                case Globals.ProfileOrigin.GlobalChat:
                    app.globalChatComponent.globalChatWindow.SetActive(true);
                break;
                case Globals.ProfileOrigin.FriendsPanel:
                    app.friendsComponent.OnOpenFriendWindow(app.friendsComponent.isFromTable);
                    break;
                case Globals.ProfileOrigin.Lobby:

                    break;
                case Globals.ProfileOrigin.Rankings:

                    break;
                case Globals.ProfileOrigin.WinnerPanel:

                    break;
            }
        }
        else
            app.mainController.StartCoroutine(app.mainController.ChangeWindow());
    }

    
    public void OnEditName()
    {
        nicknameInput.gameObject.SetActive(true);
        nicknameInput.Select();
        changeNameButton.gameObject.SetActive(true);

        playerNicknameText.gameObject.SetActive(false);
        editNicknameButton.SetActive(false);
    }
    
    public void OnChangeName(TMP_InputField _nicknameInput)
    {
        current_nicknameInput = _nicknameInput;

        // Check name for bad words
        if(current_nicknameInput.text != app.globalChatComponent.BadWordValidator(current_nicknameInput.text))
        {
            app.promptComponent.ShowPrompt("ADVERTENCIA", "Se detectó lenguaje innapropiado en tu solicitud de apodo.", Globals.PromptType.ConfirmMessage,
                null, Globals.PromptColors.Red);
            return;
        }
        //Check if have "bot_" in name
        if(current_nicknameInput.text.Length >= 4)
        {
            if (current_nicknameInput.text.Substring(0, 4).Contains("bot_"))
            {
                app.promptComponent.ShowPrompt("Oops", "No se puede iniciar tu nombre con \"bot_\".", Globals.PromptType.ConfirmMessage,
                    null, Globals.PromptColors.Red);
                return;
            }
        }

        app.friendsComponent.OnSearchNameFOrChange();
    }

    public IEnumerator ChangeName()
    {
        var userName_ref = FirebaseDatabase.DefaultInstance.RootReference.Child("Players").Child(app.mainController.myPlayer.id);

        Dictionary<string, object> updatedPlayer = new Dictionary<string, object>();
        updatedPlayer["/name"] = current_nicknameInput.text;

        userName_ref.UpdateChildrenAsync(updatedPlayer).ContinueWith(t =>
        {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Player name changed successfully.");
                UnityMainThreadDispatcher.Instance().Enqueue(FinishNameChange());
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(
                    app.promptComponent.ShowPrompt_Routine("Error", "Hubo un error al cambiar el nombre.\nIntentelo de nuevo",
                    Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow));
            }
        });

        yield return null;
    }

    IEnumerator FinishNameChange()
    {
        yield return null;

        app.mainController.loadingPanel.SetActive(false);

        app.mainController.myPlayer.name = nicknameInput.text;

        app.mainController.playerInfo_nameText.text = nicknameInput.text;

        playerNicknameText.text = nicknameInput.text;

        nicknameInput.gameObject.SetActive(false);
        changeNameButton.gameObject.SetActive(false);

        playerNicknameText.gameObject.SetActive(true);
        editNicknameButton.SetActive(true);

        UnityMainThreadDispatcher.Instance().Enqueue(
            app.promptComponent.ShowPrompt_Routine("¡Éxito!", "Se ha cambiado su nombre exitosamente", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Green));
    }

    public void PickImage()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            UnityMainThreadDispatcher.Instance().Enqueue(ValidateSelectedPhoto(path));
            
        }, "Selecciona una imagen para tu perfil", "image/*");
        Debug.Log("Permission result: " + permission);
#else
        // Open file with filter
        var extensions = new[] {
            new ExtensionFilter("Archivos de Imagen", "png", "jpg", "jpeg" )
        };
        var paths = StandaloneFileBrowser.OpenFilePanel("Selecciona una imagen para tu perfil", "", extensions, false);
        if(paths.Length > 0)
            StartCoroutine(ValidateSelectedPhoto(paths[0]));
#endif
    }

    IEnumerator ValidateSelectedPhoto(string path)
    {
        uploadingImagePanel.SetActive(true);

        yield return null;

        if (path != null)
        {
            try
            {
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, MAX_IMAGE_SIZE);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("0"));
                }
                else
                {
                    // Moderate image
                    UploadPhoto(app.mainController.myPlayer.id, path, texture);
                }
            }
            catch(Exception e)
            {
            }
        }
        else
        {
        }
    }

    void UploadPhoto(string userId, string imagePath, Texture2D texture)
    {
        Firebase.Storage.FirebaseStorage storage = Firebase.Storage.FirebaseStorage.DefaultInstance;

        Firebase.Storage.StorageReference storage_ref = storage.GetReferenceFromUrl("gs://loteriamexicanaonline.appspot.com").Child("ProfileImages");

        // Create a reference
        Firebase.Storage.StorageReference userPhoto_ref = storage_ref.Child(userId + ".jpg");

        // Upload the file
        userPhoto_ref.PutFileAsync("file://" + imagePath).ContinueWith((Task<StorageMetadata> task) => {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.Log(task.Exception.ToString());
                UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("1"));
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(GetUploadedPhotoUrl(userPhoto_ref, texture));
            }
        });
    }

    IEnumerator GetUploadedPhotoUrl(Firebase.Storage.StorageReference userPhoto_ref, Texture2D texture)
    {
        userPhoto_ref.GetDownloadUrlAsync().ContinueWith(t => {
            if (t.IsFaulted || t.IsCanceled)
            {
                Debug.Log(t.Exception.ToString());
                UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("2"));
            }
            else
            {
                print(t.Result);
                UnityMainThreadDispatcher.Instance().Enqueue(ModerateNsfwImage(UnityWebRequest.EscapeURL(t.Result.ToString()), userPhoto_ref, texture));
            }
        });
        yield return null;
    }

    IEnumerator ModerateNsfwImage(string imageUrl, Firebase.Storage.StorageReference userPhoto_ref, Texture2D texture)
    {
        string models = "models=nudity,offensive,scam";
        string api = "api_user=366197926&api_secret=S9PxVwUasGSxkq5g8cY7";
        string url = "url=" + imageUrl;

        UnityWebRequest www = UnityWebRequest.Get("https://api.sightengine.com/1.0/check.json?"+models+"&"+api+"&"+url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("3"));
        }
        else
        {
            print(www.downloadHandler.text);

            Globals.SightengineResponse _res = JsonUtility.FromJson<Globals.SightengineResponse>(www.downloadHandler.text);
            Debug.Log("Form upload complete!");

            if(_res.status.Contains("success"))
            {
                if(_res.nudity.safe > 0.5f & _res.offensive.prob < 0.5f && _res.scam.prob < 0.5f)
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(SavePlayerPhotoUrl(imageUrl, texture));
                }
                else
                {
                    profileImage.sprite = defaultProfileSprite;
                    UnityMainThreadDispatcher.Instance().Enqueue(DeletePhoto(userPhoto_ref));
                }
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("4"));
            }
        }
    }

    IEnumerator SavePlayerPhotoUrl(string _photoUrl, Texture2D texture)
    {
        yield return null;

        var userPhoto_ref = FirebaseDatabase.DefaultInstance.RootReference.Child("Players").Child(app.loginController.fbUser.UserId).Child("profileImageUrl");
        print(_photoUrl);
        string _ext = Path.GetExtension(_photoUrl);
        print(_ext);
        string complete = _photoUrl.Replace(_ext, "") + "_200x200" + _ext;
        print(complete);
        userPhoto_ref.SetValueAsync(UnityWebRequest.UnEscapeURL(complete)).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                UnityMainThreadDispatcher.Instance().Enqueue(SetNewPhoto(texture));
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("7"));
            }
        });
        //Set photo edit time
        StartCoroutine(SetPhotoLastEditTime());
    }

    IEnumerator SetPhotoLastEditTime()
    {
        yield return null;
        // Last photo upload
        app.mainController.myPlayer.lastPhotoUploadDateTime = DateTime.Now.ToString();
        var userPhoto_ref = FirebaseDatabase.DefaultInstance.RootReference.Child("Players").Child(app.loginController.fbUser.UserId).Child("lastPhotoUploadDateTime");
        userPhoto_ref.SetValueAsync(app.mainController.myPlayer.lastPhotoUploadDateTime).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                uploadPhotoButton.interactable = false;
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("7"));
            }
        });
    }

    public IEnumerator SetNewPhoto(Texture2D texture)
    {
        yield return null;

        Sprite mySprite;
        mySprite = texture == null ? null : Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        profileImage.sprite = mySprite;
        profileImage.preserveAspect = true;

        app.mainController.playerInfo_photoImage.sprite = mySprite;
        app.mainController.playerInfo_photoImage.preserveAspect = true;

        uploadingImagePanel.SetActive(false);

        deletePhotoButton.SetActive(true);

        print("New photo set!");
    }

    IEnumerator DeletePhoto(Firebase.Storage.StorageReference userPhoto_ref)
    {
        yield return null;

        userPhoto_ref.DeleteAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("File deleted successfully.");
                UnityMainThreadDispatcher.Instance().Enqueue(UpdateWarnings());
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("5"));
            }
        });
    }

    public void OnRemoveAvatarPhoto()
    {
        app.promptComponent.ShowPrompt("Remover foto de perfíl", "¿Desea borrar la foto? Se mostrará una imagen por defecto",
            Globals.PromptType.YesNoMessage, "RemoveAvatarPhoto", Globals.PromptColors.Yellow);
    }

    public void RemoveAvatarPhoto()
    {
        // Remove phot url from user
        var userName_ref = FirebaseDatabase.DefaultInstance.RootReference.Child("Players").Child(app.loginController.fbUser.UserId).Child("profileImageUrl");
        userName_ref.SetValueAsync(string.Empty).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Player avatar removed successfully.");
                UnityMainThreadDispatcher.Instance().Enqueue(ResetPhotoToDefault());
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(
                    app.promptComponent.ShowPrompt_Routine("Error", "Hubo un error al remover la foto.\nIntentelo de nuevo",
                    Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow));
            }
        });
    }

    IEnumerator ResetPhotoToDefault()
    {
        yield return null;

        // Set default avatar pic
        StartCoroutine(SetNewPhoto(app.tools.TextureToTexture2D(app.privateRoomLobbyComponent.defaultAvatar)));
        deletePhotoButton.SetActive(false);
    }

    IEnumerator UpdateWarnings()
    {
        yield return null;

        var userPhoto_ref = FirebaseDatabase.DefaultInstance.RootReference.Child("Players").Child(app.loginController.fbUser.UserId).Child("warnings");
        app.mainController.myPlayer.warnings++;
        userPhoto_ref.SetValueAsync(app.mainController.myPlayer.warnings).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Player warnings updated successfully.");
                UnityMainThreadDispatcher.Instance().Enqueue(ShowWarning());
            }
            else
            {
                UnityMainThreadDispatcher.Instance().Enqueue(ShowPhotoUploadError("6"));
            }
        });
    }

    IEnumerator ShowWarning()
    {
        app.promptComponent.ShowPrompt("¡ADVERTENCIA!",
                    "Se detectó contenido innapropiedo en la imagen seleccionada.\nEsta es tu advertencia # " + app.mainController.myPlayer.warnings,
                    Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Red);

        SetPhotoLastEditTime();

        StartCoroutine(SavePlayerPhotoUrl(string.Empty, null));

        yield return null;
    }

    IEnumerator ShowPhotoUploadError(string errorId)
    {
        yield return null;

        app.promptComponent.ShowPrompt("Error", "Error al subir la imagen (x0" + errorId + ")", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
        uploadingImagePanel.SetActive(false);
    }

    public void OnLogOut()
    {
        app.promptComponent.ShowPrompt("Cerrar sesión", "¿Estás segure de querer cerrar la sesión?", Globals.PromptType.YesNoMessage, "LogOut", Globals.PromptColors.Yellow);
    }

    #region EXPERIENCE
    public int UpdateExperience(float experience)
    {
        int lvlsUp = 0;
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        app.mainController.myPlayer.experience += experience;
        childUpdates["/experience"] = (float)app.mainController.myPlayer.experience;

        //show experience notification
        app.notificationsController.StartCoroutine(
            app.notificationsController.ReceiveNotificationRoutine(Globals.NotificationType.Experience, null, experience.ToString(), null));

        float nextLvl = 0, nextLvlExpNeeded = 0;
        bool isCheckingLevelIncrease = true;
        while (isCheckingLevelIncrease)
        {
            for (int i = 0; i < app.mainController.myPlayer.level + 1; i++)
            {
                nextLvl = app.tools.NextLevel(i);
                nextLvlExpNeeded += nextLvl;
            }

            if (app.mainController.myPlayer.experience >= nextLvlExpNeeded)
            {
                lvlsUp++;
                app.mainController.myPlayer.level++;
                childUpdates["/level"] = (int)app.mainController.myPlayer.level;

                //show level up notification
                app.notificationsController.StartCoroutine(
                    app.notificationsController.ReceiveNotificationRoutine(Globals.NotificationType.LevelUp, null, "Nivel " + app.mainController.myPlayer.level, null));
            }
            else
            {
                isCheckingLevelIncrease = false;
            }
        }

        var player_ref = FirebaseDatabase.DefaultInstance.GetReference("Players/" + app.mainController.myPlayer.id);
        player_ref.UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Experience/Level updated");
                }
            });

        FillExperienceUI();

        return lvlsUp;
    }

    public void FillExperienceUI()
    {
        float lastLvl = 0;
        for (int i = 0; i < app.mainController.myPlayer.level; i++)
            lastLvl += app.tools.NextLevel(i);
        float nextLvl = lastLvl + app.tools.NextLevel(app.mainController.myPlayer.level);
        //float expNeeded = app.tools.NextLevel(app.mainController.myPlayer.level);
        
        float sliderVal = (app.mainController.myPlayer.experience - lastLvl) / (nextLvl - lastLvl);
        sliderVal = Double.IsNaN(sliderVal) ? 0 : sliderVal;
        app.mainController.playerInfo_levelSlider.value = sliderVal;
        //print("LVL SLIDER VAL: " + app.mainController.playerInfo_levelSlider.value);

        // Slider color
        float _grad = app.mainController.myPlayer.level / 100;
        int gradientId = _grad > .5f ? 1 : 0;
        float gradientVal = _grad > .5f ? (_grad - .5f) * 2f : 1f - (_grad * 2f);
        app.mainController.playerInfo_levelSliderFillImage.color = app.privateRoomLobbyComponent.gradients[gradientId].Evaluate(gradientVal);

        //print("current: " + app.mainController.myPlayer.experience + "  -  nextLvl: " + nextLvl + "  -  nextLvl: " + nextLvl + " %: " +
        //app.mainController.playerInfo_levelSlider.value.ToString("f3"));

        app.mainController.playerInfo_levelText.text = "Nivel " + app.mainController.myPlayer.level;
    }
    #endregion

    #region PLAYER STATS
    public IEnumerator OnUpdatePlayerStats(Globals.PlayerStats newPlayerStats, string checkIfIncreased)
    {
        yield return null;

        // Name
        nicknameInput.text = app.mainController.myPlayer.name;
        playerNicknameText.text = app.mainController.myPlayer.name;

        DatabaseReference _playerStats_ref = FirebaseDatabase.DefaultInstance.GetReference("PlayerStats/" + app.mainController.myPlayer.id);

        _playerStats_ref.GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                string json = snapshot.GetRawJsonValue();
                //print(json);

                Globals.PlayerStats oldPlayerStats = JsonUtility.FromJson<Globals.PlayerStats>(json);
                if (oldPlayerStats == null) oldPlayerStats = new Globals.PlayerStats();

                if (newPlayerStats == null)
                    UnityMainThreadDispatcher.Instance().Enqueue(FillProfilePlayerStats(oldPlayerStats));
                else
                {
                    if(string.IsNullOrEmpty(checkIfIncreased))
                        UnityMainThreadDispatcher.Instance().Enqueue(UpdatePlayerStats(_playerStats_ref, oldPlayerStats, newPlayerStats));
                    else
                    {
                        switch (checkIfIncreased)
                        {
                            case "Streak":
                                if(oldPlayerStats.maxStreak < newPlayerStats.maxStreak)
                                {
                                    UnityMainThreadDispatcher.Instance().Enqueue(UpdatePlayerStats(_playerStats_ref, oldPlayerStats, newPlayerStats));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        });
    }

    public IEnumerator FillProfilePlayerStats(Globals.PlayerStats playerStats)
    {
        yield return null;

        // Level
        levelText.text = "Nivel " + playerStats.level;
        // Experience
        float lastLvl = 0;
        for (int i = 0; i < playerStats.level; i++)
            lastLvl += app.tools.NextLevel(i);
        float nextLvl = lastLvl + app.tools.NextLevel(playerStats.level);
        experienceSlider.value = (playerStats.totalExperience - lastLvl) / (nextLvl - lastLvl);
        // Exp Slider color
        float _grad = playerStats.level / 100;
        int gradientId = _grad > .5f ? 1 : 0;
        float gradientVal = _grad > .5f ? (_grad - .5f) * 2f : 1f - (_grad * 2f);
        experienceSliderFillImage.color = app.privateRoomLobbyComponent.gradients[gradientId].Evaluate(gradientVal);

        // Stats panels
        PlayerStatPanel _psp = new PlayerStatPanel(playedGamesObj);
        _psp.statValueText.text = playerStats.playedGames.ToString();

        _psp = new PlayerStatPanel(wonGamesObj);
        _psp.statValueText.text = playerStats.wonGames.ToString();

        _psp = new PlayerStatPanel(totalCoinsEarnedObj);
        _psp.statValueText.text = playerStats.totalCoinsEarned.ToString();

        _psp = new PlayerStatPanel(maxStreakObj);
        _psp.statValueText.text = playerStats.maxStreak.ToString();

        TimeSpan t = TimeSpan.FromSeconds(playerStats.hostTime);
        string hostTime = string.Format("{0:D2}h:{1:D2}m:{2:D2}s",
                        t.Hours,
                        t.Minutes,
                        t.Seconds,
                        t.Milliseconds);
        _psp = new PlayerStatPanel(hostTimeObj);
        _psp.statValueText.text = hostTime;

        app.mainController.loadingPanel.SetActive(false);
    }

    IEnumerator UpdatePlayerStats(DatabaseReference _playerStats_ref, Globals.PlayerStats oldPlayerStats, Globals.PlayerStats newPlayerStats)
    {
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();

        if (newPlayerStats.abandonedGame > 0)
            childUpdates["/abandonedGame"] = oldPlayerStats.abandonedGame + newPlayerStats.abandonedGame;
        if (newPlayerStats.hostTime > 0)
            childUpdates["/hostTime"] = oldPlayerStats.hostTime + newPlayerStats.hostTime;
        if (newPlayerStats.level > 0)
            childUpdates["/level"] = oldPlayerStats.level + newPlayerStats.level;
        if (newPlayerStats.maxStreak > 0)
            childUpdates["/maxStreak"] = oldPlayerStats.maxStreak + newPlayerStats.maxStreak;
        if (newPlayerStats.playedGames > 0)
            childUpdates["/playedGames"] = oldPlayerStats.playedGames + newPlayerStats.playedGames;
        if (newPlayerStats.totalCoinsEarned > 0)
            childUpdates["/totalCoinsEarned"] = oldPlayerStats.totalCoinsEarned + newPlayerStats.totalCoinsEarned;
        if (newPlayerStats.totalExperience > 0)
            childUpdates["/totalExperience"] = oldPlayerStats.totalExperience + newPlayerStats.totalExperience;
        if (newPlayerStats.wonGames > 0)
            childUpdates["/wonGames"] = oldPlayerStats.wonGames + newPlayerStats.wonGames;
        if (newPlayerStats.maxStreak > 0)
            childUpdates["/maxStreak"] = newPlayerStats.maxStreak;

        _playerStats_ref.UpdateChildrenAsync(childUpdates).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                //Debug.Log("Completed! - Player Stats updated");
            }
        });

        yield return null;
    }
    #endregion

    public void OnSendFriendRequest(int _type, string _originId, string _destinyId, string _content)
    {
        print(_type + " " + _originId + " " + _destinyId + " " + _content);
        //Send invitation
        var _friendRequests_ref = FirebaseDatabase.DefaultInstance.RootReference.Child("FriendRequests");
        var key = _friendRequests_ref.Push().Key;

        Dictionary<string, object> newFriend = new Dictionary<string, object>();
        newFriend["/type"] = _type == 4 ? 1 : _type;
        newFriend["/originId"] = _originId;
        newFriend["/destinyId"] = _destinyId;
        newFriend["/content"] = _content;
        newFriend["/timestamp"] = ServerValue.Timestamp;

        _friendRequests_ref.Child(key).UpdateChildrenAsync(newFriend).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Friend request sent!");
                switch (_type)
                {
                    case 0: // Add friend
                        UnityMainThreadDispatcher.Instance().Enqueue(FinishAddFriend(key, _destinyId));
                        break;
                    case 1: // Friend request remove
                        UnityMainThreadDispatcher.Instance().Enqueue(app.globalChatComponent.FinishRemoveFriendRequest(_content));
                        break;
                    case 2: // Confirm friend request
                        break;
                    case 4: // Remove friend
                        UnityMainThreadDispatcher.Instance().Enqueue(FinishRemoveFriend(key));
                        break;
                    default:
                        break;
                }
                
            }
        });
    }

    IEnumerator FinishAddFriend(string key, string _playerId)
    {
        yield return null;

        waitingFriendButton.SetActive(true);
    }

    public void OnRemoveFriend(string _playerId)
    {
        removeFriendButton.SetActive(false);
        sendCoinsButton.SetActive(false);

        var fbKey = app.mainController.myPlayer.friendList.First(x => ((Globals.Friend)x.Value).playerId == _playerId).Key;
        app.profileController.OnSendFriendRequest(4, app.mainController.myPlayer.id, _playerId, fbKey);
    }

    IEnumerator FinishRemoveFriend(string key)
    {
        yield return null;

        if(app.mainController.myPlayer.friendList.ContainsKey(key))
            app.mainController.myPlayer.friendList.Remove(key);

        addFriendButton.SetActive(true);
    }
}
