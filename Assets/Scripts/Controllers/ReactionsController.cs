﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ReactionsController : LoteriaOnlineElement
{
    public GameObject reactionsButton;

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    bool isPressed = false;
    float pressedTime = 0;
    const float PRESS_TIMER = 0.375f;

    public RectTransform buttonRectT;
    Vector2 buttonInitSize = Vector2.one * 125;
    Vector2 buttonFinalSize = Vector2.one * 150;

    public GameObject emojeesPanel;
    float selectionTime = 0;
    const float SELECTION_TIMER = 0.125f;
    public GameObject[] emojeeButtons; // 0 - angry | 1 - sad | 2 - goodLuck | 3 - happy
    RectTransform selectedEmojee;
    Vector2 emojeeInitSize = Vector2.one * 80;

    DatabaseReference _emojees_ref;

    private void Start()
    {
        reactionsButton.SetActive(false);
        m_Raycaster = FindObjectOfType<Canvas>().GetComponent<GraphicRaycaster>();
        m_EventSystem = FindObjectOfType<Canvas>().GetComponent<EventSystem>();
    }

    public void InitReactions()
    {
        reactionsButton.SetActive(true);

        _emojees_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/emojees");
        _emojees_ref.ChildAdded -= Handle_Emojees_ChildAdded;
        _emojees_ref.ChildAdded += Handle_Emojees_ChildAdded;
    }

    public void EndReactions()
    {
        reactionsButton.SetActive(false);

        _emojees_ref.ChildAdded -= Handle_Emojees_ChildAdded;
        _emojees_ref = null;
    }

    void Handle_Emojees_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());
        Globals.Emojee emojee = JsonUtility.FromJson<Globals.Emojee>(args.Snapshot.GetRawJsonValue());
        //print(emojee.playerId);
        app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
            Globals.NotificationType.Emojee,
            emojeeButtons[emojee.emojeeIndex].GetComponent<Image>().sprite.texture,
            string.Empty,
            emojee.playerId
        ));
    }

    private void Update()
    {
        if (app.mainController.gameState == MainController.GameState.InLobby || app.mainController.gameState == MainController.GameState.Playing)
        {
            m_PointerEventData = new PointerEventData(m_EventSystem);

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Touch touch = Input.GetTouch(i);
                    TouchLogic(touch.position, touch.phase == TouchPhase.Began, touch.phase == TouchPhase.Ended);
                }
            }
#else
            TouchLogic(Input.mousePosition, Input.GetMouseButtonDown(0), Input.GetMouseButtonUp(0));
#endif
        }
    }

    void TouchLogic(Vector2 position, bool isDown, bool isUp)
    {
        if (!isPressed && isDown)
        {
            isPressed = true;
        }

        if (isPressed && isUp)
        {
            if(selectedEmojee != null)
            {
                print("EMOJEE: " + selectedEmojee.gameObject.name);

                StartCoroutine(SendEmojee(emojeeButtons.ToList().IndexOf(selectedEmojee.gameObject)));
            }

            ResetPress();
            return;
        }

        if(isPressed)
        {
            m_PointerEventData.position = position;

            List<RaycastResult> results = new List<RaycastResult>();

            m_Raycaster.Raycast(m_PointerEventData, results);

            if (results.Count == 0) return;

            RaycastResult result = results[0];
            //Debug.Log("Hit " + result.gameObject.tag);

            if (result.gameObject.tag == "EmojeePressButton")
            {
                if(selectedEmojee != null)
                {
                    selectedEmojee.sizeDelta = emojeeInitSize;
                    selectedEmojee = null;
                    selectionTime = 0;
                }

                pressedTime += Time.deltaTime;

                buttonRectT.sizeDelta = Vector2.Lerp(buttonInitSize, buttonFinalSize, pressedTime / PRESS_TIMER);

                if (pressedTime > PRESS_TIMER && !emojeesPanel.activeSelf)
                {
                    emojeesPanel.SetActive(true);
                    buttonRectT.sizeDelta = buttonFinalSize;
                }
            }
            else if (result.gameObject.tag == "EmojeeButton")
            {
                if(selectedEmojee == null)
                {
                    selectedEmojee = result.gameObject.GetComponent<RectTransform>();
                    selectionTime = 0;
                }

                selectionTime += Time.deltaTime;

                selectedEmojee.sizeDelta = Vector2.Lerp(emojeeInitSize, emojeeInitSize * 1.25f, selectionTime / SELECTION_TIMER);
            }
            else
            {
                ResetPress();
            }
        }
    }

    IEnumerator SendEmojee(int emojeeIndex)
    {
        string key = _emojees_ref.Push().Key;

        Dictionary<string, object> childUpdates = new Dictionary<string, object>();

        childUpdates["/playerId"] = app.mainController.myPlayer.id;
        childUpdates["/emojeeIndex"] = emojeeIndex;

        _emojees_ref.Child(key).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Emojee sent");
            }
        });

        yield return new WaitForSeconds(1f);

        _emojees_ref.Child(key).RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Emojee removed");
                //UnityMainThreadDispatcher.Instance().Enqueue(LeavePrivateRoom());
            }
        });
    }

    void ResetPress()
    {
        isPressed = false;
        emojeesPanel.SetActive(false);
        pressedTime = 0;
        buttonRectT.sizeDelta = buttonInitSize;

        selectedEmojee = null;
        selectionTime = 0;
    }
}
