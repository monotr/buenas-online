﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameController : LoteriaOnlineElement
{
    const int NEXT_GAME_TIMER = 15;
    internal float inGameLastTouchTime;

    [Header("Tables")]
    public GameObject tablePrefab;
    public Transform tableUpContainer;
    public Transform tableDownContainer;
    internal Dictionary<GameObject, Game_Table> myTablesDict;
    public GameObject spectatorModePanel;

    public class Game_Table
    {
        public string firebaseId;
        public Image backgroundImage;
        public List<Image> panelsImages;
        public Globals.Table myTable;
        public List<int> cardsList;
        public RectTransform myRectT;
        public List<GameObject> disabledPanels;
        public bool isSelected;

        public Game_Table(GameObject obj)
        {

            isSelected = false;
            // Get RectT and background
            myRectT = obj.GetComponent<RectTransform>();
            backgroundImage = obj.GetComponent<Image>();

            // Get table image panels
            disabledPanels = new List<GameObject>();
            panelsImages = new List<Image>();
            foreach (Transform row in obj.transform)
            {
                foreach (Transform col in row)
                {
                    panelsImages.Add(col.GetChild(0).gameObject.GetComponentInChildren<Image>());

                    disabledPanels.Add(col.GetChild(1).gameObject);
                    col.GetChild(1).gameObject.SetActive(false);

                    // draggable
                    col.gameObject.GetComponentInChildren<DraggableCardComponent>().enabled = false;
                }
            }

            cardsList = new List<int>();
        }

        public void ResetTable()
        {
            foreach (var panel in disabledPanels)
            {
                panel.gameObject.SetActive(false);
            }
        }
    }

    [Header("Chips")]
    public GameObject chipPrefab;
    public GameObject chipParentPrefab;
    public Transform chipParentsContainer;
    public List<Transform> chipsParents;
    
    internal int currentTablesPage;

    public class TablePageButton
    {
        public Image backgroundImage;
        public Image colorIndicatorImage;
        public RectTransform missedPanelRectT;
        public TMP_Text missedText;
        RectTransform myRectT;
        // Background
        // MAX SIZE = 125 -- MIN SIZE = 59.6875
        // MissedPanel
        // MAX SIZE = 80 -- MIN SIZE = 38

        public TablePageButton(GameObject obj)
        {
            myRectT = obj.GetComponent<RectTransform>();

            backgroundImage = obj.transform.GetChild(0).GetComponent<Image>();
            colorIndicatorImage = obj.transform.GetChild(1).GetComponent<Image>();
            missedPanelRectT = obj.transform.GetChild(2).GetComponent<RectTransform>();
            missedText = missedPanelRectT.GetComponentInChildren<TMP_Text>();

            HideMissedCardIndicator();
        }

        public void ShowMissedCardIndicator(int qty)
        {
            if(qty > 0)
            {
                if (!missedPanelRectT.gameObject.activeSelf)
                {
                    missedPanelRectT.gameObject.SetActive(true);
                }
            }
            else
            {
                HideMissedCardIndicator();
            }
            missedText.text = qty.ToString();
        }

        public void HideMissedCardIndicator()
        {
            missedPanelRectT.gameObject.SetActive(false);
        }

        public void FitMissedPanel(int buttonsCount)
        {
            var x = (myRectT.sizeDelta.x - 59.6875f) / (125 - 59.6875f);
            x = (x * (80 - 38)) + 38;
            missedPanelRectT.sizeDelta = new Vector2(x, x);

            missedPanelRectT.localPosition = Vector3.zero;
        }

        public void ShowMissedPanel()
        {
            missedPanelRectT.localPosition = Vector3.up * missedPanelRectT.sizeDelta.y * 1.33f;
        }
    }
    internal List<TablePageButton> tablePageButtonList;

    [Header("Called cards")]
    public GameObject calledCardPrefab;
    public Transform[] calledCardGrids;
    public Image lastCalledCardImage;
    List<int> callableCards;
    internal List<int> calledCards;
    public TMP_Text calledCardsCounterText;
    internal bool gameInitialized = false;
    KeyValuePair<int, int> lastWinningTable; // max matches - count of tables with max matches

    [Header("Rascada")]
    public GameObject rascadaObj;
    public GameObject[] rascadaParts;
    Coroutine fadeRascadaRoutine;
    int lastRequieredCardTapped = -1;
    float rascadaTapTimer = 1f;
    int timesTapped = 0;
    float firstTapTime;

    internal DatabaseReference _playersTables_ref;

    internal DatabaseReference _room_buenas_ref;
    DatabaseReference _calledCards_ref;
    DatabaseReference _player_table_ref;

    DatabaseReference _room_ref;
    DatabaseReference _player_room_ref;

    DatabaseReference _rascadas_ref;

    Coroutine cardCallerRoutine;

    [Header("Prize and Rank")]
    public TMP_Text prizeText;
    public TMP_Text accumulatedText;
    public GameObject[] rankPanels;
    public TMP_Text[] playersCounts;
    internal Dictionary<string, long> inGameDisconnectedPlayers;
    internal Globals.Buenas buenas;

    public class RankPanel
    {
        public Image backgroundImage;
        public RawImage playerImage;
        private List<Image> tablePanels;
        private GameObject panel;
        
        public RankPanel(GameObject obj)
        {
            panel = obj;

            backgroundImage = panel.GetComponent<Image>();

            playerImage = obj.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetComponent<RawImage>();

            tablePanels = new List<Image>();
            foreach (Transform row in obj.transform.GetChild(0).GetChild(1))
            {
                foreach (Transform column in row)
                {
                    tablePanels.Add(column.GetComponentInChildren<Image>());
                }
            }

            Hide();
        }

        public void PaintTable(List<int> checkedPanels, bool isYou)
        {
            for (int i = 0; i < tablePanels.Count; i++)
            {
                tablePanels[i].color = checkedPanels.Contains(i) ? Color.gray : Color.white;
            }

            backgroundImage.enabled = isYou;
        }

        public void Show()
        {
            panel.SetActive(true);
        }

        public void Hide()
        {
            PaintTable(new List<int>(), false);
            panel.SetActive(false);
        }
    }
    internal List<RankPanel> rankPanelsList;

    [Header("Change Table Button")]
    public GameObject changeTableButtonPrefab;
    public Transform changeButtonParent;
    internal GridLayoutGroup changeButtonParentGridLayout;

    [Header("Victories")]
    public RectTransform buenasButtonRectT;
    GameObject buenasPressIndicator;
    public GameObject victoryPanel;
    internal int lastWinStreak;
    internal string lastWinnerId;
    internal Dictionary<string, object> lastWinners;
    public TMP_Text victoryPlayerText;
    public RawImage victoryPlayerPhotoImage;
    public TMP_Text victoryNextGameTitleText;
    public TMP_Text victoryNextGameCounterText;
    public GameObject changeRulesButton;
    public Button selectTableButton;
    public Transform winnerTableChipsParent;

    [Header("Winners")]
    public Transform winnersParent;
    public GameObject winnerPlayerPrefab;
    internal List<WinnerPlayerComponent> winnerPlayers;

    [Header("Emblems")]
    public GameObject emblemsParent;
    public GameObject streakEmblemObj;
    public TMP_Text streakEmblemCountText;
    public GameObject streakKillerEmblemObj;

    internal List<float> victoryExperience = new List<float>() { 10, 10, 10, 20 }; // 0 - Linea | 1 - Equinas | 2 - Pozito | 3 - Llena
    internal List<List<int>> victoriesList;
    Game_Table lastPlayedTable;
    internal int victoryIndex;
    internal bool playingFullTableVictory = false;

    [Header("Start Game Counter")]
    public GameObject startGameCounterPanel;
    public TMP_Text startGameCounterText;
    public TMP_Text startGameCounterPlayersText;
    public TMP_Text startGameCounterBotsText;
    public TMP_Text startGameCounterExpectatorsText;
    public TMP_Text startGameCounterBetPerTableText;
    public TMP_Text startGameCounterPrizeText;
    public TMP_Text startGameCounterAccumulatedText;
    public Slider startGameCounterCantadaSpeedSlider;
    public TMP_Text startGameCounterCantadaSpeedText;
    public Image startGameCounterCantadaSpeedImage;
    public Image[] startGameCounterVictoryTypesImages;
    internal Coroutine startGameCounterLineTableAnimCoroutine;
    public GameObject startGameCounterLineTable;
    internal Coroutine nextGameCounterCoroutine;
    internal float initGameTime;

    [Header("Audios")]
    public AudioClip[] cardCantadaSounds;
    public AudioClip[] gameSounds;
    internal AudioSource audioSource;

    //UI Raycast
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    internal bool inGame = false;
    internal bool isPlaying = false;

    private void Start()
    {
        m_Raycaster = FindObjectOfType<Canvas>().GetComponent<GraphicRaycaster>();
        m_EventSystem = FindObjectOfType<Canvas>().GetComponent<EventSystem>();

        audioSource = FindObjectOfType<AudioSource>();

        // table buttons
        changeButtonParentGridLayout = changeButtonParent.GetComponent<GridLayoutGroup>();

        // Create rank panels
        rankPanelsList = new List<RankPanel>();
        foreach (var rank in rankPanels)
            rankPanelsList.Add(new RankPanel(rank));

        // Victories
        victoriesList = new List<List<int>>();
        for (int i = 0; i < 4; i++)
        {
            // Horizontal
            victoriesList.Add(new List<int>() { i * 4, (i * 4) + 1, (i * 4) + 2, (i * 4) + 3 });
            // Vertical
            victoriesList.Add(new List<int>() { i, i + 4, i + 8, i + 12 });
        }
        // Diagonales
        victoriesList.Add(new List<int>() { 0, 5, 10, 15 });
        victoriesList.Add(new List<int>() { 3, 6, 9, 12 });
        // Esquinas
        victoriesList.Add(new List<int>() { 0, 3, 12, 15 });
        // Pozito
        victoriesList.Add(new List<int>() { 5, 6, 9, 10 });
        // Full
        victoriesList.Add(new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });

        // Buenas button
        buenasPressIndicator = buenasButtonRectT.GetChild(1).gameObject;
        buenasPressIndicator.SetActive(false);
    }

    private void Update()
    {
        m_PointerEventData = new PointerEventData(m_EventSystem);

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Touch touch = Input.GetTouch(i);
                    TouchLogic(touch.position, touch.phase == TouchPhase.Began);
                }
            }
#else
        TouchLogic(Input.mousePosition, Input.GetMouseButtonDown(0));
#endif
    }

    void TouchLogic(Vector2 position, bool isDown)
    {
        // Save last touch time
        if (isDown) inGameLastTouchTime = Time.time;

        // Game touch logic
        if (inGame && victoryIndex < 0 && isPlaying && !startGameCounterPanel.activeSelf)
        {
            m_PointerEventData.position = position;

            List<RaycastResult> results = new List<RaycastResult>();

            m_Raycaster.Raycast(m_PointerEventData, results);

            foreach (RaycastResult result in results)
            {
                //Debug.Log("Hit " + result.gameObject.tag);

                if (result.gameObject.tag == "GameChangeTableButton")
                {
                    int panelIndex = result.gameObject.transform.parent.GetSiblingIndex();
                    //print("Changing tables - " + panelIndex);

                    ChangeTablesPage(panelIndex);
                }
                else if (result.gameObject.tag == "TableCardPanel" && isDown)
                {
                    //Debug.Log("Hit " + result.gameObject.name + " --- " + result.gameObject.transform.GetSiblingIndex());

                    int panelIndex = result.gameObject.transform.GetSiblingIndex() + (result.gameObject.transform.parent.GetSiblingIndex() * 4);
                    int cardIndex = myTablesDict[result.gameObject.transform.parent.parent.gameObject].cardsList[panelIndex];
                    //print(panelIndex + " ---- " + myTablesDict[result.gameObject.transform.parent.parent.gameObject].cardsList[panelIndex]);
                    if (calledCards.Contains(cardIndex))
                    {
                        if(SetTableChip(result.gameObject.transform.parent.parent.gameObject, panelIndex))
                        {
                            FillMissedCardsCounter();

                            StartCoroutine(VictoryCheck(
                                app.privateRoomLobbyComponent.curretRoomPlayerFbId,
                                null,
                                myTablesDict[result.gameObject.transform.parent.parent.gameObject])
                            );

                            lastRequieredCardTapped = -1;
                        }
                        rascadaObj.SetActive(false);
                    }
                    else
                    {
                        if (lastRequieredCardTapped == -1)
                        {
                            ResetRascadaTaps(cardIndex);
                        }
                        else if (lastRequieredCardTapped == cardIndex)
                        {
                            timesTapped++;

                            if(Time.time - firstTapTime < rascadaTapTimer)
                            {
                                ShowRascadaTrebol(timesTapped);

                                if (timesTapped == 4)
                                {
                                    lastRequieredCardTapped = -1;

                                    // Send notification message
                                    app.StartCoroutine(SendRascada(cardIndex));
                                }
                            }
                            else
                            {
                                ResetRascadaTaps(cardIndex);
                            }
                        }
                        else
                        {
                            ResetRascadaTaps(cardIndex);
                        }
                    }
                }
            }
        }
    }

    void ResetRascadaTaps(int _cardIndex)
    {
        lastRequieredCardTapped = _cardIndex;

        timesTapped = 1;
        firstTapTime = Time.time;
        ShowRascadaTrebol(timesTapped);
    }

    void ShowRascadaTrebol(int _timesTapped)
    {
        rascadaObj.SetActive(true);
        rascadaObj.GetComponent<RectTransform>().position = Input.mousePosition + (Vector3.up * 25f);
        for (int i = 0; i < rascadaParts.Length; i++)
        {
            rascadaParts[i].SetActive(i < _timesTapped);
        }

        if(fadeRascadaRoutine != null)  StopCoroutine(fadeRascadaRoutine);
        fadeRascadaRoutine = StartCoroutine(FadeOutRascadaTrebol());
    }

    IEnumerator FadeOutRascadaTrebol()
    {
        List<Image> parts = new List<Image>();
        List<Color> colors = new List<Color>();
        Color _color;
        for (int i = 0; i < rascadaParts.Length; i++)
        {
            parts.Add(rascadaParts[i].GetComponent<Image>());

            _color = parts.Last().color;
            _color = new Color(_color.r, _color.g, _color.b, 1.0f);
            colors.Add(_color);
        }

        float fadeTime = 1.5f;
        float curTime = 0;
        while (curTime < fadeTime)
        {
            for (int i = 0; i < parts.Count; i++)
            {
                parts[i].color = Color.Lerp(colors[i], colors[i] - new Color(0, 0, 0, 1), curTime / fadeTime);
                curTime += Time.deltaTime;
                yield return null;
            }
        }
        rascadaObj.SetActive(false);
    }

    bool  SetTableChip(GameObject selectedPanelObj, int panelIndex)
    {
        if(myTablesDict[selectedPanelObj].disabledPanels[panelIndex].activeSelf)
            return false;

        myTablesDict[selectedPanelObj].disabledPanels[panelIndex].SetActive(true);
        var panelRectT = myTablesDict[selectedPanelObj].disabledPanels[panelIndex].GetComponent<RectTransform>();

        GameObject newChip = Instantiate(chipPrefab, chipsParents[currentTablesPage]);
        var rectT = newChip.GetComponent<RectTransform>();
        rectT.position = Input.mousePosition;
        rectT.Rotate(Vector3.forward * Random.Range(0, 360));

        float difX = panelRectT.position.x - rectT.position.x;
        float difY = panelRectT.position.y - rectT.position.y;
        if(Mathf.Abs(difX) > 25)
        {
            rectT.position += Vector3.right * difX / 2;
        }
        if (Mathf.Abs(difY) > 70)
        {
            rectT.position += Vector3.up * difY / 2;
        }

        return true;
    }

    internal IEnumerator VictoryCheck(string fbId, BotComponent _bot, Game_Table table)
    {
        yield return null;

        if (!gameInitialized || !inGame)
            throw new System.Exception("Game not initialized");

        List<int> checkedPanels = new List<int>();
        for (int i = 0; i < table.disabledPanels.Count; i++)
            if (table.disabledPanels[i].activeSelf)
                checkedPanels.Add(i);

        string[] parts = app.mainController.currentRoom.victoryTypes.Split(',');

        // RANK TESTS
        int maxMatches = 0;
        int totalMatches = 0;
        Dictionary<int, int> matchesDic = new Dictionary<int, int>(); // key: victoryId - value: matches
        
        int matches = 0;
        int _victoryIndex = -1;

        foreach (var p in parts)
        {
            switch (int.Parse(p))
            {
                case 0: // LINES
                    if (playingFullTableVictory) break;
                    for (int i = 0; i < 10; i++)
                    {
                        matches = 0;
                        foreach (var v in victoriesList[i])
                        {
                            if (checkedPanels.Contains(v))
                            {
                                matches++;
                                if (matches > maxMatches) maxMatches = matches;
                                totalMatches++;
                                if (matches == victoriesList[i].Count)
                                {
                                    _victoryIndex = i;
                                    break;
                                }
                            }
                        }
                        matchesDic.Add(i, matches);
                        if (matches == victoriesList[i].Count)
                        {
                            print("Holy shit you won! - LINE " + i);
                            break;
                        }
                    }
                    break;
                case 1: // CORNERS
                    if (playingFullTableVictory) break;
                    matches = 0;
                    foreach (var v in victoriesList[10])
                    {
                        if (checkedPanels.Contains(v))
                        {
                            matches++;
                            totalMatches++;
                            if (matches > maxMatches) maxMatches = matches;
                            if (matches == victoriesList[10].Count)
                            {
                                _victoryIndex = 10;
                                print("Holy shit you won! - CORNERS");
                                break;
                            }
                        }
                    }
                    matchesDic.Add(10, matches);
                    break;
                case 2: // CENTER
                    if (playingFullTableVictory) break;
                    matches = 0;
                    foreach (var v in victoriesList[11])
                    {
                        if (checkedPanels.Contains(v))
                        {
                            matches++;
                            totalMatches++;
                            if (matches > maxMatches) maxMatches = matches;
                            if (matches == victoriesList[11].Count)
                            {
                                _victoryIndex = 11;
                                print("Holy shit you won! - CENTER");
                                break;
                            }
                        }
                    }
                    matchesDic.Add(11, matches);
                    break;
                case 3: // FULL
                    if(playingFullTableVictory)
                    {
                        if (checkedPanels.Count == 16)
                        {
                            _victoryIndex = 12;
                            print("OMG! Holy shit you won! - FULL");
                            matches = 16;
                            totalMatches = 16;
                            if (matches > maxMatches) maxMatches = matches;
                        }
                        matchesDic.Add(12, checkedPanels.Count);
                    }
                    break;
                default:
                    break;
            }
            // Check if won
            if (matches >= 4 && _victoryIndex >= 0)
            {
                if(_bot == null)
                {
                    victoryIndex = _victoryIndex;
                    lastPlayedTable = table;
                    StartCoroutine(BuenasButtonAppearAnimation());
                }
                else
                {
                    _bot.victoryIndex = _victoryIndex;
                    _bot.lastPlayedTable = table;
                    print("BOT WON");
                    _bot.OnGritarBuenas();
                }
                break;
            }
        }

        // Update checked panels in firebase if this table is the closest to victory
        int max = matchesDic.Count == 0 ? 0 : matchesDic.Max(x => x.Value);
        if(max > 0)
        {
            int victoriesWithMax = matchesDic.Count(x => x.Value == max);
            //print("=== CHECKING WINNING TABLE:  --  MAX= " + max + "   --  VICTORYCOUNT= " + victoriesWithMax);
            var _lastWinningTable = _bot == null ? lastWinningTable : _bot.lastWinningTable;

            if (_lastWinningTable.Key < max || (_lastWinningTable.Key == max && _lastWinningTable.Value <= victoriesWithMax))
            {
                _lastWinningTable = new KeyValuePair<int, int>(max, victoriesWithMax);

                if (_bot == null)
                    lastWinningTable = _lastWinningTable;
                else
                {
                    _bot.lastWinningTable = _lastWinningTable;
                    _bot.lastPlayedTable = table;
                }

                //print("=== NEW WINNING TABLE:  --  MAX= " + max + "   --  VICTORYCOUNT= " + victoriesWithMax);

                float rankPoints = ((float)max + (((float)victoriesWithMax / (float)max) / matchesDic.Count));
                StartCoroutine(UpdateCheckedPanelsFb(fbId, checkedPanels, rankPoints, 0));
            }
        }
    }

    IEnumerator SendRascada(int cardIndex)
    {
        string key = _rascadas_ref.Push().Key;

        Dictionary<string, object> childUpdates = new Dictionary<string, object>();

        childUpdates["/playerId"] = app.mainController.myPlayer.id;
        childUpdates["/cardIndex"] = cardIndex;

        _rascadas_ref.Child(key).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Rascada sent");
            }
        });

        yield return new WaitForSeconds(1f);

        _rascadas_ref.Child(key).RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Rascada removed");
            }
        });
    }

    void Handle_Rascada_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        print(args.Snapshot.GetRawJsonValue());
        Globals.Rascada rascada = JsonUtility.FromJson<Globals.Rascada>(args.Snapshot.GetRawJsonValue());
        //print(rascada.playerId);
        app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
            Globals.NotificationType.Rascada,
            app.tableBuilderComponent.deckCards[rascada.cardIndex],
            string.Empty,
            rascada.playerId
        ));
    }

    public IEnumerator UpdateCheckedPanelsFb(string fbId, List<int> checkedPanels, float rankPoints, float delay)
    {
        yield return new WaitForSeconds(delay);

        //print(fbId + "  -  " + rankPoints);
        if(app.privateRoomLobbyComponent._players_ref != null)
        {
            Dictionary<string, object> childUpdates = new Dictionary<string, object>();
            childUpdates["/checkedPanels"] = checkedPanels;
            childUpdates["/rankPoints"] = rankPoints;
            app.privateRoomLobbyComponent._players_ref.Child(fbId).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    //Debug.Log("Completed! - Player checked panels updated");
                }
            });
        }
    }

    List<Coroutine> tablesInOutRoutines;
    IEnumerator AnimateTableInOut(GameObject tableObj, bool isIn, bool moveRight)
    {
        tableObj.SetActive(true);
        RectTransform rectT = tableObj.GetComponent<RectTransform>();

        Vector3 initPos = isIn ? (Vector3.right * rectT.sizeDelta.x * (moveRight ? -1 : 1)) : Vector3.zero;
        Vector3 endPos = isIn ? Vector3.zero : (Vector3.right * rectT.sizeDelta.x * (moveRight ? -1 : 1) * (isIn ? 1 : -1));
        rectT.localPosition = initPos;

        float animTime = 0.1f;
        float currentTime = 0;
        while (currentTime < animTime)
        {
            rectT.localPosition = Vector3.Lerp(initPos, endPos, currentTime / animTime);

            currentTime += Time.deltaTime;
            yield return null;
        }
        rectT.localPosition = endPos;
    }
    public void ChangeTablesPage(int page)
    {
        if(currentTablesPage == page)
            return;

        // ANim routines
        if (tablesInOutRoutines != null)
            foreach (var r in tablesInOutRoutines)
                StopCoroutine(r);
        tablesInOutRoutines = new List<Coroutine>();

        // chips parent
        for (int i = 0; i < chipsParents.Count; i++)
        {
            chipsParents[i].gameObject.SetActive(i == page || i == currentTablesPage);

            if((i == page || i == currentTablesPage))
            {
                tablesInOutRoutines.Add(StartCoroutine(AnimateTableInOut(chipsParents[i].gameObject,
                    i == page,
                    currentTablesPage - page > 0
                )));
            }
        }

        // enable selected tables
        foreach (var table in myTablesDict)
            table.Key.SetActive(false);
        bool isIn, isOut;
        for (int i = 0; i < app.privateRoomLobbyComponent.selectedTablesObjList.Count; i++)
        {
            isIn = (i == page * 2 || i == (page * 2) + 1);
            isOut = (i == currentTablesPage * 2 || i == (currentTablesPage * 2) + 1);
            if(isIn || isOut)
            {
                tablesInOutRoutines.Add(StartCoroutine(AnimateTableInOut(app.privateRoomLobbyComponent.selectedTablesObjList[i],
                    isIn,
                    currentTablesPage - page > 0
                )));
            }
            //app.privateRoomLobbyComponent.selectedTablesObjList[i].SetActive(isActive);
            //print("Table: " + app.privateRoomLobbyComponent.selectedTablesIdList[i] + " --- " + isActive);
        }

        // page buttons
        // Set colors and missed
        for (int i = 0; i < tablePageButtonList.Count; i++)
        {
            tablePageButtonList[i].backgroundImage.rectTransform.localScale = Vector3.one * (i == page ? 1.3f : 1f);
            tablePageButtonList[i].backgroundImage.color = new Color(
                tablePageButtonList[i].backgroundImage.color.r,
                tablePageButtonList[i].backgroundImage.color.g,
                tablePageButtonList[i].backgroundImage.color.b,
                i == page ? 1 : 0.5f
            );

            // Missed indicator
            if(i == page)
                tablePageButtonList[i].ShowMissedPanel();
            else
                tablePageButtonList[i].FitMissedPanel(tablePageButtonList.Count);
        }

        currentTablesPage = page;
    }

    public void CreatePlayerTables()
    {
        // Destroy old tables
        foreach (Transform child in tableUpContainer)
            Destroy(child.gameObject);
        foreach (Transform child in tableDownContainer)
            Destroy(child.gameObject);

        myTablesDict = new Dictionary<GameObject, Game_Table>();

        _playersTables_ref = FirebaseDatabase.DefaultInstance.GetReference("Players/" + app.mainController.myPlayer.id + "/tablesDic");

        _playersTables_ref.ChildAdded -= HandleTableChildAdded;
        _playersTables_ref.ChildAdded += HandleTableChildAdded;

        app.mainController.loadingPanel.SetActive(false);
    }

    internal void EndPlayerTablesHandler()
    {
        _playersTables_ref.ChildAdded -= HandleTableChildAdded;
    }

    void HandleTableChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());

        Globals.Table _table = JsonUtility.FromJson<Globals.Table>(args.Snapshot.GetRawJsonValue());

        if(string.IsNullOrEmpty(_table.name))
        {
            throw new System.Exception("Error with player " + app.mainController.myPlayer.id + " table data: " + args.Snapshot.GetRawJsonValue());
        }

        try
        {
            GameObject newTableInGame = Instantiate(tablePrefab, myTablesDict.Count % 2 == 0 ? tableUpContainer : tableDownContainer);
            newTableInGame.transform.localPosition = Vector3.zero;
            newTableInGame.GetComponent<RectTransform>().sizeDelta = new Vector2(540, 825);

            Game_Table newTable = new Game_Table(newTableInGame);
            newTable.myTable = _table;
            newTable.firebaseId = args.Snapshot.Key;

            // Set panels
            string[] parts = newTable.myTable.tablePanels.Replace("\"", "").Split(',');
            int cardId;
            for (int i = 0; i < parts.Length; i++)
            {
                cardId = int.Parse(parts[i]);
                newTable.panelsImages[i].sprite = app.tableBuilderComponent.deckCardsSprites[cardId];

                newTable.cardsList.Add(cardId);

                // Hide disable panel image
                newTable.disabledPanels[i].GetComponent<Image>().enabled = false;
            }

            myTablesDict.Add(newTableInGame, newTable);

            // Create preview tables on Table Selection Panel
            app.privateRoomLobbyComponent.CreateTableSelection(newTable);
        }
        catch(System.Exception e)
        {
            throw new System.Exception(e.Message);
        }
    }

#region GAME MECHANIC
    public IEnumerator InitGame(bool fromLobby)
    {
        print("===STARTING GAME=== . " + fromLobby);
        initGameTime = Time.time;

        // Clear called cards
        foreach (Transform g in calledCardGrids)
        {
            foreach (Transform c in g)
                Destroy(c.gameObject);
        }
        lastCalledCardImage.sprite = null;

        yield return null;

        // if game come from lobby, start firebase references calls
        if (fromLobby)
            StartRefreshCurrentPrivateRoom();

        gameInitialized = true;

        inGame = true;
        calledCards = new List<int>();
        lastWinningTable = new KeyValuePair<int, int>(0, 0);

        inGameDisconnectedPlayers = new Dictionary<string, long>();

        // Reactions button
        app.reactionsController.reactionsButton.SetActive(true);

        // Counter panel
        CleanCountdownWindow();
        startGameCounterText.text = "";
        startGameCounterPanel.SetActive(true);

        // check if can play (choose tables and have money to bet them)
        if (app.privateRoomLobbyComponent.selectedTablesIdList.Count > 0)
        {
            // If was not playing, insert tables to game
            if (!isPlaying)
                app.privateRoomLobbyComponent.UpdateTablesQty(app.privateRoomLobbyComponent.selectedTablesIdList.Count);

            isPlaying = true;
            spectatorModePanel.SetActive(false);
        }
        else
        {
            isPlaying = false;
            app.promptComponent.ShowPrompt("Modo espectadore",
                "El juego ya comenzó.\nElige tus tablas y espera a que termine la partida",
                Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Pink);
            spectatorModePanel.SetActive(true);
            app.privateRoomLobbyComponent.OpenTableSelectionPanel();
            startGameCounterPanel.SetActive(false);
            victoryPanel.SetActive(false);
            app.adsController.HideBanner();
            emblemsParent.SetActive(false);
            // Destroy old select buttons
            foreach (Transform child in changeButtonParent)
                Destroy(child.gameObject);

            // Retire tables from play to avoid bet
            app.privateRoomLobbyComponent.UpdateTablesQty(0);
        }

        // hide victory panel / buenas button
        victoryPanel.SetActive(false);
        app.adsController.HideBanner();
        emblemsParent.SetActive(false);
        buenasButtonRectT.gameObject.SetActive(false);
        victoryIndex = -1;
        // hide victory emblem
        emblemsParent.SetActive(false);

        // reset called cards
        calledCardsCounterText.text = calledCards.Count + "/" + app.tableBuilderComponent.deckCardsSprites.Count;

        // reset playable cards
        foreach (var table in myTablesDict)
            table.Value.ResetTable();
        // Reset chips
        foreach (Transform parent in chipsParents)
            foreach (Transform chip in parent)
                Destroy(chip.gameObject);

        // rankings
        foreach (var r in rankPanelsList)
            r.Hide();

        changeRulesButton.SetActive(false);

        // HOST
        if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
        {
            // Reset calling cards
            callableCards = new List<int>();
            for (int i = 0; i < app.tableBuilderComponent.deckCardsSprites.Count; i++)
                callableCards.Add(i);

            changeRulesButton.SetActive(true);

            StartCoroutine(StartGameCountdown());

            // Bots start to play
            foreach (var bot in app.privateRoomLobbyComponent.myBots)
            {
                bot.StartPlaySimulation(true);
            }
            // Block bots related buttons
            app.privateRoomLobbyComponent.createBotButton.GetComponent<Button>().interactable = false;
            foreach (var pol in app.privateRoomLobbyComponent.playersOnListDic)
                pol.Value.kickButton.interactable = false;
            app.botWindowComponent.CloseWindow();
            app.privateRoomLobbyComponent.editRulesButton.GetComponent<Button>().interactable = false;
        }

        // Play corre y se va corriendo sound
        audioSource.volume = 1;
        audioSource.clip = gameSounds[0];
        audioSource.Play();
    }

    public IEnumerator StartGameCountdown()
    {
        yield return new WaitForSeconds(1f);

        if (app.privateRoomLobbyComponent._room_state_ref == null)
            throw new System.Exception("No room state reference");

        // Change room status with timer
        for (int i = 0; i < 3; i++)
        {
            app.privateRoomLobbyComponent._room_state_ref.SetValueAsync("GameStartCountdown," + (3 - i), 1).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    //Debug.Log("Completed! - GameStart Counter updated");
                    //UnityMainThreadDispatcher.Instance().Enqueue(EndPrivateRoomPlayerRefresh());
                }
            });

            yield return new WaitForSeconds(1f);
        }

        yield return null;

        if(app.privateRoomLobbyComponent._room_state_ref != null)
        {
            app.privateRoomLobbyComponent._room_state_ref.SetValueAsync("InGame", 1).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - StartGame");
                    UnityMainThreadDispatcher.Instance().Enqueue(PrepareCardCaller());
                }
            });
        }
    }

    public void GritarBuenas()
    {
        buenasButtonRectT.gameObject.SetActive(false);

        // Subir buenas a game session
        Globals.Buenas buenas = new Globals.Buenas()
        {
            playerId = app.mainController.myPlayer.id,
            winningTable = lastPlayedTable.myTable.tablePanels,
            victoryIndex = victoryIndex,
            winningTableId = lastPlayedTable.firebaseId,
            victoryType = victoryIndex <= 8 ? 0 : victoryIndex - 9, // 0 a 9: Lineas | 10 - Esquinas | 11 - Pozito | 12 - Llena
            botName = null
        };
        string json = JsonUtility.ToJson(buenas);
        //print(json);
        _room_buenas_ref.SetRawJsonValueAsync(json, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Buenas screamed");
                //UnityMainThreadDispatcher.Instance().Enqueue(AfterTableSave());
            }
        });
    }

    IEnumerator UpdateTableVictories(int newValue)
    {
        _player_table_ref.SetValueAsync(newValue, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Table victories updated");
                //UnityMainThreadDispatcher.Instance().Enqueue(AfterTableSave());
            }
        });

        yield return null;
    }

    public void StartRefreshCurrentPrivateRoom()
    {
        if(_room_buenas_ref != null)
        {
            _room_buenas_ref.ValueChanged -= Handle_PrivateRoomBuenas_ValueChanged;
            _room_buenas_ref = null;
        }
        _room_buenas_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/Buenas");
        _room_buenas_ref.ValueChanged += Handle_PrivateRoomBuenas_ValueChanged;

        if (_calledCards_ref != null)
        {
            _calledCards_ref.ChildAdded -= Handle_CalledCards_ChildAdded;
            _calledCards_ref = null;
        }
        _calledCards_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/calleddCardsDic");
        _calledCards_ref.ChildAdded += Handle_CalledCards_ChildAdded;

        if (_room_ref != null)
        {
            _room_ref = null;
        }
        _room_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms").Child(app.mainController.currentRoom.id);

        if (_rascadas_ref != null)
        {
            _rascadas_ref.ChildAdded -= Handle_Rascada_ChildAdded;
            _rascadas_ref = null;
        }
        _rascadas_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/rascadas");
        _rascadas_ref.ChildAdded += Handle_Rascada_ChildAdded;

        if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id) // HOST
        {
            // If gameHost disconnects, destroy game
            _room_ref.OnDisconnect().RemoveValue();
        }
        else // CLIENT
        {
            // If client disconnects, remove from list
            if(_player_room_ref != null)
            {
                _player_room_ref = null;
            }
            _player_room_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/playersIdList")
                .Child(app.privateRoomLobbyComponent.curretRoomPlayerFbId);
            _player_room_ref.OnDisconnect().RemoveValue();
        }
    }

    void Handle_PrivateRoomBuenas_ValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        if (victoryPanel.activeSelf)
        {
            print("You can't win! there's a winner already! D:"); // this is bullshit...
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());

        if(!string.IsNullOrEmpty(args.Snapshot.GetRawJsonValue()))
        {
            buenas = JsonUtility.FromJson<Globals.Buenas>(args.Snapshot.GetRawJsonValue());

            // Time
            print("===*** Game time: " + (Time.time - initGameTime) + " - Winner: " + buenas.playerId + " ***===");

            // === HOST === //
            if (app.mainController.myPlayer.id == app.mainController.currentRoom.playerHostId)
            {
                // Stop card caller routine
                if (cardCallerRoutine != null)
                    StopCoroutine(cardCallerRoutine);

                // If have also full table, the game goes on
                List<string> _parts = app.mainController.currentRoom.victoryTypes.Split(',').ToList();
                if(_parts.Contains("3") && !playingFullTableVictory)
                {
                    changeRulesButton.SetActive(false);
                    StartCoroutine(StartFullTableGameCounter());
                }
                else // if not full table selected, or only full table game
                {
                    changeRulesButton.SetActive(true);
                    StartCoroutine(StartNextGameCounter());
                }

                // Unblock bots related buttons
                app.privateRoomLobbyComponent.createBotButton.GetComponent<Button>().interactable = true;
                foreach (var pol in app.privateRoomLobbyComponent.playersOnListDic)
                    pol.Value.kickButton.interactable = true;

                // Unblock change rules
                app.privateRoomLobbyComponent.editRulesButton.GetComponent<Button>().interactable = true;

                // If winner was NOT a bot, check if have accumulative money to give
                if (app.mainController.currentRoom.accumulatedCoins > 0 && string.IsNullOrEmpty(buenas.botName))
                {
                    app.coinController.SetCoinTransaction(
                        app.privateRoomLobbyComponent.botsAccumulativePlayerFbId,
                        buenas.playerId,
                        app.mainController.currentRoom.accumulatedCoins
                    );
                }

                // Manage losers coins transactions
                foreach (var player in app.privateRoomLobbyComponent.playersInRoomList)
                {
                    // Not the winner and not a bot
                    if(player.playerId != buenas.playerId && !player.playerId.Substring(0, 4).Contains("Bot_"))
                    {
                        app.coinController.SetCoinTransaction(
                            player.playerId,
                            string.IsNullOrEmpty(buenas.botName) ? buenas.playerId : buenas.accumulativeCoinId,
                            app.mainController.currentRoom.betPerTable * player.tablesQty
                        );
                    }
                }

                // Check if have disconnected games inGame
                if (inGameDisconnectedPlayers.Count > 0)
                {
                    // Make the transactions from the disconnected players to the winner or the accumulative player if a bot won
                    foreach (var dp in inGameDisconnectedPlayers)
                    {
                        app.coinController.SetCoinTransaction(
                            dp.Key,
                            string.IsNullOrEmpty(buenas.botName) ? buenas.playerId : buenas.accumulativeCoinId,
                            dp.Value
                        );
                    }
                }
                inGameDisconnectedPlayers = new Dictionary<string, long>();

                // Check if streak killer
                string emblemType = string.Empty;
                int newWinStreak = 0;
                if(app.mainController.currentRoom.lastWinner.playerId != buenas.playerId)
                {
                    if(app.mainController.currentRoom.lastWinner.winStreak >= 3)
                    {
                        print("==STREAK KILLER==");
                        emblemType = "StreakKiller";
                    }
                    newWinStreak = 1;
                }
                else
                {
                    newWinStreak = app.mainController.currentRoom.lastWinner.winStreak + 1;
                    if (newWinStreak >= 3)
                    {
                        emblemType = "WinStreak";
                    }
                }
                // Update last winner
                InsertGameWinner(buenas.playerId, newWinStreak, emblemType);
            }

            // SHOW AD BANNER
            victoryPanel.SetActive(true);
            if (app.adsController.bannerRoutine != null)
                app.adsController.StopCoroutine(app.adsController.bannerRoutine);
            app.adsController.bannerRoutine = app.adsController.StartCoroutine(app.adsController.ShowBannerWhenReady());

            // ALL - Victory panel
            inGame = false;

            // Play buenas sound
            audioSource.clip = gameSounds[1];
            audioSource.Play();

            // fill winner player info
            var profileBut = victoryPlayerPhotoImage.gameObject.GetComponent<Button>();
            profileBut.onClick.RemoveAllListeners();
            profileBut.enabled = false;
            if (string.IsNullOrEmpty(buenas.botName))
            {
                victoryPlayerText.text = app.privateRoomLobbyComponent.playersOnListDic[buenas.playerId].nameText.text;
                victoryPlayerPhotoImage.texture = app.privateRoomLobbyComponent.playersOnListDic[buenas.playerId].avatarImage.texture;
                victoryPlayerPhotoImage.color = Color.white;

                if(buenas.playerId != app.mainController.myPlayer.id)
                {
                    profileBut.enabled = true;
                    profileBut.onClick.AddListener(() => {
                        app.globalChatComponent.OnOpenPlayerProfile(buenas.playerId, victoryPlayerText.text, Globals.ProfileOrigin.WinnerPanel);
                    });
                }
            }
            else
            {
                victoryPlayerText.text = buenas.botName;
                victoryPlayerPhotoImage.texture = app.privateRoomLobbyComponent.avatarBotTexture;
                victoryPlayerPhotoImage.color = app.botWindowComponent.GetDificultyColorFromBotId(buenas.playerId);
            }

            // victory panel buttons
            StartCoroutine(TemporalyDisableVictoryButtons());

            // if you are the winner, increase exp
            if(buenas.playerId == app.mainController.myPlayer.id)
            {
                if (app.mainController.currentRoom.playerCount > 1) //- IF NOT PLAYING ALONE
                {
                    // Update experience/level 
                    int lvlsUp = app.profileController.UpdateExperience(victoryExperience[buenas.victoryType]);

                    // Update table victory count
                    _player_table_ref = FirebaseDatabase.DefaultInstance.GetReference("Players/" + app.mainController.myPlayer.id + "/tablesDic/" + buenas.winningTableId + "/victories");
                    _player_table_ref.GetValueAsync().ContinueWith(t => {
                        if (t.IsFaulted)
                        {
                            Debug.Log("Faulted.." + t.Exception.Message);
                        }

                        if (t.IsCanceled)
                        {
                            Debug.Log("Cancelled..");
                        }

                        if (t.IsCompleted)
                        {
                            Debug.Log("Completed! - Got table victories");
                            DataSnapshot snapshot = t.Result;
                            string _res = snapshot.GetRawJsonValue();
                            int tableVictories = int.Parse(string.IsNullOrEmpty(_res) ? "0" : _res) + 1;
                            print(_res);
                            UnityMainThreadDispatcher.Instance().Enqueue(UpdateTableVictories(tableVictories));

                            // Update player stats
                            UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.OnUpdatePlayerStats(new Globals.PlayerStats()
                            {
                                playedGames = 1,
                                totalExperience = victoryExperience[buenas.victoryType],
                                level = lvlsUp,
                                wonGames = 1
                            }, null));
                        }
                    });
                }
                app.mainController.createPrivateRoomWindow.SetActive(false); // << what
            }
            else // if loser, increase exp and stats
            {
                if(isPlaying) // only if is playing and not expectator
                {
                    // Update a little experience/level
                    int lvlsUp = app.profileController.UpdateExperience(victoryExperience[buenas.victoryType] / 5);

                    // Update player stats
                    UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.OnUpdatePlayerStats(new Globals.PlayerStats()
                    {
                        playedGames = 1,
                        totalExperience = victoryExperience[buenas.victoryType] / 5,
                        level = lvlsUp
                    }, null));
                }
            }

            // Winner Table
            List<Image> winnerTablePanels = new List<Image>();
            foreach (Transform row in victoryPanel.transform.GetChild(1).GetChild(1).GetChild(0).GetChild(0))
            {
                foreach (Transform column in row)
                    winnerTablePanels.Add(column.GetComponentInChildren<Image>());
            }
            // Destroy old chips
            foreach (Transform child in winnerTableChipsParent) Destroy(child.gameObject);
            // Set table panels
            string[] parts = buenas.winningTable.Split(',');
            GameObject newChip;
            for (int i = 0; i < parts.Length; i++)
            {
                winnerTablePanels[i].sprite = app.tableBuilderComponent.deckCardsSprites[int.Parse(parts[i])];

                // Add chip if winner panel
                if(victoriesList[buenas.victoryIndex].Contains(i))
                {
                    newChip = Instantiate(chipPrefab, winnerTableChipsParent);
                    var rectT = newChip.GetComponent<RectTransform>();
                    rectT.position = winnerTablePanels[i].rectTransform.position;
                    rectT.sizeDelta = Vector2.one * 60;
                    newChip.transform.GetChild(0).gameObject.GetComponent<RectTransform>().sizeDelta = Vector2.one * 75;
                    newChip.GetComponent<Image>().color = app.promptComponent.promptColors[1];
                    newChip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = Vector2.one * 40;
                }
            }
        }
    }

    public void InsertGameWinner(string _playerId, int _newWinStreak, string _emblemType)
    {
        // Last winner
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        childUpdates["/lastWinner/playerId"] = _playerId;
        childUpdates["/lastWinner/winStreak"] = _newWinStreak;
        childUpdates["/lastWinner/emblemType"] = _emblemType;

        app.privateRoomLobbyComponent._room_ref
            .UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Updater last winner");
                }
            });

        // Winners list
        childUpdates = new Dictionary<string, object>();
        childUpdates["/playerId"] = _playerId;

        string key = app.privateRoomLobbyComponent._room_ref.Child("winners").Push().Key;

        app.privateRoomLobbyComponent._room_ref.Child("winners").Child(key)
            .UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    //Debug.Log("Completed! - Inserted winner to list");
                }
            });
    }

    IEnumerator TemporalyDisableVictoryButtons()
    {
        changeRulesButton.GetComponent<Button>().interactable = false;
        selectTableButton.interactable = false;

        yield return new WaitForSeconds(2f);

        changeRulesButton.GetComponent<Button>().interactable = true;
        selectTableButton.interactable = true;
    }

    IEnumerator ResetBotRanks()
    {
        foreach (var bot in app.privateRoomLobbyComponent.myBots)
        {
            StartCoroutine(UpdateCheckedPanelsFb(bot.myFbKey, new List<int>(), 0, 4.20f));
            yield return new WaitForSeconds(0.12f);
        }
    }

    public IEnumerator CheckYourBet()
    {
        if(gameInitialized)
        {
            yield return new WaitForSeconds(0.5f);

            print("Checking your bet!");
            if (app.privateRoomLobbyComponent.selectedTablesIdList.Count * app.mainController.currentRoom.betPerTable > app.mainController.myPlayer.coins)
            {
                app.privateRoomLobbyComponent.OpenTableSelectionPanel_FromGame();
                app.privateRoomLobbyComponent.UnselectAllTables();

                // Show coins eshop window
                app.eShopComponent.OpeneShopWindow();
            }
        }
    }

    public void StartFullTableGame()
    {
        inGame = true;
        playingFullTableVictory = true;
        victoryPanel.SetActive(false);
        app.adsController.HideBanner();
        emblemsParent.SetActive(false);
        victoryIndex = -1;

        // HOST
        if (app.mainController.myPlayer.id == app.mainController.currentRoom.playerHostId)
        {
            // Start bots play
            if(app.privateRoomLobbyComponent.myBots.Count > 0)
            {
                for (int i = 0; i < app.privateRoomLobbyComponent.myBots.Count; i++)
                {
                    app.privateRoomLobbyComponent.myBots[i].StartPlaySimulation(false);
                }
            }

            StartCoroutine(InitCardCaller());
        }
    }

    public IEnumerator StartFullTableGameCounter()
    {
        yield return null;

        nextGameCounterCoroutine = StartCoroutine(FullTableGameCounter());
    }

    IEnumerator FullTableGameCounter()
    {
        // Change room status with timer
        for (int i = 0; i < 5; i++)
        {
            app.privateRoomLobbyComponent._room_state_ref.SetValueAsync("WaitingFullTableGame," + (5 - i), 1).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    //Debug.Log("Completed! - Counter updated");
                    //UnityMainThreadDispatcher.Instance().Enqueue(EndPrivateRoomPlayerRefresh());
                }
            });

            yield return new WaitForSeconds(1f);
        }

        yield return null;

        app.privateRoomLobbyComponent._room_state_ref.SetValueAsync("FullTableGame", 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Status updated");
                //UnityMainThreadDispatcher.Instance().Enqueue(EndPrivateRoomPlayerRefresh());
            }
        });
    }

    public IEnumerator StartNextGameCounter()
    {
        // from bots too
        if (app.privateRoomLobbyComponent.myBots.Count > 0)
        {
            StartCoroutine(ResetBotRanks());
        }

        yield return null;

        nextGameCounterCoroutine = StartCoroutine(NextGameCounter());
    }

    IEnumerator NextGameCounter()
    {
        // Change room status with timer
        for (int i = 0; i < NEXT_GAME_TIMER; i++)
        {
            app.privateRoomLobbyComponent._room_state_ref.SetValueAsync("WaitingNextGame," + (NEXT_GAME_TIMER - i), 1).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    //Debug.Log("Completed! - Counter updated");
                    //UnityMainThreadDispatcher.Instance().Enqueue(EndPrivateRoomPlayerRefresh());
                }
            });

            yield return new WaitForSeconds(1f);
        }

        yield return null;

        app.privateRoomLobbyComponent._room_state_ref.SetValueAsync("InGame", 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Status updated");
                //UnityMainThreadDispatcher.Instance().Enqueue(EndPrivateRoomPlayerRefresh());
            }
        });
    }

    void Handle_CalledCards_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());

        int _calledCard = int.Parse(args.Snapshot.GetRawJsonValue());

        //Create called card
        if (calledCards.Count > 0)
        {
            GameObject newCalledCard = Instantiate(calledCardPrefab, calledCardGrids[0]);
            //newCalledCard.transform.SetAsFirstSibling();
            newCalledCard.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = app.tableBuilderComponent.deckCardsSprites.First(x => x == lastCalledCardImage.sprite);

            // If first column is full, move to 2nd
            if(calledCardGrids[0].childCount >= 27)
            {
                Transform movingCard = calledCardGrids[0].GetChild(calledCardGrids[0].childCount - 1);
                movingCard.SetParent(calledCardGrids[1]);
                //movingCard.SetAsFirstSibling();
            }
        }

        calledCards.Add(_calledCard);
        lastCalledCardImage.sprite = app.tableBuilderComponent.deckCardsSprites[_calledCard];

        // Fill missed cards indicator in page selector
        FillMissedCardsCounter();

        // Play cantada sound
        audioSource.clip = cardCantadaSounds[_calledCard];
        //print("Playing sound " + _calledCard);
        audioSource.Play();

        calledCardsCounterText.text = calledCards.Count + "/" + app.tableBuilderComponent.deckCardsSprites.Count;

        // Hide preStartPanel if failed to been rmoved
        if (calledCards.Count > 0 && startGameCounterPanel.activeSelf)
            startGameCounterPanel.SetActive(false);
    }

    void FillMissedCardsCounter()
    {
        if (tablePageButtonList == null || !gameInitialized || !inGame || app.privateRoomLobbyComponent.selectedTablesObjList.Count == 0 || !isPlaying)
            return;

        List<int> missingCounts = new List<int>();
        foreach (var but in tablePageButtonList)
            missingCounts.Add(0);
        Game_Table table;
        foreach (var called in calledCards)
        {
            for (int t = 0; t < app.privateRoomLobbyComponent.selectedTablesObjList.Count; t++)
            {
                if (app.privateRoomLobbyComponent.selectedTablesObjList[t] == null)
                    continue;
                table = app.gameController.myTablesDict.First(x => x.Key == app.privateRoomLobbyComponent.selectedTablesObjList[t]).Value;
                for (int i = 0; i < table.disabledPanels.Count; i++)
                {
                    if (!table.disabledPanels[i].activeSelf)
                    {
                        if (table.cardsList[i] == called)
                        {
                            missingCounts[t / 2]++;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < tablePageButtonList.Count; i++)
        {
            if (missingCounts[i] == 0)
                tablePageButtonList[i].HideMissedCardIndicator();
            else
                tablePageButtonList[i].ShowMissedCardIndicator(missingCounts[i]);
        }
    }

    
    public void ExitPrivateRoom()
    {
        inGame = false;
        isPlaying = false;

        if (!gameInitialized)
            return;

        gameInitialized = false;

        if (cardCallerRoutine != null)
            StopCoroutine(cardCallerRoutine);

        RemoveReferences(false);

        // HOST
        if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
        {
            if (nextGameCounterCoroutine != null)
                StopCoroutine(nextGameCounterCoroutine);

            _room_ref.RemoveValueAsync().ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Room removed");
                    UnityMainThreadDispatcher.Instance().Enqueue(LeavePrivateRoom());
                }
            });
        }
        else
        {
            _player_room_ref.RemoveValueAsync().ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Leaved Room");
                    UnityMainThreadDispatcher.Instance().Enqueue(LeavePrivateRoom());
                }
            });
        }
    }

    public void RemoveReferences(bool endGame)
    {
        app.adsController.HideBanner();
        app.privateRoomLobbyComponent.notificationsEnabled = false;
        app.mainController.ShowLoading("Cerrango juego");

        try
        {
            _room_buenas_ref.ValueChanged -= Handle_PrivateRoomBuenas_ValueChanged;

            _calledCards_ref.ChildAdded -= Handle_CalledCards_ChildAdded;
        }
        catch(System.Exception e)
        {
            print(e.Message);
        }

        if (endGame)
        {
            StartCoroutine(LeavePrivateRoom());
        }
    }

    IEnumerator LeavePrivateRoom()
    {
        yield return null;

        app.mainController.privateRoomLobbyWindow.SetActive(false);
        app.mainController.gameWindow.SetActive(false);

        // Return to main menu
        app.mainController.gameState = MainController.GameState.LoggedIn;
        app.mainController.StartCoroutine(app.mainController.ChangeWindow());
    }

    IEnumerator PrepareCardCaller()
    {
        // Delete old Cartas cantadas
        _calledCards_ref.RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Old cartas cantdas removed");
                UnityMainThreadDispatcher.Instance().Enqueue(PrepareCardCaller_2());
            }
        });

        yield return null;
    }

    IEnumerator PrepareCardCaller_2()
    {
        // Delete old Buenas
        _room_buenas_ref.RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Old buenas removed");
                UnityMainThreadDispatcher.Instance().Enqueue(InitCardCaller());
            }
        });

        yield return null;
    }

    IEnumerator InitCardCaller()
    {
        yield return null;

        cardCallerRoutine = StartCoroutine(CardCaller());
    }

    IEnumerator CardCaller()
    {
        while (callableCards.Count > 0)
        {
            int rand = callableCards[Random.Range(0, callableCards.Count)];
            callableCards.Remove(rand);

            string key = _calledCards_ref.Push().Key;
            _calledCards_ref.Child(key).SetValueAsync(rand, 1).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    //Debug.Log("Completed! - Called card registered");
                }
            });

            yield return new WaitForSeconds(app.mainController.currentRoom.cantadaSpeed);
        }

        print("GAME ENDED!!!");
    }

    IEnumerator BuenasButtonAppearAnimation()
    {
        buenasButtonRectT.gameObject.SetActive(true);

        // Buenas press indicator
        int playedGames = PlayerPrefs.GetInt("BuenasShownTimes", -1);
        if (playedGames < 5)
        {
            buenasPressIndicator.SetActive(true);
            playedGames++;
            PlayerPrefs.SetInt("BuenasShownTimes", playedGames);
        }
        else
        {
            buenasPressIndicator.SetActive(false);
        }

        float animTime = 0.420f / 2;
        float curTime = 0;

        while (curTime < animTime)
        {
            curTime += Time.deltaTime;

            buenasButtonRectT.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, curTime / animTime);

            yield return null;
        }
        buenasButtonRectT.localScale = Vector3.one;
    }
    #endregion

    void CleanCountdownWindow()
    {
        startGameCounterText.text = string.Empty;
        startGameCounterPlayersText.text = "---";
        startGameCounterBotsText.text = "---";
        startGameCounterExpectatorsText.text = "---";
        startGameCounterBetPerTableText.text = "---";
        startGameCounterPrizeText.text = "---";
        startGameCounterAccumulatedText.text = "---";
        startGameCounterCantadaSpeedSlider.value = 0;
        startGameCounterCantadaSpeedText.text = "---";
        startGameCounterCantadaSpeedImage.color = Color.white;
        foreach (var img in startGameCounterVictoryTypesImages)
            img.color = Color.white;
    }

    private void OnApplicationQuit()
    {
        RemoveReferences(false);
    }
}
