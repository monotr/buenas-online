﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsController : LoteriaOnlineElement, IUnityAdsListener
{
    internal int MIN_PLAYED_GAMES_TO_AD = 5;
    internal int playedGames;
    internal bool atLeastPlayedOnce = false;

#if UNITY_IOS
    private string gameId = "3607234";
#elif UNITY_ANDROID
    private string gameId = "3607235";
#endif

    public string videoRewarId = "rewardedVideo";
    public string regularVideoAdId = "video";

    public string bannerId = "victoryBanner";
    internal Coroutine bannerRoutine;

    void Start()
    {
        playedGames = PlayerPrefs.GetInt("PlayedGames", 0);
        print("Played games at load: " + playedGames);

        // Initialize the Ads listener and service:
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, false);
    }

    public void ShowFreeCoinsVideoAdPrompt()
    {
        app.promptComponent.ShowPrompt("¡Monedas GRATIS!", "¿Deseas ver un video y recibir $500 GRATIS?", Globals.PromptType.YesNoMessage, "VideoAd", Globals.PromptColors.Green);
    }

    // Implement a function for showing a rewarded video ad:
    public void ShowRewardedVideo()
    {
        if(Advertisement.IsReady(videoRewarId))
            Advertisement.Show(videoRewarId);
    }

    public void ShowRegularVideoAd()
    {
        if (Advertisement.IsReady(regularVideoAdId))
            Advertisement.Show(regularVideoAdId);
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, activate the button: 
        if (placementId == videoRewarId)
        {
            
        }
        else if (placementId == regularVideoAdId)
        {

        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (placementId == videoRewarId)
        {
            // Define conditional logic for each ad completion status:
            if (showResult == ShowResult.Finished)
            {
                // Reward the user for watching the ad to completion.
                app.coinController.SetCoinTransaction("VideoAdReward", app.mainController.myPlayer.id, 500);
                app.promptComponent.ShowPrompt("¡Genial!", "¡Aqui tienes tu recompensa!\n$500", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Blue);
            }
            else if (showResult == ShowResult.Skipped)
            {
                // Do not reward the user for skipping the ad.
            }
            else if (showResult == ShowResult.Failed)
            {
                Debug.LogWarning("The ad did not finish due to an error.");
            }
        }
        
    }

    // BANNER
    public IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.IsReady(bannerId))
        {
            yield return new WaitForSeconds(0.5f);
        }

        if(app.gameController.victoryPanel.activeSelf)
        {
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            Advertisement.Banner.Show(bannerId);
        }
        else
        {
            HideBanner();
        }
    }

    public void HideBanner()
    {
        Advertisement.Banner.Hide();
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }
}
