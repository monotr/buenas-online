﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NotificationsController : LoteriaOnlineElement
{
    bool notificationsOn = true;

    internal float[] notificationSizes = new float[] { 160, 160, 140, 140, 160, 160, 140, 160, 160, 160, 160, 160 };

    public GameObject notificationBubblePrefab;
    public Transform notificationsParent;

    public Texture[] notificationIcons; // 0 - Coin | 1 - Level | 2 - Level | 3 - RulesChange | 4 - FriendRequest
    public Texture[] notificationSecondIcons; // 0 - + | 1 - - | 2 - sent(paperplane)
    public Color[] notificationSecondColors;

    List<NotificationBubbleComponent> notificationBubbles;

    private void Start()
    {
        notificationBubbles = new List<NotificationBubbleComponent>();

        StartCoroutine(NotificationCreatorRoutine());
    }

    public IEnumerator ReceiveNotificationRoutine(Globals.NotificationType _type, Texture _texture, string _message, string _playerId)
    {
        CreateNotification(_type, _texture, _message, _playerId);
        yield return null;
    }

    IEnumerator NotificationCreatorRoutine()
    {
        while (notificationsOn)
        {
            if (notificationBubbles.Count > 0)
            {
                notificationBubbles.First().isInit = true;
                notificationBubbles.RemoveAt(0);
                yield return new WaitForSeconds(0.5f);
            }
            yield return null;
        }
    }

    void CreateNotification(Globals.NotificationType _type, Texture _texture, string _message, string _playerId)
    {
        GameObject newNotificationObj = Instantiate(notificationBubblePrefab, notificationsParent);
        NotificationBubbleComponent bubble = newNotificationObj.GetComponent<NotificationBubbleComponent>();
        bubble.Init(_type, 0, _texture, _message, _playerId);
        notificationBubbles.Add(bubble);

        if (!string.IsNullOrEmpty(_playerId) && _type != Globals.NotificationType.Emojee && _type != Globals.NotificationType.Rascada &&
            _type != Globals.NotificationType.CoinsIn && _type != Globals.NotificationType.CoinsOut)
        {
            if (app.privateRoomLobbyComponent.pendingPhotoNotifications.ContainsKey(_playerId))
            {
                app.privateRoomLobbyComponent.pendingPhotoNotifications.Remove(_playerId);
            }
            app.privateRoomLobbyComponent.pendingPhotoNotifications.Add(_playerId, bubble);
        }
    }
}
