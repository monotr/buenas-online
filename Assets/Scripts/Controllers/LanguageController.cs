﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageController : MonoBehaviour
{
    public enum Language { Espanol, English }
    public Dictionary<string, Dictionary<Language, string>> languageStrings;

    private void Awake()
    {
        print(Application.systemLanguage);
        if (Application.systemLanguage == SystemLanguage.French)
        {
            //Outputs into console that the system is French
            Debug.Log("This system is in French. ");
        }

        languageStrings = new Dictionary<string, Dictionary<Language, string>>();
        
        // === STATIC TEXTS === //
        // LOGIN WINDOW
        AddNewString("login01", Language.Espanol, "Inicia sesión o crea una nueva cuenta");
        AddNewString("login01", Language.English, "Login or create a new account");
        AddNewString("login02", Language.Espanol, "Email");
        AddNewString("login02", Language.English, "Email");
        AddNewString("login03", Language.Espanol, "Contraseña");
        AddNewString("login03", Language.English, "Password");
        AddNewString("login04", Language.Espanol, "JUGAR!");
        AddNewString("login04", Language.English, "PLAY!");
        AddNewString("login05", Language.Espanol, "Enviar email");
        AddNewString("login05", Language.English, "Send email");
        AddNewString("login06", Language.Espanol, "Olvidé mi contraseña");
        AddNewString("login06", Language.English, "Forgot my password");
    }

    void AddNewString(string _key, Language _language, string _string)
    {
        if (!languageStrings.ContainsKey(_key))
            languageStrings.Add(_key, new Dictionary<Language, string>());
        languageStrings[_key].Add(_language, _string);
    }
}
