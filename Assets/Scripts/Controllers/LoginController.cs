﻿using Firebase.Auth;
using Google;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoginController : LoteriaOnlineElement
{
    internal Firebase.Auth.FirebaseUser fbUser;

    public TMP_Text instructionsText;
    public TMP_InputField emailInput;
    public TMP_InputField passwordInput;
    public GameObject playButtonPanel;
    public GameObject forgotPassSendEmailButtonPanel;
    public TMP_Text forgotPassText;
    public GameObject sendEmailButton;
    Button forgotPassButton;

    public GameObject loginTypePanel;
    public GameObject emailLoginPanel;

    private void Start()
    {
        forgotPassButton = forgotPassText.transform.parent.gameObject.GetComponent<Button>();
    }

    void OpenLoginPanel()
    {
        loginTypePanel.SetActive(true);
        emailLoginPanel.SetActive(false);

        instructionsText.text = "Inicia sesión o crea una nueva cuenta";
        passwordInput.transform.parent.gameObject.SetActive(true);
        forgotPassText.text = "Olvidé mi contraseña";
        playButtonPanel.SetActive(true);
        sendEmailButton.SetActive(false);

        forgotPassButton.onClick.RemoveAllListeners();
        forgotPassButton.onClick.AddListener(delegate { OpenForgotPasswordPanel(); });
    }

    public void OpenForgotPasswordPanel()
    {
        instructionsText.text = "Ingresa tu e-mail para enviarte una contraseña nueva";
        passwordInput.transform.parent.gameObject.SetActive(false);
        forgotPassText.text = "Iniciar sesión";
        forgotPassSendEmailButtonPanel.SetActive(true);
        playButtonPanel.SetActive(false);
        sendEmailButton.SetActive(true);

        forgotPassButton.onClick.RemoveAllListeners();
        forgotPassButton.onClick.AddListener(delegate { OpenLoginPanel(); });
    }

    public void SendPasswordResetEmail()
    {
        app.mainController.auth.SendPasswordResetEmailAsync(emailInput.text).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                UnityMainThreadDispatcher.Instance().Enqueue(
                    app.promptComponent.ShowPrompt_Routine("Email inválido", "Verifique el email ingresado", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow
                ));
                return;
            }

            UnityMainThreadDispatcher.Instance().Enqueue(ConfirmPasswordResetRequest());
            Debug.Log("Password reset email sent successfully.");
        });
    }

    IEnumerator ConfirmPasswordResetRequest()
    {
        yield return null;

        OpenLoginPanel();
        app.promptComponent.ShowPrompt("Contraseña reestablecida",
            "Se envió un correo electrónico para hacer el cambio de contraseña", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Green);
    }
    
    public void TryAutoLogin()
    {
        OpenLoginPanel();

#if UNITY_EDITOR
        emailInput.text = "joseluis.gonf@gmail.com";
        passwordInput.text = "1qazxsw2";

        //StartCoroutine(EmailLogin());
#else
        SignInWithGoogleCredential();
#endif
    }

    public void OnEmailCreateAccount()
    {
        if (string.IsNullOrEmpty(emailInput.text))
        {
            //print("Empty email");
            app.promptComponent.ShowPrompt("Datos incompletos", "Favor de introducir su email", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            return;
        }
        else if (string.IsNullOrEmpty(passwordInput.text))
        {
            //print("Empty password");
            app.promptComponent.ShowPrompt("Datos incompletos", "Favor de introducir su contraseña", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            return;
        }
        else if (passwordInput.text.Length < 8)
        {
            //print("Short password");
            app.promptComponent.ShowPrompt("Datos incompletos", "La contraseña debe de tener al menos 8 carácteres", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            return;
        }

        app.mainController.ShowLoading("Creando cuenta");

        PlayerPrefs.SetString("_user", emailInput.text + "," + passwordInput.text);

        app.mainController.auth.CreateUserWithEmailAndPasswordAsync(emailInput.text, passwordInput.text).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                //Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                print("User registered, will try to login");
                UnityMainThreadDispatcher.Instance().Enqueue(EmailLogin());
                return;
            }

            fbUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", fbUser.DisplayName, fbUser.UserId);

            UnityMainThreadDispatcher.Instance().Enqueue(app.mainController.ChangeGameState_LoggedIn());
        });
    }

    public void SignInWithGoogleCredential()
    {
        GoogleSignIn.Configuration = new GoogleSignInConfiguration
        {
            RequestIdToken = true,
            WebClientId = "239144898134-dmqv61644u34milv6kh4udsnenu4342h.apps.googleusercontent.com",
            UseGameSignIn = false
        };

        System.Threading.Tasks.Task<GoogleSignInUser> signIn = GoogleSignIn.DefaultInstance.SignIn();

        System.Threading.Tasks.TaskCompletionSource<FirebaseUser> signInCompleted = new System.Threading.Tasks.TaskCompletionSource<FirebaseUser>();
        signIn.ContinueWith(task => {
            if (task.IsCanceled)
            {
                signInCompleted.SetCanceled();
                UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("ERROR", "LOGIN CANCELLED", Globals.PromptType.ConfirmMessage, "", Globals.PromptColors.Yellow));
            }
            else if (task.IsFaulted)
            {
                signInCompleted.SetException(task.Exception);
                UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("ERROR", "LOGIN FAULTED 0x00: " + task.Exception, Globals.PromptType.ConfirmMessage, "", Globals.PromptColors.Yellow));
            }
            else
            {
                Credential credential = Firebase.Auth.GoogleAuthProvider.GetCredential(((System.Threading.Tasks.Task<GoogleSignInUser>)task).Result.IdToken, null);
                app.mainController.auth.SignInWithCredentialAsync(credential).ContinueWith(authTask => {
                    if (authTask.IsCanceled)
                    {
                        signInCompleted.SetCanceled();
                        UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("ERROR", "FB CANCELLED", Globals.PromptType.ConfirmMessage, "", Globals.PromptColors.Yellow));
                    }
                    else if (authTask.IsFaulted)
                    {
                        signInCompleted.SetException(authTask.Exception);
                        UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("ERROR", "FB FAULTED 0x01: " + authTask.Exception, Globals.PromptType.ConfirmMessage, "", Globals.PromptColors.Yellow));
                    }
                    else
                    {
                        signInCompleted.SetResult(((System.Threading.Tasks.Task<FirebaseUser>)authTask).Result);

                        fbUser = ((System.Threading.Tasks.Task<FirebaseUser>)authTask).Result;
                        Debug.LogFormat("User signed in successfully at: {0} ({1}) - {2}", fbUser.DisplayName, fbUser.UserId, Firebase.Database.ServerValue.Timestamp);

                        //UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("WELCOME", fbUser.DisplayName, Globals.PromptType.ConfirmMessage, "", Globals.PromptColors.Green));

                        UnityMainThreadDispatcher.Instance().Enqueue(app.mainController.ChangeGameState_LoggedIn());


                    }
                });
            }
        });
    }

    IEnumerator EmailLogin()
    {
        app.mainController.ShowLoading("Iniciando sesión");
        yield return new WaitForSeconds(0.1f);
        OnEmailLogin();
    }

    void OnEmailLogin()
    {
        if (string.IsNullOrEmpty(emailInput.text))
        {
            print("Empty email");
            return;
        }
        else if (string.IsNullOrEmpty(passwordInput.text))
        {
            print("Empty password");
            return;
        }

        PlayerPrefs.SetString("_user", emailInput.text + "," + passwordInput.text);

        // Firebase login
        app.mainController.auth.SignInWithEmailAndPasswordAsync(emailInput.text, passwordInput.text).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                UnityMainThreadDispatcher.Instance().Enqueue(WrongCredentials());
                return;
            }

            fbUser = task.Result;
            Debug.LogFormat("User signed in successfully at: {0} ({1}) - {2}", fbUser.DisplayName, fbUser.UserId, Firebase.Database.ServerValue.Timestamp);

            UnityMainThreadDispatcher.Instance().Enqueue(app.mainController.ChangeGameState_LoggedIn());
        });
    }

    IEnumerator WrongCredentials()
    {
        yield return null;

        app.mainController.loadingPanel.SetActive(false);
        app.promptComponent.ShowPrompt("Error de acceso", "Verifique el correo y la contraseña", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
    }

    public void LogOut()
    {
        app.mainController.auth.SignOut();
        app.voiceChatController.LogoutOfVivoxService();

#if !UNITY_EDITOR
        GoogleSignIn.DefaultInstance.SignOut();
#endif

        /*PlayerPrefs.SetString("_user", string.Empty);
        emailInput.text = string.Empty;
        passwordInput.text = string.Empty;*/

        app.profileController.StartCoroutine(app.profileController.SetNewPhoto(app.tools.TextureToTexture2D(app.privateRoomLobbyComponent.defaultAvatar)));

        app.mainController.profileWindow.SetActive(false);

        app.mainController.gameState = MainController.GameState.LogginIn;
        app.mainController.StartCoroutine(app.mainController.ChangeWindow());
        OpenLoginPanel();
    }
}
