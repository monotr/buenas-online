﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : LoteriaOnlineElement
{
    DatabaseReference _player_coinInput_ref;
    DatabaseReference _player_coins_ref;

    public void InitCoinInput()
    {
        _player_coinInput_ref = FirebaseDatabase.DefaultInstance.GetReference("Players/" + app.mainController.myPlayer.id + "/coinsInput");
        _player_coinInput_ref.ChildAdded += Handle_PlayerCoinInput_ChildAdded;

        _player_coins_ref = FirebaseDatabase.DefaultInstance.GetReference("Players/" + app.mainController.myPlayer.id + "/coins");
        _player_coins_ref.ValueChanged += Handle_PlayerCoins_ValueChanged;
    }

    private void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.T))
        {
            SetCoinTransaction("ZFXCN1vrykdOs4A0RWSkyOeRBzj2", "J39xjXwZYfa0K62cCnkQbRPWBpC2", 100);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            SetCoinTransaction("J39xjXwZYfa0K62cCnkQbRPWBpC2", "ZFXCN1vrykdOs4A0RWSkyOeRBzj2", 100);
        }*/
    }

    void Handle_PlayerCoins_ValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        print("New coins: " + args.Snapshot.Value);
        SetPlayerCoins((long)args.Snapshot.Value);
    }

    void SetPlayerCoins(long coins)
    {
        app.mainController.myPlayer.coins = coins;
        app.mainController.playerInfo_coinsText.text = app.mainController.myPlayer.coins.ToString("f0");
    }

    void Handle_PlayerCoinInput_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        string json = args.Snapshot.GetRawJsonValue();
        print(json);
        Globals.ReceivedCoinTransaction receivedCoinTransaction = null;
        if(json.Contains("quantity"))
            receivedCoinTransaction = JsonUtility.FromJson<Globals.ReceivedCoinTransaction>(args.Snapshot.GetRawJsonValue());
        else
            receivedCoinTransaction = new Globals.ReceivedCoinTransaction(){
                quantity = (long)args.Snapshot.Value,
                originId = null
            };

        // Check if was for buying a table, enable buy table button
        if(!app.tableBuilderComponent.buyTableButton.interactable && (long)args.Snapshot.Value == -500)
        {
            app.tableBuilderComponent.buyTableButton.interactable = true;
        }

        _player_coinInput_ref.Child(args.Snapshot.Key).RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Removed coin input");
                UnityMainThreadDispatcher.Instance().Enqueue(GetPlayerCoins(receivedCoinTransaction));
            }
        });
    }

    IEnumerator GetPlayerCoins(Globals.ReceivedCoinTransaction _receivedCoinTransaction)
    {
        yield return null;

        _player_coinInput_ref.Parent.Child("coins").GetValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                DataSnapshot snapshot = t.Result;
                Debug.Log("Completed! - Current coins " + snapshot.Value);

                UnityMainThreadDispatcher.Instance().Enqueue(UpdatePlayerCoins(string.IsNullOrEmpty(snapshot.GetRawJsonValue()) ? 0 : (long)snapshot.Value, _receivedCoinTransaction));
            }
        });
    }

    IEnumerator UpdatePlayerCoins(long oldCoins, Globals.ReceivedCoinTransaction _receivedCoinTransaction)
    {
        yield return null;

        _player_coinInput_ref.Parent.Child("coins").SetValueAsync(oldCoins + _receivedCoinTransaction.quantity).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Updated coins: " + (oldCoins + _receivedCoinTransaction.quantity));

                // Check bet
                UnityMainThreadDispatcher.Instance().Enqueue(app.gameController.CheckYourBet());

                // Show notification bubble
                UnityMainThreadDispatcher.Instance().Enqueue(app.notificationsController.ReceiveNotificationRoutine(
                    _receivedCoinTransaction.quantity > 0 ? Globals.NotificationType.CoinsIn : Globals.NotificationType.CoinsOut,
                    null,
                    (_receivedCoinTransaction.quantity > 0 ? "" : "-") + "$" + Mathf.Abs(_receivedCoinTransaction.quantity),
                    _receivedCoinTransaction.originId
                ));

                // Update player stats
                if(_receivedCoinTransaction.quantity > 0)
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.OnUpdatePlayerStats(new Globals.PlayerStats()
                    {
                        totalCoinsEarned = _receivedCoinTransaction.quantity
                    }, null));
                }
            }
        });
    }

    public void SetCoinTransaction(string _originId, string _destinyId, long _quantity)
    {
        if(_quantity <= 0)
        {
            print("WTF?");
            return;
        }

        var _coinTransactionRef = FirebaseDatabase.DefaultInstance.GetReference("CoinTransactions");

        string key = _coinTransactionRef.Push().Key;

        Dictionary<string, object> transaction = new Dictionary<string, object>();
        transaction["/id"] = key;
        transaction["/originId"] = _originId;
        transaction["/destinyId"] = _destinyId;
        transaction["/quantity"] = _quantity;
        transaction["/timestamp"] = ServerValue.Timestamp;

        _coinTransactionRef.Child(key).UpdateChildrenAsync(transaction).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Transaction success");
            }
        });
    }
}
