﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class MainController : LoteriaOnlineElement
{
    public TMP_Text versionText;

    internal Firebase.Auth.FirebaseAuth auth;
    internal FirebaseApp fbApp;
    internal DatabaseReference mDatabaseRef;
    internal DatabaseReference _player_status_ref;
    internal DatabaseReference _online_players_count_ref;

    internal Globals.Player myPlayer;
    internal Globals.Room currentRoom;

    public enum GameState { Init, Ready, LogginIn, LoggedIn, InLobby, Playing }
    public GameState gameState;

    [Header("Login Window")]
    public GameObject loginWindow;

    [Header("Main Window")]
    public GameObject mainWindow;
    public TMP_Text onlinePlayersText;

    [Header("Player Info")]
    public Image playerInfo_photoImage;
    public TMP_Text playerInfo_nameText;
    public TMP_Text playerInfo_coinsText;
    public TMP_Text playerInfo_levelText;
    public Slider playerInfo_levelSlider;
    public Image playerInfo_levelSliderFillImage;

    [Header("Gifts")]
    public TMP_Text minuteGiftText;
    public TMP_Text dailyGiftText;
    public AudioClip[] giftsAudioClips;
    int GIFT_MINUTES = 15;
    long GIFT_MINUTES_COINS = 150;
    internal long GIFT_WELCOME_COINS = 1000;
    bool[] gotGift; // 0 - minute | 1 - daily
    DateTime current_dt;
    DateTime daily_dt;
    DateTime minutes_dt;

    [Header("Profile")]
    public GameObject profileWindow;

    [Header("Private room")]
    public GameObject privateRoomsWindow;

    [Header("Create Private room")]
    public GameObject createPrivateRoomWindow;

    [Header("Private room lobby")]
    public GameObject privateRoomLobbyWindow;

    [Header("Table builder")]
    public GameObject tableBuilderWindow;

    [Header("Game")]
    public GameObject gameWindow;

    [Header("Loading")]
    public GameObject loadingPanel;
    public TMP_Text loadingMessageText;

    [Header("Tutorial")]
    public GameObject tutorialPanel;
    public RectTransform tutorialCircleRectT;
    public RectTransform[] tutorialPoiRectTs;
    public TMP_Text tutorialText;
    public TMP_Text tutorialSubText;
    Color subtextColor, subtextColor_alpha;
    bool isTutorialWaitingTouch, isTutorialTouchEnabled;

    [Header("Menu buttons")]
    public Button[] waitLoadMenuButtons;

    private void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
    }

    void Start()
    {
        versionText.text = "versión " + Globals.appVersion;

        gotGift = new bool[2] { false, false };

        ChangeGameState(GameState.Init);
        app.mainController.ShowLoading("Iniciando conexiones");

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                //   app = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
                print("Firebase can start!");
                UnityMainThreadDispatcher.Instance().Enqueue(ChangeGameState_Ready());
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });

        // Set default avatar pic
        app.profileController.StartCoroutine(app.profileController.SetNewPhoto(app.tools.TextureToTexture2D(app.privateRoomLobbyComponent.defaultAvatar)));
    }

    private void Update()
    {
        //DEBUG
        //if(Input.GetKeyDown(KeyCode.S))
            //ScreenCapture.CaptureScreenshot(Application.persistentDataPath + "/Screenshot" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png");

        if (tutorialPanel.activeSelf || loadingPanel.activeSelf)
            return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(app.promptComponent.promptPanel.activeSelf)
            {
                app.promptComponent.OnCancelPrompt();
            }
            else if (app.globalChatComponent.globalChatWindow.activeSelf)
            {
                app.globalChatComponent.CloseGlobalWindow();
            }
            else if(app.friendsComponent.friendWindow.activeSelf)
            {
                app.friendsComponent.OnCloseFriendWindow();
            }
            else if(profileWindow.activeSelf)
            {
                app.profileController.CloseProfileWindow();
            }
            else if (app.rankingComponent.rankingsPanel.activeSelf)
            {
                app.rankingComponent.OnCloseRankings();
            }
            else if (tableBuilderWindow.activeSelf)
            {
                app.tableBuilderComponent.OnCloseTableEditor();
            }
            else if(createPrivateRoomWindow.activeSelf && !app.newPrivateRoomComponent.isEditing)
            {
                app.newPrivateRoomComponent.OnCloseCreatePrivateRoom();
            }
            else if(app.gameNotificationsComponent.gameNotificationsWindow.activeSelf)
            {
                app.gameNotificationsComponent.CloseGameNotificationsWindow();
            }
            else if(app.privateRoomLobbyComponent.tableSelectionPanel.activeSelf)
            {
                app.privateRoomLobbyComponent.OnConfirmTableSelection();
            }
            else if (privateRoomsWindow.activeSelf)
            {
                app.privateRoomsListComponent.OnClosePrivateRoomsList();
            }
            else if(gameState == GameState.InLobby)
            {
                app.privateRoomLobbyComponent.OnExitPrivateRoom();
            }
            else if(gameState == GameState.Playing)
            {
                if(privateRoomLobbyWindow.activeSelf && app.privateRoomLobbyComponent.lobbyWindowRectT.localPosition.y == 0)
                {
                    app.privateRoomLobbyComponent.OnShowHideLobbyWindow(false);
                }
            }
            else
            {
                app.promptComponent.ShowPrompt("Cerrar juego", "¿Desea salir de la aplicación?", Globals.PromptType.YesNoMessage, "CloseGame", Globals.PromptColors.Red);
            }
        }
    }

    IEnumerator ChangeGameState_Ready()
    {
        // Create and hold a reference to your FirebaseApp,
        // where app is a Firebase.FirebaseApp property of your application class.
        fbApp = FirebaseApp.DefaultInstance;

        ChangeGameState(GameState.Ready);
        yield return null;
    }

    public IEnumerator ChangeGameState_LoggedIn()
    {
        // Check app version
        var _app_version_ref = FirebaseDatabase.DefaultInstance.GetReference("AppVersion");
        _app_version_ref.GetValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Got app version");
                DataSnapshot snapshot = t.Result;
                //print(snapshot.GetRawJsonValue());
                UnityMainThreadDispatcher.Instance().Enqueue(CheckAppNewestVersion(snapshot.GetRawJsonValue()));
            }
        });
        
        ChangeGameState(GameState.LoggedIn);

        // Init Firebase Crashlytics
        app.crashlyticsInitComponent.Init();
        //throw new Exception("Oh Co-Bother...");

        yield return null;
    }

    IEnumerator CheckAppNewestVersion(string _appVersion)
    {
        yield return null;

        if(string.IsNullOrEmpty(_appVersion))
        {
            Debug.LogError("This should never happen... if so, there was a problem with Firebase");
        }
        else
        {
            if (!_appVersion.Contains(Globals.appVersion))
            {
                app.promptComponent.StartCoroutine(app.promptComponent.ShowPrompt_Routine(
                    "Nueva versión disponible",
                    "Descarga la nueva versión de la aplicación pulsando ACEPTAR\nCerrar aplicación con CANCELAR",
                    Globals.PromptType.YesNoMessage, "NewAppVersion", Globals.PromptColors.Pink
                ));
            }
        }
    }

    public void ChangeGameState(GameState gs)
    {
        gameState = gs;

        if (app == null)
            app = FindObjectOfType<LoteriaOnlineApplication>();

        switch (gameState)
        {
            case GameState.Init:

                break;
            case GameState.Ready:
                ChangeGameState(GameState.LogginIn);
                break;
            case GameState.LogginIn:
                StartCoroutine(ChangeWindow());
                app.loginController.TryAutoLogin();

                foreach (var btn in waitLoadMenuButtons)
                    btn.interactable = false;
                break;
            case GameState.LoggedIn:
                print("LoggedIn");
                
                // Set this before calling into the realtime database.
                fbApp.SetEditorDatabaseUrl("https://loteriamexicanaonline.firebaseio.com/");

                // Get the root reference location of the database.
                mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

                app = FindObjectOfType<LoteriaOnlineApplication>();

                app.mainController.ShowLoading("Obteniendo tu información!");

                // - If player exists, load data, dont create new
                mDatabaseRef.Child("Players").Child(app.loginController.fbUser.UserId)
                  .GetValueAsync().ContinueWith(task => {
                      if (task.IsFaulted)
                      {
                          // Handle the error...
                          print("ERROR");
                      }
                      else if (task.IsCompleted)
                      {
                          DataSnapshot snapshot = task.Result;

                          string json = snapshot.GetRawJsonValue();
                          //print(json);
                          Globals.Player p = JsonUtility.FromJson<Globals.Player>(json);
                          if(p != null)
                          {
                              // Get friends
                              p.friendList = new Dictionary<string, object>();
                              // Coins
                              if (!json.Contains("coins"))
                                  p.coins = -1;
                              // Set player
                              myPlayer = p;
                              print("Loaded player");
                              UnityMainThreadDispatcher.Instance().Enqueue(GetPlayerImage());
                          }
                          else
                          {
                              UnityMainThreadDispatcher.Instance().Enqueue(GenerateNewPlayer());
                              print("Generating new player");
                          }
                      }
                  });

                break;
            case GameState.InLobby:
                app.gameController.lastWinStreak = 0;
                app.gameController.lastWinnerId = string.Empty;
                app.gameController.lastWinners = new Dictionary<string, object>();
                privateRoomsWindow.SetActive(false);
                privateRoomLobbyWindow.SetActive(true);
                app.privateRoomLobbyComponent.StartRefreshPrivateRoomPlayerList();
                break;
            case GameState.Playing:
                mainWindow.SetActive(false);
                app.privateRoomLobbyComponent.OnShowHideLobbyWindow(false);
                app.privateRoomLobbyComponent.hideLobbyWindowButton.gameObject.SetActive(true);
                gameWindow.SetActive(true);
                app.gameController.StartCoroutine(app.gameController.InitGame(true));
                break;
            default:
                break;
        }
    }

    public IEnumerator CompleteLogin()
    {
        yield return null;

        // Vivox login
        app.voiceChatController.LoginToVivoxService(myPlayer.name, app.loginController.fbUser.UserId);

        // User name
        app.profileController.nicknameInput.text = myPlayer.name;

        // Player info
        playerInfo_nameText.text = myPlayer.name;
        playerInfo_photoImage.sprite = app.profileController.profileImage.sprite;
        app.profileController.FillExperienceUI();

        // Start player status ref
        _player_status_ref = FirebaseDatabase.DefaultInstance.GetReference("Players").Child(myPlayer.id);
        Dictionary<string, object> newFriend = new Dictionary<string, object>();
        newFriend["/status"] = "Online";

        _player_status_ref.UpdateChildrenAsync(newFriend).ContinueWith(t =>
        {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Player status updated!");
            }
        });
        
        _player_status_ref.Child("status").OnDisconnect().SetValue("Offline").ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Player status updated to OFFLINE");
            }
        });

        // Online players
        _online_players_count_ref = FirebaseDatabase.DefaultInstance.GetReference("OnlinePlayerCount");
        _online_players_count_ref.ValueChanged += Handle_OnlinePlayers_ValueAdded;

        // Set login time
        StartCoroutine(SetTimeToPlayer("lastLoginDateTime"));

        // Start coin reference
        app.coinController.InitCoinInput();
    }

    void Handle_OnlinePlayers_ValueAdded(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        string onlinePlayerCount = args.Snapshot.Value.ToString();

        onlinePlayersText.text = "Jugadores en linea: " + onlinePlayerCount;
    }

    IEnumerator GetPlayerImage()
    {
        // If old user without coins, give free coins
        if(myPlayer.coins < 0)
        {
            app.coinController.SetCoinTransaction("WelcomeGift", myPlayer.id, GIFT_WELCOME_COINS);
            app.promptComponent.ShowPrompt(
                "¡Bienvenido!",
                "Agradecemos que juegues Buenas Online!, ¡te damos de bienvenida $1,000 para que empieces a jugar!",
                Globals.PromptType.ConfirmMessage,
                "Welcome", Globals.PromptColors.Blue
            );
        }

        // User profile photo
        UnityMainThreadDispatcher.Instance().Enqueue(app.tools.DownloadFirebasePhoto(myPlayer.profileImageUrl, null, Globals.PhotoDownloadType.Ranking));

        // Start game
        UnityMainThreadDispatcher.Instance().Enqueue(ChangeWindow());
        UnityMainThreadDispatcher.Instance().Enqueue(CompleteLogin());

        yield return null;
    }

    IEnumerator GenerateNewPlayer()
    {
        myPlayer = new Globals.Player() {
            id = app.loginController.fbUser.UserId,
            name = "Jugadore_" + app.loginController.fbUser.UserId.Substring(app.loginController.fbUser.UserId.Length - 7, 7),
            profileImageUrl = string.Empty,
            warnings = 0,
            tablesDic = new Dictionary<string, object>(),
            lastPhotoUploadDateTime = DateTime.Now.AddDays(-1).ToString(),
            lastDailyGiftDateTime = app.tools.ConvertToTimestamp(DateTime.Now.AddDays(-1)),
            level = 0,
            experience = 0,
            coins = 0
        };
        myPlayer.name = myPlayer.name.ToLower();
        
        string json = JsonUtility.ToJson(myPlayer);

        var playersDB = mDatabaseRef.Child("Players");
        playersDB.Child(myPlayer.id).SetRawJsonValueAsync(json, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Player registered");
                List<Globals.Table> newTables = new List<Globals.Table>() { null, null, null, null, null, null, };
                UnityMainThreadDispatcher.Instance().Enqueue(app.tableBuilderComponent.SetTableToPlayer(newTables, true));
            }
        });

        yield return null;
    }

    IEnumerator SetTimeToPlayer(string child)
    {
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        
        if(!string.IsNullOrEmpty(child))
            childUpdates["/" + child] = ServerValue.Timestamp;
        if(myPlayer.lastMinutesGiftDateTime <= 0)
            childUpdates["/lastMinutesGiftDateTime"] = ServerValue.Timestamp;
        if (myPlayer.lastDailyGiftDateTime <= 0)
            childUpdates["/lastDailyGiftDateTime"] = ServerValue.Timestamp;

        childUpdates["/timestampCheck"] = ServerValue.Timestamp;

        var playersDB = mDatabaseRef.Child("Players").Child(myPlayer.id)
            .UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Player -" + child == null ? "timestampCheck" : child + "- time registered");

                    UnityMainThreadDispatcher.Instance().Enqueue(InitFreeCoins(child != "lastLoginDateTime"));

                    if (child == "lastMinutesGiftDateTime")
                        gotGift[0] = false;
                    else if (child == "lastDailyGiftDateTime")
                        gotGift[1] = false;
                }
            });

        yield return null;
    }

    IEnumerator InitFreeCoins(bool isUpdate)
    {
        yield return null;

        var playersDB = mDatabaseRef.Child("Players").Child(myPlayer.id)
            .GetValueAsync().ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Player free coins check");

                    string res_json = t.Result.GetRawJsonValue();
                    var player = JsonUtility.FromJson<Globals.Player>(res_json);

                    UnityMainThreadDispatcher.Instance().Enqueue(CheckGiftTimes(isUpdate, player));
                }
            });
    }

    IEnumerator CheckGiftTimes(bool isUpdate, Globals.Player player)
    {
        current_dt = DateTimeOffset.FromUnixTimeMilliseconds(player.timestampCheck).DateTime;
        daily_dt = DateTimeOffset.FromUnixTimeMilliseconds(player.lastDailyGiftDateTime).DateTime;
        minutes_dt = DateTimeOffset.FromUnixTimeMilliseconds(player.lastMinutesGiftDateTime).DateTime;

        int _h, _m, _s;

        if(!isUpdate)
        {
            while (true)
            {
                _h = (23 - (current_dt - daily_dt).Hours);
                _m = (59 - (current_dt - daily_dt).Minutes);
                _s = (59 - (current_dt - daily_dt).Seconds);

                dailyGiftText.text = "Regalo diario en: " + (_h <= 9 ? "0" : "") + _h + ":" + (_m <= 9 ? "0" : "") + _m + ":" + (_s <= 9 ? "0" : "") + _s;

                _m = ((GIFT_MINUTES - 1) - (current_dt - minutes_dt).Minutes);
                _s = (59 - (current_dt - minutes_dt).Seconds);

                minuteGiftText.text = "Regalo chico en: " + (_m <= 9 ? "0" : "") + _m + ":" + (_s <= 9 ? "0" : "") + _s;

                if ((current_dt - minutes_dt).Minutes >= GIFT_MINUTES && !gotGift[0])
                {
                    print("Minutes gift!");

                    minuteGiftText.text = "Regalo chico en: YA!";

                    app.gameController.audioSource.clip = giftsAudioClips[0];
                    app.gameController.audioSource.Play();

                    gotGift[0] = true;

                    StartCoroutine(SetTimeToPlayer("lastMinutesGiftDateTime"));

                    app.coinController.SetCoinTransaction("MinuteGift", myPlayer.id, GIFT_MINUTES_COINS);
                }

                if ((current_dt - daily_dt).Hours >= 24 && !gotGift[1])
                {
                    print("Daily gift!");

                    dailyGiftText.text = "Regalo diario en: YA!";

                    app.gameController.audioSource.clip = giftsAudioClips[1];
                    app.gameController.audioSource.Play();

                    gotGift[1] = true;

                    StartCoroutine(SetTimeToPlayer("lastDailyGiftDateTime"));
                }

                current_dt = current_dt.AddSeconds(1f);
                yield return new WaitForSeconds(1f);
            }
        }
    }

    public void OpenCreatePrivateRoomWindow()
    {
        mainWindow.SetActive(false);
        createPrivateRoomWindow.SetActive(true);
        app.newPrivateRoomComponent.InitNewPrivateRoom();
    }

    public void OpenPrivateRoomsWindow()
    {
        mainWindow.SetActive(false);
        privateRoomsWindow.SetActive(true);
        app.privateRoomsListComponent.StartFillPrivateRoomsList();
    }

    public IEnumerator OpenPrivateRoomLobbyWindow()
    {
        yield return new WaitForSeconds(0.1f);
        ChangeGameState(GameState.InLobby);
    }

    public void OpenTableBuilderWindow()
    {
        tableBuilderWindow.SetActive(true);
        app.tableBuilderComponent.FillPlayerTables();
    }

    public IEnumerator ChangeWindow()
    {
        loginWindow.SetActive(gameState == GameState.LogginIn);
        mainWindow.SetActive(gameState == GameState.LoggedIn);

        if (gameState == GameState.LoggedIn && app.adsController.playedGames >= app.adsController.MIN_PLAYED_GAMES_TO_AD && app.adsController.atLeastPlayedOnce)
        {
            app.adsController.ShowRegularVideoAd();
            app.adsController.playedGames = 0;
            PlayerPrefs.SetInt("PlayedGames", 0);
        }

        loadingPanel.SetActive(false);

        yield return null;
    }

    #region TUTORIAL
    public IEnumerator TutorialSequence()
    {
        isTutorialWaitingTouch = true;
        isTutorialTouchEnabled = false;
        subtextColor = tutorialSubText.color;
        subtextColor_alpha = new Color(subtextColor.r, subtextColor.g, subtextColor.b, 0);
        tutorialSubText.color = subtextColor_alpha;

        List<string> instructionsList = new List<string>() {
            "Bienvenido a Buenas Online!\nLotería Mexicana -BETA -\nTe daremos un pequeño recorrido para que veas todo lo que puedes hacer!",
            "Aquí podrás ver y buscar las mesas creadas por otros jugadores, elige la que más te agrade y entra a jugar!",
            "Crea tu mesa de juego con las reglas que tu quieras y juega con tus amigos o personas de todo el mundo!",
            "Pulsando este mágico botón, llegarás a la mejor mesa del momento o te creará una para que juegues al instante!",
            "Mira quienes son los mejores jugadores del mundo! ¿Estarás en esta lista?",
            "Cambia tu apodo y tu imagen a mostrar! Mira también tus estadísticas de juego!",
            "Edita tus tablas y consigue más! Puedes jugar con hasta 32 tablas en total!",
            "Aquí puedes ver y buscar a tus amigos!",
            "Platica con jugadores de todo el mundo y haz nuevos amigos!",
            "Aquí recibirás notificaciones de solicitud de amistar o invitaciones para jugar!",
            "Que no se te acaben tus monedas! Consigue más GRATIS o compra paquetes de monedas!"
        };

        tutorialText.text = instructionsList[0];
        tutorialText.rectTransform.localPosition = Vector3.zero;

        tutorialCircleRectT.gameObject.SetActive(false);

        tutorialPanel.SetActive(true);

        StartCoroutine(TutorialShowTouchText());

        for (int i = 1; i < instructionsList.Count; i++)
        {
            while (isTutorialWaitingTouch) yield return null;
            isTutorialWaitingTouch = true;
            
            if(!tutorialCircleRectT.gameObject.activeSelf)
                tutorialCircleRectT.gameObject.SetActive(true);
            
            Tutorial_MoveCircleIndicator(tutorialPoiRectTs[i - 1]);
            tutorialText.text = instructionsList[i];
            StartCoroutine(TutorialShowTouchText());
        }

        while (isTutorialWaitingTouch) yield return null;
        tutorialPanel.SetActive(false);
    }

    void Tutorial_MoveCircleIndicator(RectTransform item)
    {
        tutorialCircleRectT.position = item.position - (Vector3.up * (item.sizeDelta.y / 8));
        
        tutorialText.rectTransform.localPosition = new Vector3(
            tutorialText.rectTransform.localPosition.x,
            tutorialCircleRectT.localPosition.y + (item.sizeDelta.y * 2.2f),
            tutorialText.rectTransform.localPosition.z
        );
    }

    public void Tutorial_TouchToContinue()
    {
        if(isTutorialTouchEnabled)
        {
            isTutorialTouchEnabled = false;
            isTutorialWaitingTouch = false;
            tutorialSubText.color = subtextColor_alpha;
        }
    }

    IEnumerator TutorialShowTouchText()
    {
        yield return new WaitForSeconds(4.20f / 6);
        isTutorialTouchEnabled = true;

        float fadeTime = 0.5f;
        float currentTime = 0;

        while (currentTime < fadeTime)
        {
            tutorialSubText.color = Color.Lerp(subtextColor_alpha, subtextColor, currentTime / fadeTime);
            currentTime += Time.deltaTime;
            yield return null;
        }
        tutorialSubText.color = subtextColor_alpha;

        while (isTutorialTouchEnabled)
        {
            currentTime = 0;

            while (currentTime < fadeTime && isTutorialTouchEnabled)
            {
                tutorialSubText.color = Color.Lerp(subtextColor, subtextColor_alpha, currentTime / fadeTime);
                currentTime += Time.deltaTime;
                yield return null;
            }
            tutorialSubText.color = subtextColor_alpha;

            currentTime = 0;
            while (currentTime < fadeTime && isTutorialTouchEnabled)
            {
                tutorialSubText.color = Color.Lerp(subtextColor_alpha, subtextColor, currentTime / fadeTime);
                currentTime += Time.deltaTime;
                yield return null;
            }
            tutorialSubText.color = subtextColor_alpha;
        }

        tutorialSubText.color = subtextColor_alpha;
    }
    #endregion

    public void ShowLoading(string loadingMessage)
    {
        loadingPanel.SetActive(true);
        loadingMessageText.text = loadingMessage;
    }
}
