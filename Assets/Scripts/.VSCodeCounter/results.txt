Date : 2020-06-08 18:13:29
Directory : d:\Git\BuenasOnline\Assets\Scripts
Total : 36 files,  8721 codes, 1264 comments, 1805 blanks, all 11790 lines

Languages
+----------+------------+------------+------------+------------+------------+
| language | files      | code       | comment    | blank      | total      |
+----------+------------+------------+------------+------------+------------+
| C#       |         36 |      8,721 |      1,264 |      1,805 |     11,790 |
+----------+------------+------------+------------+------------+------------+

Directories
+------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                         | files      | code       | comment    | blank      | total      |
+------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                            |         36 |      8,721 |      1,264 |      1,805 |     11,790 |
| Components                                                                   |         20 |      5,159 |        353 |      1,061 |      6,573 |
| Controllers                                                                  |         10 |      3,010 |        881 |        648 |      4,539 |
+------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+------------------------------------------------------------------------------+----------+------------+------------+------------+------------+
| filename                                                                     | language | code       | comment    | blank      | total      |
+------------------------------------------------------------------------------+----------+------------+------------+------------+------------+
| d:\Git\BuenasOnline\Assets\Scripts\Components\BotComponent.cs                | C#       |        157 |         10 |         30 |        197 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\BotWindowComponent.cs          | C#       |        231 |          7 |         43 |        281 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\CrashlyticsInitComponent.cs    | C#       |         20 |          7 |          3 |         30 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\DeckCreatorComponent.cs        | C#       |         84 |          4 |         21 |        109 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\DraggableCardComponent.cs      | C#       |         79 |          3 |         19 |        101 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\FriendsComponent.cs            | C#       |        225 |         18 |         51 |        294 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\GameNotificationsComponent.cs  | C#       |        163 |          9 |         34 |        206 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\GlobalChatComponent.cs         | C#       |        641 |         50 |        129 |        820 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\LoadingComponent.cs            | C#       |         10 |          0 |          3 |         13 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\NewPrivateRoomComponent.cs     | C#       |        397 |         18 |         87 |        502 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\NotificationBubbleComponent.cs | C#       |        134 |          5 |         16 |        155 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\PrivateRoomLobbyComponent.cs   | C#       |      1,599 |        159 |        327 |      2,085 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\PrivateRoomsListComponent.cs   | C#       |        356 |         18 |         72 |        446 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\PromptComponent.cs             | C#       |        152 |          4 |         19 |        175 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\RankingComponent.cs            | C#       |        348 |         15 |         68 |        431 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\ScrollBackgroundComponent.cs   | C#       |         27 |          0 |          5 |         32 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\TableBuilderComponent.cs       | C#       |        410 |         23 |        100 |        533 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\VoiceChatLobbyComponent.cs     | C#       |         70 |          2 |         20 |         92 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\WinnerPlayerComponent.cs       | C#       |         37 |          0 |          8 |         45 |
| d:\Git\BuenasOnline\Assets\Scripts\Components\eShopComponent.cs              | C#       |         19 |          1 |          6 |         26 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\AdsController.cs              | C#       |         69 |          9 |         16 |         94 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\CoinController.cs             | C#       |        144 |         12 |         33 |        189 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\GameController.cs             | C#       |      1,376 |        114 |        273 |      1,763 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\InAppPurchasesController.cs   | C#       |        141 |         97 |         44 |        282 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\LoginController.cs            | C#       |        173 |          5 |         34 |        212 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\MainController.cs             | C#       |        575 |         31 |        130 |        736 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\NotificationsController.cs    | C#       |         53 |          0 |         12 |         65 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\ProfileController.cs          | C#       |        233 |        599 |         47 |        879 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\ReactionsController.cs        | C#       |        180 |          4 |         43 |        227 |
| d:\Git\BuenasOnline\Assets\Scripts\Controllers\VoiceChatController.cs        | C#       |         66 |         10 |         16 |         92 |
| d:\Git\BuenasOnline\Assets\Scripts\Globals.cs                                | C#       |        212 |          0 |         29 |        241 |
| d:\Git\BuenasOnline\Assets\Scripts\LoteriaOnlineApplication.cs               | C#       |         37 |          2 |          6 |         45 |
| d:\Git\BuenasOnline\Assets\Scripts\MobileUtilities.cs                        | C#       |         40 |          8 |          8 |         56 |
| d:\Git\BuenasOnline\Assets\Scripts\ShareAndRate.cs                           | C#       |         50 |          9 |         14 |         73 |
| d:\Git\BuenasOnline\Assets\Scripts\SimpleCameraController.cs                 | C#       |        132 |          9 |         28 |        169 |
| d:\Git\BuenasOnline\Assets\Scripts\Tools.cs                                  | C#       |         81 |          2 |         11 |         94 |
| Total                                                                        |          |      8,721 |      1,264 |      1,805 |     11,790 |
+------------------------------------------------------------------------------+----------+------------+------------+------------+------------+