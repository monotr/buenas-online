# Details

Date : 2020-06-08 18:13:29

Directory d:\Git\BuenasOnline\Assets\Scripts

Total : 36 files,  8721 codes, 1264 comments, 1805 blanks, all 11790 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Components/BotComponent.cs](/Components/BotComponent.cs) | C# | 157 | 10 | 30 | 197 |
| [Components/BotWindowComponent.cs](/Components/BotWindowComponent.cs) | C# | 231 | 7 | 43 | 281 |
| [Components/CrashlyticsInitComponent.cs](/Components/CrashlyticsInitComponent.cs) | C# | 20 | 7 | 3 | 30 |
| [Components/DeckCreatorComponent.cs](/Components/DeckCreatorComponent.cs) | C# | 84 | 4 | 21 | 109 |
| [Components/DraggableCardComponent.cs](/Components/DraggableCardComponent.cs) | C# | 79 | 3 | 19 | 101 |
| [Components/FriendsComponent.cs](/Components/FriendsComponent.cs) | C# | 225 | 18 | 51 | 294 |
| [Components/GameNotificationsComponent.cs](/Components/GameNotificationsComponent.cs) | C# | 163 | 9 | 34 | 206 |
| [Components/GlobalChatComponent.cs](/Components/GlobalChatComponent.cs) | C# | 641 | 50 | 129 | 820 |
| [Components/LoadingComponent.cs](/Components/LoadingComponent.cs) | C# | 10 | 0 | 3 | 13 |
| [Components/NewPrivateRoomComponent.cs](/Components/NewPrivateRoomComponent.cs) | C# | 397 | 18 | 87 | 502 |
| [Components/NotificationBubbleComponent.cs](/Components/NotificationBubbleComponent.cs) | C# | 134 | 5 | 16 | 155 |
| [Components/PrivateRoomLobbyComponent.cs](/Components/PrivateRoomLobbyComponent.cs) | C# | 1,599 | 159 | 327 | 2,085 |
| [Components/PrivateRoomsListComponent.cs](/Components/PrivateRoomsListComponent.cs) | C# | 356 | 18 | 72 | 446 |
| [Components/PromptComponent.cs](/Components/PromptComponent.cs) | C# | 152 | 4 | 19 | 175 |
| [Components/RankingComponent.cs](/Components/RankingComponent.cs) | C# | 348 | 15 | 68 | 431 |
| [Components/ScrollBackgroundComponent.cs](/Components/ScrollBackgroundComponent.cs) | C# | 27 | 0 | 5 | 32 |
| [Components/TableBuilderComponent.cs](/Components/TableBuilderComponent.cs) | C# | 410 | 23 | 100 | 533 |
| [Components/VoiceChatLobbyComponent.cs](/Components/VoiceChatLobbyComponent.cs) | C# | 70 | 2 | 20 | 92 |
| [Components/WinnerPlayerComponent.cs](/Components/WinnerPlayerComponent.cs) | C# | 37 | 0 | 8 | 45 |
| [Components/eShopComponent.cs](/Components/eShopComponent.cs) | C# | 19 | 1 | 6 | 26 |
| [Controllers/AdsController.cs](/Controllers/AdsController.cs) | C# | 69 | 9 | 16 | 94 |
| [Controllers/CoinController.cs](/Controllers/CoinController.cs) | C# | 144 | 12 | 33 | 189 |
| [Controllers/GameController.cs](/Controllers/GameController.cs) | C# | 1,376 | 114 | 273 | 1,763 |
| [Controllers/InAppPurchasesController.cs](/Controllers/InAppPurchasesController.cs) | C# | 141 | 97 | 44 | 282 |
| [Controllers/LoginController.cs](/Controllers/LoginController.cs) | C# | 173 | 5 | 34 | 212 |
| [Controllers/MainController.cs](/Controllers/MainController.cs) | C# | 575 | 31 | 130 | 736 |
| [Controllers/NotificationsController.cs](/Controllers/NotificationsController.cs) | C# | 53 | 0 | 12 | 65 |
| [Controllers/ProfileController.cs](/Controllers/ProfileController.cs) | C# | 233 | 599 | 47 | 879 |
| [Controllers/ReactionsController.cs](/Controllers/ReactionsController.cs) | C# | 180 | 4 | 43 | 227 |
| [Controllers/VoiceChatController.cs](/Controllers/VoiceChatController.cs) | C# | 66 | 10 | 16 | 92 |
| [Globals.cs](/Globals.cs) | C# | 212 | 0 | 29 | 241 |
| [LoteriaOnlineApplication.cs](/LoteriaOnlineApplication.cs) | C# | 37 | 2 | 6 | 45 |
| [MobileUtilities.cs](/MobileUtilities.cs) | C# | 40 | 8 | 8 | 56 |
| [ShareAndRate.cs](/ShareAndRate.cs) | C# | 50 | 9 | 14 | 73 |
| [SimpleCameraController.cs](/SimpleCameraController.cs) | C# | 132 | 9 | 28 | 169 |
| [Tools.cs](/Tools.cs) | C# | 81 | 2 | 11 | 94 |

[summary](results.md)