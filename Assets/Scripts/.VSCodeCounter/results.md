# Summary

Date : 2020-06-08 18:13:29

Directory d:\Git\BuenasOnline\Assets\Scripts

Total : 36 files,  8721 codes, 1264 comments, 1805 blanks, all 11790 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 36 | 8,721 | 1,264 | 1,805 | 11,790 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 36 | 8,721 | 1,264 | 1,805 | 11,790 |
| Components | 20 | 5,159 | 353 | 1,061 | 6,573 |
| Controllers | 10 | 3,010 | 881 | 648 | 4,539 |

[details](details.md)