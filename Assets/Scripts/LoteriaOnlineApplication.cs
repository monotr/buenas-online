﻿/// INICIADO PROYECTO: 20200425 (:

using UnityEngine;

public class LoteriaOnlineElement : MonoBehaviour
{
    // Gives access to the application and all instances.
    internal LoteriaOnlineApplication app;

    private void Awake()
    {
        app = FindObjectOfType<LoteriaOnlineApplication>();
    }
}

public class LoteriaOnlineApplication : MonoBehaviour
{
    [Header("Controllers")]
    public MainController mainController;
    public LoginController loginController;
    public GameController gameController;
    public VoiceChatController voiceChatController;
    public ProfileController profileController;
    public CoinController coinController;
    public AdsController adsController;
    public InAppPurchasesController inAppPurchasesController;
    public NotificationsController notificationsController;
    public ReactionsController reactionsController;
    public LanguageController languageController;

    [Header("Components")]
    public TableBuilderComponent tableBuilderComponent;
    public NewPrivateRoomComponent newPrivateRoomComponent;
    public PrivateRoomsListComponent privateRoomsListComponent;
    public PrivateRoomLobbyComponent privateRoomLobbyComponent;
    public PromptComponent promptComponent;
    public Tools tools;
    public CrashlyticsInitComponent crashlyticsInitComponent;
    public GlobalChatComponent globalChatComponent;
    public BotWindowComponent botWindowComponent;
    public eShopComponent eShopComponent;
    public RankingComponent rankingComponent;
    public FriendsComponent friendsComponent;
    public GameNotificationsComponent gameNotificationsComponent;
    public SendCoinsComponent sendCoinsComponent;
}
