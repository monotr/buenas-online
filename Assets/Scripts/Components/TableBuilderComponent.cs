﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TableBuilderComponent : LoteriaOnlineElement
{
    [Header("Tables on list")]
    public GameObject tableOnListPrefab;
    public Transform tablesOnListParent;
    public Transform newTablePanel;
    public Color[] tableOnListSelectionColors;
    public GameObject[] tablesPagerButtons;
    int currentPage = 0;
    public Button buyTableButton;

    [Header("Cards")]
    public GameObject dragableCardPrefab;
    public Transform dragableCardsParent;
    internal List<DraggableCardComponent> draggableCardList;
    public Transform draggableCardDragParent;
    public ScrollRect cardsScrollRect;

    public class Builder_Table
    {
        public GameObject myObj;

        public string firebaseId;
        public Globals.Table myTable;

        public List<Image> panelsImages;
        public List<int> cardsList;

        public Image selectionImage;
        public Button selectButton;
        public TMP_Text tableNameText;

        public Button favoriteButton;
        public Image favoriteHeartEmptyImage;
        public Image favoriteHeartFilledImage;

        public bool isSelected = false;

        public bool hasChanges = false;

        public Builder_Table(GameObject obj) 
        {
            myObj = obj;

            // Get table image panels
            panelsImages = new List<Image>();
            foreach (Transform row in obj.transform.GetChild(0))
            {
                foreach (Transform col in row)
                {
                    panelsImages.Add(col.gameObject.GetComponentInChildren<Image>());
                }
            }

            cardsList = new List<int>();

            selectionImage = obj.GetComponent<Image>();
            selectButton = obj.GetComponent<Button>();
            tableNameText = obj.GetComponentInChildren<TMP_Text>();

            favoriteButton = obj.transform.GetChild(2).GetComponent<Button>();
            favoriteHeartEmptyImage = favoriteButton.transform.GetChild(0).GetComponent<Image>();
            favoriteHeartFilledImage = favoriteButton.transform.GetChild(1).GetComponent<Image>();
        }

        public void ToggleFavorite()
        {
            myTable.isFavorite = !myTable.isFavorite;

            favoriteHeartEmptyImage.gameObject.SetActive(!myTable.isFavorite);
            favoriteHeartFilledImage.gameObject.SetActive(myTable.isFavorite);

            hasChanges = true;
        }
    }

    [Header("Table")]
    public GameObject tableObj;
    internal List<DraggableCardComponent> tablePanelsDraggables;
    public Texture[] deckCards;
    internal List<Sprite> deckCardsSprites;
    internal List<Builder_Table> myTablesList;
    internal Builder_Table selectedTable;
    public TMP_InputField tableNameInput;
    int tablesToLoad;

    public Button randomizeTableButton;

    DatabaseReference _playerTables_ref;

    private void Start()
    {
        // Textures to sprite
        deckCardsSprites = new List<Sprite>();
        foreach (var c in deckCards)
        {
            deckCardsSprites.Add(Sprite.Create((Texture2D)c, new Rect(0, 0, c.width, c.height), new Vector2(0.5f, 0.5f)));
        }

        // Get table image panels
        tablePanelsDraggables = new List<DraggableCardComponent>();
        foreach (Transform row in tableObj.transform)
        {
            foreach (Transform col in row)
            {
                tablePanelsDraggables.Add(col.gameObject.GetComponent<DraggableCardComponent>());
            }
        }

        // Fill drag cards scroll list
        draggableCardList = new List<DraggableCardComponent>();
        DraggableCardComponent newDragCard;
        foreach (Transform child in dragableCardsParent)
            Destroy(child.gameObject);
        GameObject newCard;
        foreach (var c in deckCardsSprites)
        {
            newCard = Instantiate(dragableCardPrefab, dragableCardsParent);

            newDragCard = newCard.GetComponent<DraggableCardComponent>();
            draggableCardList.Add(newDragCard);

            newDragCard.draggableImage = newCard.transform.GetChild(0).GetComponent<Image>();
            newDragCard.draggableImage.sprite = c;

            newDragCard.disabledImage = newCard.transform.GetChild(1).gameObject;

            newDragCard.cardIndex = deckCardsSprites.IndexOf(c);
            newDragCard.isTablePanel = false;
        }
    }

    public void OnCloseTableEditor()
    {
        StartCoroutine(SaveTables());
    }

    public void OnChangeTableName()
    {
        if(selectedTable != null && !string.IsNullOrEmpty(tableNameInput.text))
        {
            if(selectedTable.isSelected && selectedTable.myTable.name != tableNameInput.text)
            {
                selectedTable.myTable.name = tableNameInput.text;
                selectedTable.hasChanges = true;
            }
        }
    }

    IEnumerator SaveTables()
    {
        bool hasChanges = myTablesList.Count(x => x.hasChanges) > 0;

        app.mainController.ShowLoading("Guardando tablas");

        _playerTables_ref.ChildAdded -= HandleTableChildAdded;

        if(hasChanges)
        {
            List<Globals.Table> updateTables = new List<Globals.Table>();
            foreach (var t in myTablesList)
            {
                if (t.hasChanges)
                {
                    t.myTable.dateTime = System.DateTime.Now.ToString();

                    t.myTable.tablePanels = string.Empty;
                    foreach (var c in t.cardsList)
                        t.myTable.tablePanels += c + ",";
                    t.myTable.tablePanels = t.myTable.tablePanels.Remove(t.myTable.tablePanels.Length - 1);

                    updateTables.Add(t.myTable);
                }
            }

            StartCoroutine(SetTableToPlayer(updateTables, false));
        }
        
        yield return null;

        app.mainController.tableBuilderWindow.SetActive(false);
        if (!hasChanges)
            app.mainController.loadingPanel.SetActive(false);
    }

    public void FillPlayerTables()
    {
        currentPage = 0;
        tablesPagerButtons[0].SetActive(false);
        tablesPagerButtons[1].SetActive(false);

        randomizeTableButton.interactable = false;

        myTablesList = new List<Builder_Table>();

        // scroll card list to top
        cardsScrollRect.verticalNormalizedPosition = 1f;

        // Destroy tables on list
        foreach (Transform child in tablesOnListParent)
        {
            if (!child.name.Contains("NewTableButton"))
                Destroy(child.gameObject);
        }

        ResetTableWindow();

        tablesToLoad = 0;

        app.mainController.ShowLoading("Cargando tablas");

        _playerTables_ref = FirebaseDatabase.DefaultInstance.GetReference("Players/" + app.mainController.myPlayer.id + "/tablesDic");
        _playerTables_ref.GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                
                //print(snapshot.GetRawJsonValue());
                tablesToLoad = (int)snapshot.ChildrenCount;
                UnityMainThreadDispatcher.Instance().Enqueue(StartGetPlayerTables());
            }
        });
    }

    void ResetTableWindow()
    {
        tableNameInput.interactable = false;
        tableNameInput.text = string.Empty;

        for (int i = 0; i < tablePanelsDraggables.Count; i++)
        {
            tablePanelsDraggables[i].draggableImage.sprite = null;
            tablePanelsDraggables[i].disabledImage.SetActive(true);
        }

        for (int i = 0; i < draggableCardList.Count; i++)
        {
            draggableCardList[i].disabledImage.SetActive(true);
        }
    }

    IEnumerator StartGetPlayerTables()
    {
        _playerTables_ref.ChildAdded += HandleTableChildAdded;

        yield return null;
    }

    void HandleTableChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        //print(args.Snapshot.Key + " - " + args.Snapshot.GetRawJsonValue());

        // Create table
        GameObject newTableOnList = Instantiate(tableOnListPrefab, tablesOnListParent);
        Builder_Table newTableOnListObj = new Builder_Table(newTableOnList);
        newTableOnListObj.myTable = JsonUtility.FromJson<Globals.Table>(args.Snapshot.GetRawJsonValue());

        // Set firebase id
        newTableOnListObj.firebaseId = args.Snapshot.Key;

        // Set name
        newTableOnListObj.tableNameText.text = newTableOnListObj.myTable.name;

        // Set favorite button
        newTableOnListObj.ToggleFavorite();
        newTableOnListObj.ToggleFavorite();
        newTableOnListObj.hasChanges = false;
        newTableOnListObj.favoriteButton.onClick.AddListener(delegate{
            newTableOnListObj.ToggleFavorite();
        });

        // Set table panels
        string[] parts = newTableOnListObj.myTable.tablePanels.Replace("\"", "").Split(',');
        int cardId;
        for (int i = 0; i < parts.Length; i++)
        {
            cardId = int.Parse(parts[i]);
            newTableOnListObj.panelsImages[i].sprite = deckCardsSprites[cardId];

            newTableOnListObj.cardsList.Add(cardId);
        }
        // Set table selection
        newTableOnListObj.selectButton.onClick.RemoveAllListeners();
        newTableOnListObj.selectButton.onClick.AddListener(delegate {
            OnSelectTableOnList(newTableOnListObj);
        });
        myTablesList.Add(newTableOnListObj);

        // Hide buy button if max tables
        if(myTablesList.Count == 32)
            buyTableButton.gameObject.SetActive(false);

        // Put NEW PANEl to the end of the list
        newTablePanel.gameObject.SetActive(tablesOnListParent.childCount < 6);
        newTablePanel.SetAsLastSibling();

        tablesToLoad--;
        if(tablesToLoad <= 0)
        {
            app.mainController.loadingPanel.SetActive(false);
            ChangeTablesPage();
        }
    }

    public void OnChangeTablesPage(int val)
    {
        currentPage += val;
        ChangeTablesPage();
    }

    void ChangeTablesPage()
    {
        newTablePanel.gameObject.SetActive(false);

        foreach (var t in myTablesList)
        {
            if (t.isSelected)
                OnSelectTableOnList(t);
            t.myObj.SetActive(false);
        }
        for (int i = currentPage * 5; i < (currentPage * 5) + 5; i++)
        {
            if (myTablesList.Count <= i)
            {
                //show new button
                newTablePanel.gameObject.SetActive(true);
                newTablePanel.SetAsLastSibling();
                break;
            }
            myTablesList[i].myObj.SetActive(true);
        }

        // back page button
        tablesPagerButtons[0].SetActive(currentPage > 0);
        // next page button
        tablesPagerButtons[1].SetActive(myTablesList.Count >= (currentPage * 5) + 5);
    }

    public List<int> GenerateRandomTable()
    {
        List<int> remainCards = new List<int>();
        for (int i = 0; i < deckCardsSprites.Count; i++)
            remainCards.Add(i);

        List<int> currentTablePanels = new List<int>();
        int rand;
        for (int i = 0; i < tablePanelsDraggables.Count; i++)
        {
            rand = remainCards[Random.Range(0, remainCards.Count)];
            currentTablePanels.Add(rand);
            remainCards.Remove(rand);
        }

        return currentTablePanels;
    }

    public IEnumerator SetTableToPlayer(List<Globals.Table> tablesList, bool changeWindow)
    {
        app.mainController.ShowLoading("Creando tablas!");

        tablesToLoad = tablesList.Count;

        _playerTables_ref = FirebaseDatabase.DefaultInstance.GetReference("Players/" + app.mainController.myPlayer.id + "/tablesDic");

        string key = string.Empty;
        string json = string.Empty;

        for (int i = 0; i < tablesList.Count; i++)
        {
            if (tablesList[i] == null)
            {
                tablesList[i] = new Globals.Table() {
                    name = "Tabla_" + (myTablesList == null ? i.ToString() : myTablesList.Count.ToString()),
                    isFavorite = false,
                    tablePanels = string.Empty,
                    dateTime = System.DateTime.Now.ToString(),
                    victories = 0
                };

                foreach (var c in GenerateRandomTable())
                    tablesList[i].tablePanels += c + ",";
                tablesList[i].tablePanels = tablesList[i].tablePanels.Remove(tablesList[i].tablePanels.Length - 1);
                key = _playerTables_ref.Push().Key;
            }
            else
            {
                key = myTablesList.First(x => x.myTable == tablesList[i]).firebaseId;
            }

            json = JsonUtility.ToJson(tablesList[i]);
            json = json.Substring(0, json.Length - 1);
            json += @" , ""timestamp"": {"".sv"" : ""timestamp""} } ";

            print(json);
            _playerTables_ref.Child(key).SetRawJsonValueAsync(json, 1).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Table registered");
                    UnityMainThreadDispatcher.Instance().Enqueue(AfterTableSave());
                }
            });

            yield return null;
        }

        if(changeWindow)
        {
            // First time, give gift
            app.coinController.SetCoinTransaction("WelcomeGift", app.mainController.myPlayer.id, app.mainController.GIFT_WELCOME_COINS);
            app.promptComponent.ShowPrompt(
                "¡Bienvenido!",
                "Agradecemos que juegues Buenas Online!, ¡te damos de bienvenida $1,000 para que empieces a jugar!",
                Globals.PromptType.ConfirmMessage,
                null, Globals.PromptColors.Blue
            );
            // Start tutorial
            app.mainController.StartCoroutine(app.mainController.TutorialSequence());
            // Start game
            app.mainController.StartCoroutine(app.mainController.ChangeWindow());
            app.mainController.StartCoroutine(app.mainController.CompleteLogin());
        }
    }

    IEnumerator AfterTableSave()
    {
        tablesToLoad--;
        if(tablesToLoad <= 0)
            app.mainController.loadingPanel.SetActive(false);

        yield return null;
    }

    public void OnBuyNewTable()
    {
        app.promptComponent.ShowPrompt("Comprar nueva Tabla", "¿Desea comprar una tabla nueva por\n$500?", Globals.PromptType.YesNoMessage, "BuyTable", Globals.PromptColors.Green);
    }

    public void OnConfirmBuyNewTable()
    {
        buyTableButton.interactable = false;
        List<Globals.Table> newTable = new List<Globals.Table>() { null };
        StartCoroutine(SetTableToPlayer(newTable, false));
        app.coinController.SetCoinTransaction(app.mainController.myPlayer.id, "BuenasBank_BuyTabe", 500);
    }

    #region EDITOR
    void OnSelectTableOnList(Builder_Table _selectedTable)
    {
        foreach (var t in myTablesList)
        {
            if (t.isSelected && t != _selectedTable)
                OnSelectTableOnList(t);
        }

        selectedTable = _selectedTable;

        _selectedTable.isSelected = !_selectedTable.isSelected;

        tableNameInput.interactable = _selectedTable.isSelected;
        tableNameInput.text = _selectedTable.isSelected ? _selectedTable.myTable.name : string.Empty;

        _selectedTable.selectionImage.color = tableOnListSelectionColors[_selectedTable.isSelected ? 1 : 0];

        for (int i = 0; i < _selectedTable.cardsList.Count; i++)
        {
            tablePanelsDraggables[i].draggableImage.sprite = _selectedTable.isSelected ? deckCardsSprites[_selectedTable.cardsList[i]] : null;
            tablePanelsDraggables[i].disabledImage.SetActive(!_selectedTable.isSelected);
            tablePanelsDraggables[i].cardIndex = _selectedTable.cardsList[i];
            tablePanelsDraggables[i].isTablePanel = true;
        }

        for (int i = 0; i < draggableCardList.Count; i++)
        {
            draggableCardList[i].disabledImage.SetActive(_selectedTable.isSelected ? _selectedTable.cardsList.Contains(i) : true);
        }

        // Random buton
        randomizeTableButton.interactable = _selectedTable.isSelected;
    }

    public void OnRandomizeSelectedTable()
    {
        if (selectedTable == null)
            return;

        selectedTable.hasChanges = true;

        selectedTable.cardsList = GenerateRandomTable();
        OnSelectTableOnList(selectedTable);
        OnSelectTableOnList(selectedTable);
    }

    public void ChangeCardOnTable(int tablePanelIndex, int cardIndex, bool changePrev)
    {
        if(changePrev)
        {
            int prevCardIndex = deckCardsSprites.IndexOf(tablePanelsDraggables[tablePanelIndex].draggableImage.sprite);
            draggableCardList[prevCardIndex].disabledImage.SetActive(false);
        }

        tablePanelsDraggables[tablePanelIndex].draggableImage.sprite = deckCardsSprites[cardIndex];
        tablePanelsDraggables[tablePanelIndex].cardIndex = cardIndex;
        selectedTable.cardsList[tablePanelIndex] = cardIndex;
        draggableCardList[cardIndex].disabledImage.SetActive(true);

        selectedTable.hasChanges = true;
    }
    #endregion
}
