﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBackgroundComponent : MonoBehaviour
{
    public RectTransform movingPanelsRectT;
    Vector2 initPos = new Vector2(-1620, -1175);
    Vector2 endPos = new Vector2(1620, 1200);

    float moveTime = 15;
    float currentTime = 0;


    void Update()
    {
        if (movingPanelsRectT.transform.parent.parent.gameObject.activeSelf)
        {
            if (currentTime < moveTime)
            {
                movingPanelsRectT.localPosition = Vector2.Lerp(initPos, endPos, currentTime / moveTime);
                currentTime += Time.deltaTime;
            }
            else
            {
                currentTime = 0;
                movingPanelsRectT.localPosition = initPos;
            }
        }
    }
}
