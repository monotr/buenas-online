﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewPrivateRoomComponent : LoteriaOnlineElement
{
    public GameObject backButton;
    public TMP_Text titletext;
    public TMP_InputField roomNameInput;
    public TMP_InputField roomPasswordInput;

    internal bool isEditing = false;
    Globals.Room backupRoom;

    internal bool isCreatingWithBots = false;

    [Header("Max players")]
    public Slider maxPlayersSlider;
    public TMP_Text maxPlayersBubbleText;

    [Header("Cantada speed")]
    public Slider cantadaSpeedSlider;
    public TMP_Text cantadaSpeedBubbleText;
    internal List<int> cantadaSpeedsList = new List<int>() { 10, 8, 6, 4, 2 };
    public Color[] cantadaSpeedColors;

    [Header("Bet per Table")]
    public Slider betPerTableSlider;
    public TMP_Text betPerTableBubbleText;

    [Header("Victory types")]
    public Image[] victoryTypesButtonImages;
    List<int> victoryTypeSelected;
    public Color victoryTypeSelectionColor;
    public GameObject lineVictoryTable;
    public Color selectionColor;
    Coroutine lineVictoryTableAnimationRoutine;

    internal float hostingInitTime;

    public void InitNewPrivateRoom()
    {
        app.mainController.currentRoom = new Globals.Room()
        {
            playerHostId = app.mainController.myPlayer.id,
            maxPlayers = (int)maxPlayersSlider.value,
            cantadaSpeed = (int)cantadaSpeedSlider.value,
            state = "InLobby",
            calleddCardsDic = new Dictionary<string, object>(),
            playersIdDict = new Dictionary<string, object>(),
            accumulatedCoins = 0
        };

        isEditing = false;

        backButton.SetActive(true);
        titletext.text = "Nueva mesa";

        roomNameInput.text = string.Empty;
        roomNameInput.interactable = true;
        roomNameInput.Select();
        roomPasswordInput.text = string.Empty;
        roomPasswordInput.interactable = true;

        maxPlayersSlider.interactable = true;        
        betPerTableSlider.interactable = true;
        StartCoroutine(ResetSliderInputs());

        victoryTypeSelected = new List<int>();
        foreach (var v in victoryTypesButtonImages)
            v.color = Color.white;

        if(lineVictoryTableAnimationRoutine == null)
            lineVictoryTableAnimationRoutine = StartCoroutine(LineVictoryTableAnimation(lineVictoryTable));

        // Player stats - host time
        hostingInitTime = Time.time;

        //DEBUG
#if UNITY_EDITOR
        StartCoroutine(DebugFillFields());
#endif
        //END DEBUG
    }

    IEnumerator DebugFillFields()
    {
        yield return new WaitForSeconds(0.5f);

        // Room
        roomNameInput.text = "Testing123";
        roomPasswordInput.text = "123";
        maxPlayersSlider.value = 32;
        cantadaSpeedSlider.value = 4;
        OnSelectVictoryType(0);
    }

    IEnumerator ResetSliderInputs()
    {
        // A veces no hace el cambio inicial... se hizo esto por mientras (para siempre quizá...)
        maxPlayersSlider.value = maxPlayersSlider.maxValue;
        cantadaSpeedSlider.value = cantadaSpeedSlider.maxValue;
        betPerTableSlider.value = betPerTableSlider.maxValue;

        yield return null;

        // Set defaults values
        maxPlayersSlider.value = 16;
        cantadaSpeedSlider.value = 2;
        betPerTableSlider.value = 1;
    }

    public void InitChangeCurrentRoomRules()
    {
        isEditing = true;

        // Save starting rules
        backupRoom = new Globals.Room()
        {
            betPerTable = app.mainController.currentRoom.betPerTable + 0,
            cantadaSpeed = app.mainController.currentRoom.cantadaSpeed + 0,
            maxPlayers = app.mainController.currentRoom.maxPlayers + 0,
            victoryTypes = app.mainController.currentRoom.victoryTypes + ""
        };


        // Cancel countdown to next game
        if (app.gameController.nextGameCounterCoroutine != null)
        {
            app.gameController.StopCoroutine(app.gameController.nextGameCounterCoroutine);
            app.gameController.nextGameCounterCoroutine = null;
        }

        // Pause game
        if(app.mainController.gameState == MainController.GameState.Playing)
            app.privateRoomLobbyComponent.PauseGame("ChangingRules");

        app.mainController.createPrivateRoomWindow.SetActive(true);

        backButton.SetActive(false);
        titletext.text = "Mesa: " + app.mainController.currentRoom.name;

        roomNameInput.text = app.mainController.currentRoom.name;
        roomPasswordInput.text = app.mainController.currentRoom.password;

        maxPlayersSlider.value = app.mainController.currentRoom.maxPlayers;
        cantadaSpeedSlider.value = cantadaSpeedsList.IndexOf((int)app.mainController.currentRoom.cantadaSpeed);

        victoryTypeSelected = new List<int>();
        foreach (var v in victoryTypesButtonImages)
            v.color = Color.white;

        string[] parts = app.mainController.currentRoom.victoryTypes.Split(',');
        foreach (var p in parts)
            OnSelectVictoryType(int.Parse(p));

        if (lineVictoryTableAnimationRoutine == null)
            lineVictoryTableAnimationRoutine = StartCoroutine(LineVictoryTableAnimation(lineVictoryTable));
    }

    public IEnumerator LineVictoryTableAnimation(GameObject _lineVictoryTable)
    {
        List<Image> lineVictoryTablePanels = new List<Image>();

        foreach (Transform row in _lineVictoryTable.transform)
        {
            foreach (Transform column in row)
            {
                lineVictoryTablePanels.Add(column.GetComponentInChildren<Image>());
            }
        }

        while (true)
        {
            // Horizontal / Vertical
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int v = 0; v < 4; v++)
                    {
                        for (int p = 0; p < lineVictoryTablePanels.Count; p++)
                        {
                            lineVictoryTablePanels[p].color = app.gameController.victoriesList[i + (j * 2)].Contains(p) ? selectionColor : Color.white;
                        }
                    }
                    yield return new WaitForSeconds(1f);
                }
            }
            // Diagonales
            for (int i = 0; i < 2; i++)
            {
                for (int p = 0; p < lineVictoryTablePanels.Count; p++)
                {
                    lineVictoryTablePanels[p].color = app.gameController.victoriesList[i + 8].Contains(p) ? selectionColor : Color.white;
                }
                yield return new WaitForSeconds(1f);
            }
            yield return null;
        }
    }

    public void OnSelectVictoryType(int type)
    {
        if(victoryTypeSelected.Contains(type))
        {
            victoryTypeSelected.Remove(type);
            victoryTypesButtonImages[type].color = Color.white;
        }
        else
        {
            victoryTypeSelected.Add(type);
            victoryTypesButtonImages[type].color = victoryTypeSelectionColor;
        }
    }

    public void OnCloseCreatePrivateRoom()
    {
        if(lineVictoryTableAnimationRoutine != null)
        {
            StopCoroutine(lineVictoryTableAnimationRoutine);
            lineVictoryTableAnimationRoutine = null;
        }

        app.mainController.createPrivateRoomWindow.SetActive(false);
        app.mainController.gameState = MainController.GameState.LoggedIn;
        app.mainController.StartCoroutine(app.mainController.ChangeWindow());
    }

    public void OnConfirmPrivateRoomRules()
    {
        if (string.IsNullOrEmpty(roomNameInput.text))
        {
            app.promptComponent.ShowPrompt("Datos incompletos", "Debe ingresar un nombre a la sala", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            roomNameInput.Select();
            return;
        }
        else if(roomNameInput.text != app.globalChatComponent.BadWordValidator(roomNameInput.text))
        {
            app.promptComponent.ShowPrompt("Datos innapropiados", "Favor de no usar malas palabras", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            roomNameInput.text = string.Empty;
            roomNameInput.Select();
            return;
        }
        else if(victoryTypeSelected.Count == 0)
        {
            app.promptComponent.ShowPrompt("Datos incompletos", "Debe seleccionar al menos un tipo de victoria", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            return;
        }

        if (lineVictoryTableAnimationRoutine != null)
        {
            StopCoroutine(lineVictoryTableAnimationRoutine);
            lineVictoryTableAnimationRoutine = null;
        }

        // Check if name exists
        var room_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms");
        room_ref.GetValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                DataSnapshot snapshot = t.Result;

                //print(snapshot.GetRawJsonValue().ToString());
                UnityMainThreadDispatcher.Instance().Enqueue(CheckRepeatedName(snapshot.Children, isEditing));
            }
        });
    }

    IEnumerator CheckRepeatedName(IEnumerable<DataSnapshot> snapshotChildren, bool isEdit)
    {
        Dictionary<string, object> dic;
        bool nameRepeated = false;
        foreach (DataSnapshot child in snapshotChildren)
        {
            //print(child + " -- " + child.Value);
            dic = (Dictionary<string, object>)child.Value;

            foreach (var c in dic)
            {
                if (c.Key == "name")
                {
                    if((string)c.Value == roomNameInput.text)
                    {
                        nameRepeated = true;
                        break;
                    }
                }
                if (nameRepeated) break;
            }
        }

        yield return null;

        if (!nameRepeated || app.mainController.currentRoom.name == roomNameInput.text)
        {
            if(!isEdit)
            {
                if (lineVictoryTableAnimationRoutine != null)
                {
                    StopCoroutine(lineVictoryTableAnimationRoutine);
                    lineVictoryTableAnimationRoutine = null;
                }

                // set name, password, victory, cantadaSpeed types to new room
                app.mainController.currentRoom.name = roomNameInput.text;
                app.mainController.currentRoom.password = roomPasswordInput.text;
                SetVictoryTypes();

                app.mainController.createPrivateRoomWindow.SetActive(false);

                CreateNewRoom(false);
            }
            else
            {
                SetVictoryTypes();

                Dictionary<string, object> childUpdates = new Dictionary<string, object>();
                childUpdates["/name"] = roomNameInput.text;
                childUpdates["/password"] = roomPasswordInput.text;
                childUpdates["/cantadaSpeed"] = (int)app.mainController.currentRoom.cantadaSpeed;
                childUpdates["/maxPlayers"] = app.mainController.currentRoom.maxPlayers;
                childUpdates["/betPerTable"] = app.mainController.currentRoom.betPerTable;
                childUpdates["/victoryTypes"] = app.mainController.currentRoom.victoryTypes;

                app.mainController.createPrivateRoomWindow.SetActive(false);

                var room_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms");
                room_ref.Child(app.mainController.currentRoom.id).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                    if (t.IsFaulted)
                    {
                        Debug.Log("Faulted.." + t.Exception.Message);
                    }

                    if (t.IsCanceled)
                    {
                        Debug.Log("Cancelled..");
                    }

                    if (t.IsCompleted)
                    {
                        print("edit room rules complete");
                        UnityMainThreadDispatcher.Instance().Enqueue(CheckIfStartGameCounter());
                    }
                });

                // Send rules change messages
                if (backupRoom.maxPlayers != app.mainController.currentRoom.maxPlayers)
                    StartCoroutine(SendRuleChange("JUG. MAX: ", app.mainController.currentRoom.maxPlayers.ToString()));
                if (backupRoom.cantadaSpeed != app.mainController.currentRoom.cantadaSpeed)
                    StartCoroutine(SendRuleChange("VELOCIDAD: ", app.mainController.currentRoom.cantadaSpeed.ToString()));
                if (backupRoom.betPerTable != app.mainController.currentRoom.betPerTable)
                    StartCoroutine(SendRuleChange("APUESTA: $", app.mainController.currentRoom.betPerTable.ToString()));
                if (backupRoom.victoryTypes != app.mainController.currentRoom.victoryTypes)
                    StartCoroutine(SendRuleChange("TIPO VICTORIA", null));
            }
        }
        else
        {
            app.promptComponent.ShowPrompt("Nombre en uso", "Ya existe una mesa con el nombre ingresado, favor de cambiarlo.", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
        }
    }

    IEnumerator CheckIfStartGameCounter()
    {
        yield return null;

        if (app.mainController.gameState == MainController.GameState.Playing)
            app.gameController.StartCoroutine(app.gameController.StartNextGameCounter());
    }

    public void CreateNewRoom(bool createBots)
    {
        isCreatingWithBots = createBots;
        app.mainController.ShowLoading("Creando nueva mesa");

        var _rooms_ref = app.mainController.mDatabaseRef.Child("PrivateRooms");
        string key = _rooms_ref.Push().Key;

        // Set id to room
        app.mainController.currentRoom.id = key;

        string json = JsonUtility.ToJson(app.mainController.currentRoom);

        _rooms_ref.Child(key).SetRawJsonValueAsync(json, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Room registered");
                UnityMainThreadDispatcher.Instance().Enqueue(app.mainController.OpenPrivateRoomLobbyWindow());
            }
        });
    }

    void SetVictoryTypes()
    {
        app.mainController.currentRoom.victoryTypes = string.Empty;
        foreach (var vs in victoryTypeSelected)
            app.mainController.currentRoom.victoryTypes += vs + ",";
        app.mainController.currentRoom.victoryTypes = app.mainController.currentRoom.victoryTypes.Remove(app.mainController.currentRoom.victoryTypes.Length - 1);
    }

    public void OnChangeMaxPlayersSlider(Single val)
    {
        if(isEditing && val < app.mainController.currentRoom.playerCount)
        {
            maxPlayersSlider.value = app.mainController.currentRoom.playerCount;
            return;
        }
        
        maxPlayersBubbleText.text = val.ToString();

        app.mainController.currentRoom.maxPlayers = (int)val;
    }

    public void OnChangeCantadaSpeedSlider(Single val)
    {
        // 0 - super lento | 1 - lento | 2 - normal | 3 - rápido | 4 - turbo
        cantadaSpeedBubbleText.text = cantadaSpeedsList[(int)val] + " s";

        app.mainController.currentRoom.cantadaSpeed = cantadaSpeedsList[(int)val];
    }

    public void OnChangeBetPerTableSlider(Single val)
    {
        betPerTableBubbleText.text = "$" + (val * 10);

        app.mainController.currentRoom.betPerTable = (int)(val * 10);
    }

    IEnumerator SendRuleChange(string ruleType, string val)
    {
        string key = app.privateRoomLobbyComponent._rulesChanges_ref.Push().Key;

        Dictionary<string, object> childUpdates = new Dictionary<string, object>();

        childUpdates["/ruleType"] = ruleType;
        if(!string.IsNullOrEmpty(val))
            childUpdates["/val"] = val;

        app.privateRoomLobbyComponent._rulesChanges_ref.Child(key).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Rascada sent");
            }
        });

        yield return new WaitForSeconds(1f);

        app.privateRoomLobbyComponent._rulesChanges_ref.Child(key).RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Rascada removed");
            }
        });
    }
}
