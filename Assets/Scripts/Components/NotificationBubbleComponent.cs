﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NotificationBubbleComponent : LoteriaOnlineElement
{
    internal bool isInit = false;
    internal RawImage mainIcon;
    RawImage secondIcon;
    TMP_Text messageText;
    RectTransform rectT;
    Vector2 finalSize;
    Vector3 initialPos, finalPos;
    float currenZoomAnimTime = 0;
    float currenMoveAnimTime = 0;
    float zoomTimer = 2;
    float moveUpTimer = 6;

    List<string> gameOrigins = new List<string>(){
        "MinuteGift", "WelcomeGift", "VideoAdReward", "InAppPurchase"
    };

    private void Awake()
    {
        mainIcon = transform.GetChild(0).GetChild(0).GetComponent<RawImage>();
        secondIcon = transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RawImage>();
        messageText = transform.GetChild(1).GetComponent<TMP_Text>();

        rectT = GetComponent<RectTransform>();
        rectT.sizeDelta = Vector2.zero;
    }

    public void Init(Globals.NotificationType _type, float initPosX, Texture _texture, string _message, string _playerId)
    {
        initialPos = Vector3.right * initPosX;
        finalPos = initialPos + (Vector3.up * 1920 * 1.1f);

        if(app == null)
            app = FindObjectOfType<LoteriaOnlineApplication>();

        finalSize = Vector2.one * app.notificationsController.notificationSizes[(int)_type];

        messageText.text = _message;

        switch (_type)
        {
            case Globals.NotificationType.CoinsIn:
            case Globals.NotificationType.CoinsOut:
                if(gameOrigins.Contains(_playerId) || string.IsNullOrEmpty(_playerId))
                {
                    mainIcon.texture = app.notificationsController.notificationIcons[0];
                    secondIcon.texture = app.notificationsController.notificationSecondIcons[_type == Globals.NotificationType.CoinsIn ? 0 : 1];
                    secondIcon.color = app.notificationsController.notificationSecondColors[_type == Globals.NotificationType.CoinsIn ? 0 : 1];
                }
                else if (!string.IsNullOrEmpty(_playerId))
                {
                    string _color = ColorUtility.ToHtmlStringRGB(app.notificationsController.notificationSecondColors[_message.Contains("-") ? 1 : 0]);
                    _message = "<size=40><color=#" + _color + ">" + _message;

                    secondIcon.texture = app.notificationsController.notificationIcons[0];

                    if(app.mainController.gameState == MainController.GameState.Playing)
                    {
                        if(!app.privateRoomLobbyComponent.playersOnListDic.ContainsKey(_playerId))
                        {
                            mainIcon.texture = app.privateRoomLobbyComponent.accumulatedSprite.texture;
                            messageText.text = "ACUMULADO" + "\n" + _message;
                        }
                        else
                        {
                            mainIcon.texture = app.privateRoomLobbyComponent.playersOnListDic[_playerId].avatarImage.texture;
                            messageText.text = app.privateRoomLobbyComponent.playersOnListDic[_playerId].nameText.text + "\n" + _message;
                        }
                    }
                    else if(app.mainController.profileWindow.activeSelf)
                    {
                        mainIcon.texture = app.profileController.profileImage.sprite.texture;
                        messageText.text = app.profileController.playerNicknameText.text + "\n" + _message;
                    }
                    else
                    {
                        var fol = app.friendsComponent.friendOnListList.First(x => x.myPlayer.id == _playerId);
                        mainIcon.texture = fol.playerPhotoImage.sprite.texture;
                        messageText.text = fol.myPlayer.name + "\n" + _message;
                    }
                }
                break;
            case Globals.NotificationType.PlayerIn:
                mainIcon.texture = _texture;
                secondIcon.texture = app.notificationsController.notificationSecondIcons[0];
                secondIcon.color = app.notificationsController.notificationSecondColors[0];

                // if bot, change color
                if(_playerId.Substring(0,4).Contains("Bot_"))
                {
                    string[] parts = _playerId.Split('_');
                    mainIcon.color = app.botWindowComponent.dificultyColors[int.Parse(parts[2])];
                }
                break;
            case Globals.NotificationType.PlayerOut:
                mainIcon.texture = _texture;
                secondIcon.texture = app.notificationsController.notificationSecondIcons[1];
                secondIcon.color = app.notificationsController.notificationSecondColors[1];

                // if bot, change color
                if (_playerId.Substring(0, 4).Contains("Bot_"))
                {
                    string[] parts = _playerId.Split('_');
                    mainIcon.color = app.botWindowComponent.dificultyColors[int.Parse(parts[2])];
                }
                break;
            case Globals.NotificationType.Emojee:
                secondIcon.texture = _texture;
                mainIcon.texture = app.privateRoomLobbyComponent.playersOnListDic[_playerId].avatarImage.texture;
                messageText.text = app.privateRoomLobbyComponent.playersOnListDic[_playerId].nameText.text;
                break;
            case Globals.NotificationType.LevelUp:
                mainIcon.texture = app.notificationsController.notificationIcons[1];
                secondIcon.texture = app.notificationsController.notificationSecondIcons[0];
                secondIcon.color = app.notificationsController.notificationSecondColors[0];
                break;
            case Globals.NotificationType.Experience:
                mainIcon.texture = app.notificationsController.notificationIcons[2];
                secondIcon.texture = app.notificationsController.notificationSecondIcons[0];
                secondIcon.color = app.notificationsController.notificationSecondColors[0];
                break;
            case Globals.NotificationType.Rascada:
                secondIcon.texture = _texture;
                mainIcon.texture = app.privateRoomLobbyComponent.playersOnListDic[_playerId].avatarImage.texture;
                messageText.text = app.privateRoomLobbyComponent.playersOnListDic[_playerId].nameText.text;
                break;
            case Globals.NotificationType.RulesChange:
                mainIcon.texture = app.notificationsController.notificationIcons[3];
                mainIcon.color = Color.gray;
                secondIcon.gameObject.SetActive(false);
                messageText.text = _message;
                break;
            case Globals.NotificationType.FriendRequest:
                mainIcon.texture = app.notificationsController.notificationIcons[4];
                secondIcon.texture = app.notificationsController.notificationSecondIcons[2];
                secondIcon.color = Color.white;
                messageText.text = _message;
                break;
            case Globals.NotificationType.FriendRequestAccepted:
                mainIcon.texture = app.notificationsController.notificationIcons[4];
                secondIcon.texture = app.notificationsController.notificationSecondIcons[0];
                secondIcon.color = app.notificationsController.notificationSecondColors[0];
                messageText.text = _message;
            break;
            case Globals.NotificationType.TableInvitation:
                mainIcon.texture = app.notificationsController.notificationIcons[5];
                secondIcon.texture = app.notificationsController.notificationSecondIcons[0];
                secondIcon.color = app.notificationsController.notificationSecondColors[0];
                messageText.text = _message;
                break;
            default:
                break;
        }

        Destroy(gameObject, 30);
    }

    public void SetMainIcon(Texture icon)
    {
        mainIcon.texture = icon;
    }

    private void Update()
    {
        if (!isInit)
            return;

        // Zoom in
        if (rectT.sizeDelta.x < finalSize.x)
        {
            currenZoomAnimTime += Time.deltaTime;

            rectT.sizeDelta = Vector2.Lerp(
                rectT.sizeDelta,
                finalSize,
                currenZoomAnimTime / zoomTimer
            );
        }
        if (currenMoveAnimTime < moveUpTimer)
        {
            // Move up
            currenMoveAnimTime += Time.deltaTime;
            rectT.localPosition = Vector3.Lerp(
                initialPos
                , finalPos
                , currenMoveAnimTime / moveUpTimer
            );
            // Zigzag
            rectT.localPosition = new Vector3(Mathf.Sin(Mathf.PI * rectT.localPosition.y / 250) * 45, rectT.localPosition.y, rectT.localPosition.z);
        }
        else // If way up, destroy
        {
            Destroy(gameObject);
        }
    }
}
