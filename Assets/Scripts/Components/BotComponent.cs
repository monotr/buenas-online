﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotComponent : LoteriaOnlineElement
{
    internal string myName;
    internal string myFbKey;
    string botPlayerId;
    List<GameController.Game_Table> myTables;
    internal int dificulty; // 0 - Easy | 1 - Medium | 2 -Hard | 3 - Impusibru
    float eyeSpeed;

    internal KeyValuePair<int, int> lastWinningTable; // max matches - count of tables with max matches
    internal int victoryIndex;
    internal GameController.Game_Table lastPlayedTable;

    internal Coroutine playRoutine;

    public void InitBot(int _id, int _dificulty, int _tablesQty, float _eyeSpeed)
    {
        dificulty = _dificulty;
        eyeSpeed = _eyeSpeed;

        myName = "Bot_" + _id;
        myFbKey = app.privateRoomLobbyComponent._players_ref.Push().Key;
        botPlayerId = "Bot_" + app.mainController.myPlayer.id + "_" + _dificulty + "_" + myFbKey;

        // Set id on playerList
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        childUpdates["/playerId"] = botPlayerId;
        childUpdates["/tablesQty"] = _tablesQty;
        app.privateRoomLobbyComponent._players_ref.Child(myFbKey).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Bot in room registered");
            }
        });

        CreateBotTables(_tablesQty);
    }

    void CreateBotTables(int _tablesQty)
    {
        myTables = new List<GameController.Game_Table>();

        GameObject newBotTable;
        GameController.Game_Table game_Table;
        for (int i = 0; i < _tablesQty; i++)
        {
            newBotTable = Instantiate(app.gameController.tablePrefab, app.privateRoomLobbyComponent.botTablesParent);
            game_Table = new GameController.Game_Table(newBotTable);
            game_Table.cardsList = app.tableBuilderComponent.GenerateRandomTable();
            for (int j = 0; j < game_Table.panelsImages.Count; j++)
            {
                game_Table.panelsImages[j].sprite = app.tableBuilderComponent.deckCardsSprites[game_Table.cardsList[j]];
            }
            newBotTable.SetActive(false);
            myTables.Add(game_Table);
        }
    }

    public void StartPlaySimulation(bool resetTables)
    {
        if (playRoutine != null)
            StopCoroutine(playRoutine);

        if(resetTables)
        {
            // reset playable cards
            foreach (var table in myTables)
                foreach (var panel in table.disabledPanels)
                    panel.SetActive(false);

            lastWinningTable = new KeyValuePair<int, int>(0, 0);
        }

        playRoutine = StartCoroutine(PlaySimulation());
    }

    IEnumerator PlaySimulation()
    {
        //print("Preparing bot simulation");
        yield return new WaitForSeconds((app.mainController.currentRoom.cantadaSpeed) + 3); // 3 del countdown
        //print("Starting bot simulation");

        int tableIndex, panelIndex, cardIndex;
        GameObject panel;
        List<GameObject> panels;
        while (app.gameController.inGame)
        {
            yield return new WaitForSeconds(app.mainController.currentRoom.cantadaSpeed * eyeSpeed);

            tableIndex = Random.Range(0, myTables.Count);
            panels = new List<GameObject>();
            foreach (var dp in myTables[tableIndex].disabledPanels)
            {
                if (!dp.activeSelf)
                    panels.Add(dp);
            }

            if(panels.Count > 0)
            {
                panel = panels[Random.Range(0, panels.Count)];

                panelIndex = panel.transform.parent.GetSiblingIndex() + (panel.transform.parent.parent.GetSiblingIndex() * 4);
                cardIndex = myTables[tableIndex].cardsList[panelIndex];
                //print(panelIndex + " ---- " + myTablesDict[result.gameObject.transform.parent.parent.gameObject].cardsList[panelIndex]);
                //print("Bot choose table " + tableIndex + " panel " + panelIndex);
                if (app.gameController.calledCards.Contains(cardIndex))
                {
                    myTables[tableIndex].disabledPanels[panelIndex].SetActive(true);
                    //print("-----Was called!");
                    app.gameController.StartCoroutine(app.gameController.VictoryCheck(myFbKey, this, myTables[tableIndex]));
                }
            }
        }
        //print("Exited simulation");
    }

    public void OnGritarBuenas()
    {
        StartCoroutine(GritarBuenas());
    }

    IEnumerator GritarBuenas()
    {
        print("BOT BUENAS");
        foreach (var bot in app.privateRoomLobbyComponent.myBots)
        {
            if (bot.playRoutine != null)
            {
                bot.StopCoroutine(bot.playRoutine);
                bot.playRoutine = null;
            }
        }

        yield return new WaitForSeconds(Random.Range(1, app.mainController.currentRoom.cantadaSpeed * 2));

        // Subir buenas a game session
        string tablePanels  = string.Empty;
        foreach (var c in lastPlayedTable.cardsList)
            tablePanels += c + ",";
        tablePanels = tablePanels.Remove(tablePanels.Length - 1);
        Globals.Buenas buenas = new Globals.Buenas()
        {
            playerId = botPlayerId,
            winningTable = tablePanels,
            victoryIndex = victoryIndex,
            winningTableId = null,
            victoryType = victoryIndex <= 8 ? 0 : victoryIndex - 9, // 0 a 9: Lineas | 10 - Esquinas | 11 - Pozito | 12 - Llena
            botName = myName,
            accumulativeCoinId = app.privateRoomLobbyComponent.botsAccumulativePlayerFbId
        };
        string json = JsonUtility.ToJson(buenas);
        print(json);
        app.gameController._room_buenas_ref.SetRawJsonValueAsync(json, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Buenas screamed by Bot");
            }
        });
    }

    public void DestroyBot(bool kicked)
    {
        if (playRoutine != null)
            StopCoroutine(playRoutine);

        if(kicked)
            app.privateRoomLobbyComponent._players_ref.Child(myFbKey).RemoveValueAsync();
        
        Destroy(gameObject);
        //print("Bot: why everything turning dark?... why am I feeling tired?... what is hap............");
    }
}
