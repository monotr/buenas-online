﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BotWindowComponent : LoteriaOnlineElement
{
    public GameObject botWindow;
    public TMP_InputField[] dificultiesInputs;
    public GameObject[] allButtonsList;

    //UI Raycast
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    int currentDificulty, currentBtnIndex;
    List<int> currentBotQtys;
    Coroutine autoIncreseRoutine, inputTextAnimRoutine;
    bool isAutoincreasing = false;

    public Color[] dificultyColors;
    List<int> dificultyTableQtys = new List<int>() { 4, 5, 6 };
    List<float> dificultyEyeSpeeds = new List<float>() { 0.25f, 0.083f, 0.0210f };

    int MAX_BOTS_ALLOWED = 16;

    public enum AutoIncreaseType { Bot, SendCoins }

    private void Start()
    {
        botWindow.SetActive(false);
        m_Raycaster = FindObjectOfType<Canvas>().GetComponent<GraphicRaycaster>();
        m_EventSystem = FindObjectOfType<Canvas>().GetComponent<EventSystem>();
    }

    private void Update()
    {
        if (botWindow.activeSelf)
        {
            m_PointerEventData = new PointerEventData(m_EventSystem);

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                TouchLogic(m_PointerEventData, touch.position, touch.phase == TouchPhase.Began, touch.phase == TouchPhase.Stationary, touch.phase == TouchPhase.Ended, AutoIncreaseType.Bot);
            }
#else
            TouchLogic(m_PointerEventData, Input.mousePosition, Input.GetMouseButtonDown(0), Input.GetMouseButton(0), Input.GetMouseButtonUp(0), AutoIncreaseType.Bot);
#endif
        }
    }

    public void TouchLogic(PointerEventData _m_PointerEventData, Vector2 position, bool isDown, bool isStay, bool isUp, AutoIncreaseType autoIncreaseType)
    {
        _m_PointerEventData.position = position;

        List<RaycastResult> results = new List<RaycastResult>();

        m_Raycaster.Raycast(_m_PointerEventData, results);

        if (results.Count(x => x.gameObject.tag == "DificultyButton") > 0)
        {
            if (isDown)
            {
                switch (autoIncreaseType)
                {
                    case AutoIncreaseType.Bot:
                        currentBtnIndex = allButtonsList.ToList().IndexOf(results.First(x => x.gameObject.tag == "DificultyButton").gameObject);
                        currentDificulty = currentBtnIndex / 2;
                        currentBtnIndex = currentBtnIndex % 2;

                        //print("difficulty: " + currentDificulty + "  -  btnIndex: " + currentBtnIndex);
                        break;
                    case AutoIncreaseType.SendCoins:
                        currentBtnIndex = (results.Count(x => x.gameObject.name == "AddButton") > 0) ? 1 : -1;
                        break;
                }

                if (autoIncreseRoutine != null) StopCoroutine(autoIncreseRoutine);
                isAutoincreasing = true;
                autoIncreseRoutine = StartCoroutine(AutoIncreaseInputValue(autoIncreaseType));
            }
            else if (isStay)
            {

            }
            else if (isUp)
            {
                if (autoIncreseRoutine != null) StopCoroutine(autoIncreseRoutine);
                isAutoincreasing = false;
            }
        }
        else
        {
            if (autoIncreseRoutine != null) StopCoroutine(autoIncreseRoutine);
            isAutoincreasing = false;
        }
    }

    IEnumerator AutoIncreaseInputValue(AutoIncreaseType autoIncreaseType)
    {
        float waitTime = 0.3f;
        float minTIme = 0;

        do
        {
            switch (autoIncreaseType)
            {
                case AutoIncreaseType.Bot:
                    // Check if can add bots
                    int availableBotSlots = GetAvailableBotSlots();
                    if ((availableBotSlots <= 0 || currentBotQtys.Sum(x => x) == MAX_BOTS_ALLOWED) && currentBtnIndex > 0)
                        isAutoincreasing = false;
                    else
                    {
                        currentBotQtys[currentDificulty] += (currentBtnIndex == 0 ? -1 : 1);
                        // Lower than zero
                        currentBotQtys[currentDificulty] = currentBotQtys[currentDificulty] < 0 ? 0 : currentBotQtys[currentDificulty];
                        FillChangingInput(dificultiesInputs[currentDificulty], currentBotQtys[currentDificulty].ToString());
                    }
                    minTIme = 0.05f;
                    break;
                case AutoIncreaseType.SendCoins:
                    app.sendCoinsComponent.sendingCoins += currentBtnIndex;
                    if(app.mainController.myPlayer.coins <= app.sendCoinsComponent.sendingCoins)
                    {
                        app.sendCoinsComponent.sendingCoins = app.mainController.myPlayer.coins;
                        isAutoincreasing = false;
                    }
                    else if (app.sendCoinsComponent.sendingCoins <= 0)
                    {
                        app.sendCoinsComponent.sendingCoins = 0;
                        isAutoincreasing = false;
                    }
                    FillChangingInput(app.sendCoinsComponent.coinsInput, app.sendCoinsComponent.sendingCoins.ToString());
                    minTIme = 0.01f;
                    break;
            }

            yield return new WaitForSeconds(waitTime);
            waitTime -= 0.05f;
            if (waitTime < minTIme) waitTime = minTIme;
            
        }
        while (isAutoincreasing);
    }

    public void OpenBotWindow()
    {
        botWindow.SetActive(true);

        app.reactionsController.reactionsButton.SetActive(false);

        var _easyBots = app.privateRoomLobbyComponent.myBots.Where(x => x.dificulty == 0);
        var _mediumBots = app.privateRoomLobbyComponent.myBots.Where(x => x.dificulty == 1);
        var _hardBots = app.privateRoomLobbyComponent.myBots.Where(x => x.dificulty == 2);
        //var _imposibruBots = app.privateRoomLobbyComponent.myBots.Where(x => x.dificulty == 3);

        currentBotQtys = new List<int>() { _easyBots.Count(), _mediumBots.Count(), _hardBots.Count() };

        //DEBUG
#if UNITY_EDITOR
        currentBotQtys = new List<int>() { 10, 4, 2 };
#endif
        //END DEBUG

        for (int i = 0; i < dificultiesInputs.Length; i++)
        {
            FillChangingInput(dificultiesInputs[i], currentBotQtys[i].ToString());
        }
        isAutoincreasing = false;
    }

    public void ResetBots()
    {
        currentBotQtys = new List<int>();
        for (int i = 0; i < dificultiesInputs.Length; i++)
        {
            currentBotQtys.Add(0);
            FillChangingInput(dificultiesInputs[i], currentBotQtys[i].ToString());
        }
    }

    public void OnConfirmBots()
    {
        StartCoroutine(ConfirmBots());

        CloseWindow();
    }

    public void CloseWindow()
    {
        botWindow.SetActive(false);
        app.reactionsController.reactionsButton.SetActive(true);
    }

    IEnumerator ConfirmBots()
    {
        yield return null;

        int botsQty = currentBotQtys.Sum(x => x);
        int availableBotSlots = GetAvailableBotSlots() + botsQty;
        int currentBotsCount;
        int dif = 0;
        if (availableBotSlots >= botsQty) // enough slots for the bots
        {
            for (int i = 0; i < currentBotQtys.Count; i++)
            {
                currentBotsCount = app.privateRoomLobbyComponent.myBots.Count(x => x.dificulty == i);
                dif = currentBotQtys[i] - currentBotsCount;

                if (dif < 0)
                {
                    for (int j = 0; j < Mathf.Abs(dif); j++)
                    {
                        app.privateRoomLobbyComponent.DestroyBot(app.privateRoomLobbyComponent.myBots.First(x => x.dificulty == i).myFbKey);
                        yield return new WaitForSeconds(0.05f);
                    }
                }
                else if (dif > 0)
                {
                    for (int j = 0; j < dif; j++)
                    {
                        app.privateRoomLobbyComponent.CreateBot(i, dificultyTableQtys[i], dificultyEyeSpeeds[i]);
                        yield return new WaitForSeconds(0.05f);
                    }
                }
            }
        }
        else // the room capacity changed
        {
            for (int i = 0; i < currentBotQtys.Count; i++)
            {
                while (currentBotQtys[i] > 0 && (currentBotQtys.Sum(x => x) - (GetAvailableBotSlots() + currentBotQtys.Sum(x => x))) > 0)
                {
                    currentBotQtys[i]--;
                }
            }

            if (currentBotQtys.Sum(x => x) > 0)
            {
                StartCoroutine(ConfirmBots());
                app.promptComponent.ShowPrompt("Cupo alcanzado", "Se agregaron menos bots ya que se llenó la mesa", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Green);
            }
            else
            {
                app.promptComponent.ShowPrompt("Cupo alcanzado", "La mesa está llena, ya no caben bots", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            }
        }
    }

    public void CreateDefaultBots()
    {
        currentBotQtys = new List<int>() { 4, 0, 0 };
        StartCoroutine(ConfirmBots());
    }

    void FillChangingInput(TMP_InputField _InputField, string _value)
    {
        _InputField.text = _value;

        if (inputTextAnimRoutine != null) StopCoroutine(inputTextAnimRoutine);
        inputTextAnimRoutine = StartCoroutine(InputTextAnimation(_InputField.transform.GetChild(0).GetChild(2).GetComponent<TMP_Text>()));
    }

    IEnumerator InputTextAnimation(TMP_Text inputText)
    {
        float animTime = 0.1f;
        float currentTime = 0;

        while (currentTime < animTime)
        {
            inputText.fontSize = Mathf.Lerp(50, 70, currentTime / animTime);
            currentTime += Time.deltaTime;
            yield return null;
        }
        inputText.fontSize = 70;

        yield return null;

        currentTime = 0;
        while (currentTime < animTime)
        {
            inputText.fontSize = Mathf.Lerp(70, 50, currentTime / animTime);
            currentTime += Time.deltaTime;
            yield return null;
        }
        inputText.fontSize = 50;
    }

    int GetAvailableBotSlots()
    {
        return app.mainController.currentRoom.maxPlayers -
                app.privateRoomLobbyComponent.playersInRoomList.Count(x => !x.playerId.Substring(0, 4).Contains("Bot_")) -
                currentBotQtys.Sum(x => x);
    }

    public Color GetDificultyColorFromBotId(string botId)
    {
        string[] parts = botId.Split('_');
        int colorId = 0;
        if(int.TryParse(parts[2], out colorId))
            return app.botWindowComponent.dificultyColors[colorId];
        return Color.white;
    }
}
