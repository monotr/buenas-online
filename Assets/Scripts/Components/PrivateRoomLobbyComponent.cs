﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VivoxUnity;

public class PrivateRoomLobbyComponent : LoteriaOnlineElement
{
    internal RectTransform lobbyWindowRectT;
    public Button hideLobbyWindowButton;

    public GameObject playerOnListPrefab;
    public RectTransform playerOnListParent;

    [Header("Notifications")]
    internal Dictionary<string, NotificationBubbleComponent> pendingPhotoNotifications;
    internal bool notificationsEnabled;
    int initialPlayerCount;

    internal DatabaseReference _players_ref;
    internal DatabaseReference _room_ref;
    DatabaseReference _player_room_ref;
    internal DatabaseReference _room_state_ref;
    DatabaseReference _accumulativePlyer_ref;
    internal DatabaseReference _rulesChanges_ref;
    public class PlayerOnList
    {
        public GameObject myObj;

        public string fbId;
        public Image statusImage;
        public Image hostImage;
        public RawImage avatarImage;
        public Button avatarButton;
        public Button voiceChatButton;
        public Image voiceChatImage;
        public TMP_Text levelText;
        public TMP_Text nameText;
        public TMP_Text betText;
        public Button addFriendButton;
        public Button kickButton;

        public bool isMute;

        public PlayerOnList(GameObject obj)
        {
            myObj = obj;

            statusImage = obj.transform.GetChild(0).gameObject.GetComponent<Image>();
            hostImage = obj.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Image>();
            avatarImage = obj.transform.GetChild(1).GetChild(1).GetChild(0).gameObject.GetComponent<RawImage>();
            avatarButton = obj.transform.GetChild(1).GetChild(1).gameObject.GetComponent<Button>();
            voiceChatButton = obj.transform.GetChild(2).GetChild(0).gameObject.GetComponent<Button>();
            voiceChatImage = obj.transform.GetChild(2).GetChild(0).GetChild(0).gameObject.GetComponent<Image>();
            levelText = obj.transform.GetChild(3).gameObject.GetComponent<TMP_Text>();
            nameText = obj.transform.GetChild(4).gameObject.GetComponent<TMP_Text>();
            betText = obj.transform.GetChild(5).gameObject.GetComponent<TMP_Text>();
            addFriendButton = obj.transform.GetChild(6).GetChild(0).gameObject.GetComponent<Button>();
            kickButton = obj.transform.GetChild(7).GetChild(0).gameObject.GetComponent<Button>();
        }
    }
    public Sprite[] voiceChatButtonSprites; // 0 - Volume | 1 - Mute

    [Header("Room Info Panel")]
    public TMP_Text roomInfo_NameText;
    public TMP_Text roomInfo_PlayerCountText;
    public GameObject roomInfo_CantadaSliderObj;
    public TMP_Text roomInfo_betPerTableText;
    public GameObject roomInfo_VictoryTypeButtonsObj;
    PrivateRoomsListComponent.CantadaSlider roomInfo_CantadaSlider;
    PrivateRoomsListComponent.VictoryTypeButtons roomInfo_VictoryTypeButtons;
    public TMP_Text roomInfo_prizeText;
    internal List<Globals.PlayerInRoom> playersInRoomList;
    Coroutine mutePlayerCoroutine;
    public class TableSelection
    {
        public GameObject myObj;
        public Button selectButton;
        public Image backgroundImage;
        public GameObject selectedPanel;
        public TMP_Text selectedIndexText;
        public GameController.Game_Table myTable;

        public TableSelection(GameObject newTableSelection, GameController.Game_Table table)
        {
            myObj = newTableSelection;
            myTable = table;

            backgroundImage = newTableSelection.GetComponent<Image>();

            // Selection button
            selectButton = newTableSelection.transform.GetChild(2).GetComponent<Button>();

            // Selection panel
            selectedPanel = newTableSelection.transform.GetChild(1).gameObject;
            selectedIndexText = selectedPanel.GetComponentInChildren<TMP_Text>();

            // Create table preview
            GameObject newTable = Instantiate(table.myRectT.gameObject, newTableSelection.transform.GetChild(0).GetChild(0));
            RectTransform tableRectT = newTable.GetComponent<RectTransform>();
            tableRectT.anchorMin = Vector2.zero;
            tableRectT.anchorMax = Vector2.one;
            tableRectT.offsetMin = Vector2.zero;
            tableRectT.offsetMax = Vector2.zero;
            foreach (var i in newTableSelection.transform.GetChild(0).GetChild(0).GetChild(0).GetComponentsInChildren<Image>())
                i.raycastTarget = false;

            // Favorite
            newTableSelection.transform.GetChild(0).GetChild(1).GetChild(0).GetChild(0).gameObject.SetActive(table.myTable.isFavorite);

            // Fill table info
            string name = table.myTable.name.Length > 9 ? table.myTable.name.Substring(0, 9) + "..." : table.myTable.name;
            newTableSelection.transform.GetChild(0).GetChild(1).GetComponentInChildren<TMP_Text>().text = name + "\nVictorias: " + table.myTable.victories;

            // 
            table.myRectT.gameObject.SetActive(false);
        }
    }
    internal List<TableSelection> tableSelectionsList;
    public GameObject editRulesButton;

    [Header("Table selection")]
    public Button tableSelectionButton;
    public TMP_Text[] selectedTableCountTexts;
    public GameObject tableSelectionPanel;
    public GameObject tableSelectionPrefab;
    public Transform tableSelectionGrid;
    internal List<string> selectedTablesIdList;
    internal List<Color> tablesPagesColors;
    internal List<GameObject> selectedTablesObjList;
    public Button confirmTableSelectionButton;
    public Gradient[] gradients;
    GradientColorKey[] colorKey;
    GradientAlphaKey[] alphaKey;
    public TMP_Text tableSelectionTotalBetText;
    public Transform tableGroupsParent;
    public class TableGroupButton
    {
        public Image selectionImage;
        public TMP_Text tablesCountText;
        Dictionary<string, int> tablesPanelsDic;
        List<GameObject> tablesList;
        public bool isSelected;
        public List<GameController.Game_Table> groupTables;

        public TableGroupButton(GameObject obj)
        {
            selectionImage = obj.transform.GetComponent<Image>();
            selectionImage.enabled = false;
            tablesCountText = obj.transform.GetChild(0).GetChild(1).GetComponentInChildren<TMP_Text>();

            tablesList = new List<GameObject>();
            foreach (Transform t in obj.transform.GetChild(0).GetChild(0))
            {
                tablesList.Add(t.gameObject);
            }
            FillGroup(new List<GameController.Game_Table>());
        }

        public void AddTable(GameController.Game_Table table)
        {
            if (groupTables.Contains(table))
                return;
            groupTables.Add(table);
            FillGroup(groupTables);
        }

        public void RemoveTable(GameController.Game_Table tables)
        {
            groupTables.Remove(tables);
            FillGroup(groupTables);
        }

        public void FillGroup(List<GameController.Game_Table> tables)
        {
            groupTables = tables;

            tablesCountText.text = groupTables.Count.ToString();

            for (int i = 0; i < tablesList.Count; i++)
            {
                tablesList[i].SetActive(i < groupTables.Count);
            }
        }

        public void SelectGroup()
        {
            isSelected = true;
            selectionImage.enabled = true;
        }

        public void DeselectGroup()
        {
            isSelected = false;
            selectionImage.enabled = false;
        }
    }
    List<TableGroupButton> tableGroupsList;
    int currentGroupSelected;

    [Header("Game")]
    public Button startGameButton;
    internal List<Globals.Player> playersList;
    internal Dictionary<string, PlayerOnList> playersOnListDic;
    public Texture defaultAvatar;
    public Color[] statusColors;
    internal string curretRoomPlayerFbId;
    bool closedLobby = false;
    internal VoiceChatLobbyComponent voiceChatLobbyComponent;
    public GameObject pausePanel;
    TMP_Text pauseText;

    [Header("Bots")]
    public GameObject createBotButton;
    public Transform botTablesParent;
    internal List<BotComponent> myBots;
    internal string botsAccumulativePlayerFbId;
    public Texture avatarBotTexture;
    public Sprite accumulatedSprite;

    private void Start()
    {
        pendingPhotoNotifications = new Dictionary<string, NotificationBubbleComponent>();

        lobbyWindowRectT = app.mainController.privateRoomLobbyWindow.GetComponent<RectTransform>();

        pauseText = pausePanel.GetComponentInChildren<TMP_Text>();

        roomInfo_CantadaSlider = new PrivateRoomsListComponent.CantadaSlider(roomInfo_CantadaSliderObj);
        roomInfo_VictoryTypeButtons = new PrivateRoomsListComponent.VictoryTypeButtons(roomInfo_VictoryTypeButtonsObj);

        // Create table group buttons objects
        tableGroupsList = new List<TableGroupButton>();
        foreach (Transform c in tableGroupsParent)
            tableGroupsList.Add(new TableGroupButton(c.gameObject));

        CreateTablesGradient();

        // DEBUG
        // END DEBUG
    }

    private void Update()
    {
        // Check player idleness in game
        if (app.gameController.isPlaying)
        {
            if(app.mainController.mainWindow.activeSelf)
            {
                app.gameController.isPlaying = false;
                return;
            }

#if !UNITY_EDITOR
            float idleTime = Time.time - app.gameController.inGameLastTouchTime;
            if (idleTime > 1 * 60 && !app.promptComponent.promptPanel.activeSelf)
            {
                app.promptComponent.ShowPrompt("¿Sigues ahí?", "Hemos notado que no estás jugando, para que no pierdas monedas, te sacaremos en un minuto.",
                    Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            }
            else if (idleTime > 2 * 60)
            {
                app.promptComponent.args = "ExitPrivateRoom";
                app.promptComponent.promptType = Globals.PromptType.YesNoMessage;
                app.promptComponent.OnAcceptPrompt();

                app.promptComponent.ShowPrompt("¡Te sacamos!", "Estuviste mucho tiempo sin jugar, te sacamos de la mesa para que no pierdas más monedas.",
                    Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);
            }
#endif
        }
    }

    public void StartRefreshPrivateRoomPlayerList()
    {
        StartCoroutine(ShowHideLobbyWindow(true));

        playersList = new List<Globals.Player>();
        playersOnListDic = new Dictionary<string, PlayerOnList>();
        tableSelectionsList = new List<TableSelection>();
        playersInRoomList = new List<Globals.PlayerInRoom>();

        pausePanel.SetActive(false);
        hideLobbyWindowButton.gameObject.SetActive(false);

        tableSelectionButton.interactable = true;
        foreach (var t in selectedTableCountTexts)
            t.text = "0";

        currentGroupSelected = -1;

        closedLobby = false;

        app.gameController.inGameLastTouchTime = Time.time;

        // Bots
        app.gameController.accumulatedText.text = "---";

        initialPlayerCount = -1;
        notificationsEnabled = false;

        foreach (Transform child in playerOnListParent)
            if (!child.name.Contains("Header"))
                Destroy(child.gameObject);

        _players_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/playersIdList");
        _players_ref.ChildAdded -= Handle_PrivateRoomPlayers_ChildAdded;
        _players_ref.ChildAdded += Handle_PrivateRoomPlayers_ChildAdded;
        _players_ref.ChildChanged -= Handle_PrivateRoomPlayers_ChildChanged;
        _players_ref.ChildChanged += Handle_PrivateRoomPlayers_ChildChanged;
        _players_ref.ChildRemoved -= Handle_PrivateRoomPlayers_ChildRemoved;
        _players_ref.ChildRemoved += Handle_PrivateRoomPlayers_ChildRemoved;

        if (_rulesChanges_ref != null)
        {
            _rulesChanges_ref.ChildAdded -= Handle_RulesChange_ChildAdded;
            _rulesChanges_ref = null;
        }
        _rulesChanges_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/rulesChange");
        _rulesChanges_ref.ChildAdded += Handle_RulesChange_ChildAdded;

        // Set id on playerList
        string key = _players_ref.Push().Key;
        curretRoomPlayerFbId = key;
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        childUpdates["/playerId"] = app.mainController.myPlayer.id;
        childUpdates["/tablesQty"] = 0;
        _players_ref.Child(curretRoomPlayerFbId).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Player in room registered");
                    UnityMainThreadDispatcher.Instance().Enqueue(FinishJoinLobby());
                }
            });

        // start State reference
        _room_state_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/state");
        _room_state_ref.ValueChanged -= Handle_PrivateRoomState_ValueChanged;
        _room_state_ref.ValueChanged += Handle_PrivateRoomState_ValueChanged;

        // room reference
        _room_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms").Child(app.mainController.currentRoom.id);
        _room_ref.ValueChanged -= Handle_RoomValueChanged;
        _room_ref.ValueChanged += Handle_RoomValueChanged;
        _room_ref.ChildRemoved -= Handle_RoomRemoved;
        _room_ref.ChildRemoved += Handle_RoomRemoved;

        // reactions
        app.reactionsController.InitReactions();

        if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id) // HOST
        {
            // If gameHost disconnects, destroy game
            _room_ref.OnDisconnect().RemoveValue();

            // Bots
            myBots = new List<BotComponent>();
            foreach (Transform child in botTablesParent)
                Destroy(child.gameObject);
            _accumulativePlyer_ref = null;
            botsAccumulativePlayerFbId = string.Empty;
            createBotButton.SetActive(true);
            createBotButton.GetComponent<Button>().interactable = true;
            // Check if creating room with bots
            if(app.newPrivateRoomComponent.isCreatingWithBots)
            {
                app.newPrivateRoomComponent.isCreatingWithBots = false;
                app.botWindowComponent.CreateDefaultBots();
            }

            editRulesButton.SetActive(true);
        }
        else // CLIENT
        {
            // If client disconnects, remove from list
            _player_room_ref = _players_ref.Child(key);
            _player_room_ref.OnDisconnect().RemoveValue();

            createBotButton.SetActive(false);

            editRulesButton.SetActive(false);
        }

        // start game button
        startGameButton.gameObject.SetActive(app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id);
        startGameButton.interactable = false;

        // Destroy old table selections
        foreach (Transform child in tableSelectionGrid)
            Destroy(child.gameObject);
        currentGroupSelected = -1;
        foreach (var g in tableGroupsList)
            g.DeselectGroup();

        // Create player tables
        selectedTablesIdList = new List<string>();
        app.gameController.CreatePlayerTables();

        // Create voice chat component
        GameObject voiceChatObj = new GameObject("VoiceChatLobbyComponent");
        voiceChatLobbyComponent = voiceChatObj.AddComponent<VoiceChatLobbyComponent>();
        voiceChatLobbyComponent.Init(app.mainController.currentRoom.id);

        // Fill room info
        StartCoroutine(FillRoomInfo(null));
    }

    IEnumerator FinishJoinLobby()
    {
        yield return null;
        app.mainController.loadingPanel.SetActive(false);
    }

    void Handle_RulesChange_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        print(args.Snapshot.GetRawJsonValue());
        Globals.RuleChanged ruleChanged = JsonUtility.FromJson<Globals.RuleChanged>(args.Snapshot.GetRawJsonValue());
        //print(rascada.playerId);
        if (ruleChanged.ruleType == "JUG. MAX: ")
            app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
                Globals.NotificationType.RulesChange, null, "JUG. MAX: " + ruleChanged.val, null));
        if (ruleChanged.ruleType == "VELOCIDAD: ")
            app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
                Globals.NotificationType.RulesChange, null, "VELOCIDAD: " + ruleChanged.val + "s", null));
        if (ruleChanged.ruleType == "APUESTA: $")
            app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
                Globals.NotificationType.RulesChange, null, "APUESTA: $" + ruleChanged.val, null));
        if (ruleChanged.ruleType == "TIPO VICTORIA")
            app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
                Globals.NotificationType.RulesChange, null, "TIPO VICTORIA", null));
    }

    void Handle_RoomValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        string json = args.Snapshot.GetRawJsonValue();
        //print(json);
        if(!string.IsNullOrEmpty(json))
        {
            Globals.Room _room = JsonUtility.FromJson<Globals.Room>(json);

            // Get winners
            _room.winners = new Dictionary<string, object>();
            foreach (var _winner in args.Snapshot.Child("winners").Children)
                _room.winners.Add(_winner.Key, JsonUtility.FromJson<Globals.Winner>(_winner.GetRawJsonValue()));

            // Get playersIds
            /*_room.playersIdDict = new Dictionary<string, object>();
            foreach (var _player in args.Snapshot.Child("playersIdDict").Children)
                _room.playersIdDict.Add(_player.Key, JsonUtility.FromJson<Globals.PlayerInRoom>(_player.GetRawJsonValue()));*/

            // Fill room with new info
            UnityMainThreadDispatcher.Instance().Enqueue(FillRoomInfo(_room));
        }
    }

    void Handle_RoomRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        if (args.Snapshot.Key.Contains("playerHostId") && app.mainController.currentRoom.playerHostId != app.mainController.myPlayer.id)
        {
            print("ROOM REMOVED!!!");
            app.gameController.RemoveReferences(true);
            app.promptComponent.ShowPrompt("Partida terminada", "El anfitrión se desconectó", Globals.PromptType.ConfirmMessage, string.Empty, Globals.PromptColors.Pink);
        }

        //print(args.Snapshot.Key + " --- " + args.Snapshot.GetRawJsonValue());
    }

    IEnumerator FillRoomInfo(Globals.Room updatedRoom)
    {
        if (closedLobby)
            throw new Exception("Closed lobby, no need to refresh room");

        try
        {
            if (updatedRoom != null)
                app.mainController.currentRoom = updatedRoom;
            else
                initialPlayerCount = app.mainController.currentRoom.playerCount + 0;

            // Name
            roomInfo_NameText.text = "Mesa: " + app.mainController.currentRoom.name;

            // Players count
            roomInfo_PlayerCountText.text = "Jugadores: " + app.mainController.currentRoom.playerCount + " / " + app.mainController.currentRoom.maxPlayers;

            // Fill game window
            int botCount = playersInRoomList.Count(x => x.playerId.Substring(0, 4).Contains("Bot_"));
            int espectatorCount = playersInRoomList.Count(x => x.tablesQty == 0);
            app.gameController.playersCounts[0].text = (playersInRoomList.Count - botCount - espectatorCount).ToString();
            app.gameController.playersCounts[1].text = botCount.ToString();
            app.gameController.playersCounts[2].text = espectatorCount.ToString();
            //Countdown panel
            app.gameController.startGameCounterPlayersText.text = app.gameController.playersCounts[0].text;
            app.gameController.startGameCounterBotsText.text = app.gameController.playersCounts[1].text;
            app.gameController.startGameCounterExpectatorsText.text = app.gameController.playersCounts[2].text;

            // Cantada speed
            roomInfo_CantadaSlider.cantadaSpeedSlider.value = app.newPrivateRoomComponent.cantadaSpeedsList.IndexOf((int)app.mainController.currentRoom.cantadaSpeed);
            roomInfo_CantadaSlider.cantadaSpeedTimerText.text = app.newPrivateRoomComponent.cantadaSpeedsList[(int)roomInfo_CantadaSlider.cantadaSpeedSlider.value] + " s";
            roomInfo_CantadaSlider.cantadaSpeedTimerPanel.color = app.newPrivateRoomComponent.cantadaSpeedColors[(int)roomInfo_CantadaSlider.cantadaSpeedSlider.value];
            app.gameController.startGameCounterCantadaSpeedSlider.value = roomInfo_CantadaSlider.cantadaSpeedSlider.value;
            app.gameController.startGameCounterCantadaSpeedText.text = roomInfo_CantadaSlider.cantadaSpeedTimerText.text;
            app.gameController.startGameCounterCantadaSpeedImage.color = roomInfo_CantadaSlider.cantadaSpeedTimerPanel.color;

            // Line victory animation
            if (roomInfo_VictoryTypeButtons.lineTableAnimCoroutine == null)
                roomInfo_VictoryTypeButtons.lineTableAnimCoroutine =
                    app.newPrivateRoomComponent.StartCoroutine(app.newPrivateRoomComponent.LineVictoryTableAnimation(roomInfo_VictoryTypeButtons.lineTable));

            // Other victories
            List<string> parts = app.mainController.currentRoom.victoryTypes.Split(',').ToList();
            for (int i = 0; i < roomInfo_VictoryTypeButtons.victoryTypesImageList.Count; i++)
                roomInfo_VictoryTypeButtons.victoryTypesImageList[i].color = parts.Contains(i.ToString()) ? app.newPrivateRoomComponent.victoryTypeSelectionColor : Color.white;

            // Bet per table
            roomInfo_betPerTableText.text = "$" + app.mainController.currentRoom.betPerTable;
            app.gameController.startGameCounterBetPerTableText.text = roomInfo_betPerTableText.text;
            //Update Players bet
            int tablesQty = 0;
            foreach (var pol in playersOnListDic)
            {
                if (!pol.Key.Substring(0, 4).Contains("Bot_"))
                {
                    tablesQty = playersInRoomList.First(x => x.playerId == pol.Key).tablesQty;
                    pol.Value.betText.text = "$" + (tablesQty * app.mainController.currentRoom.betPerTable);
                }
            }

            // PRIZE
            long disconnectedBet = app.gameController.inGameDisconnectedPlayers == null ? 0 : app.gameController.inGameDisconnectedPlayers.Sum(x => x.Value);
            long _prize = ((playersInRoomList.Where(y => !y.playerId.Substring(0, 4).Contains("Bot_")).Sum(x => x.tablesQty) * app.mainController.currentRoom.betPerTable)) +
                disconnectedBet;
            roomInfo_prizeText.text = (app.gameController.inGame ? "PARTIDA EN CURSO\n" : string.Empty) + "PREMIO: $" + _prize;
            app.gameController.prizeText.text = "$" + _prize;
            app.gameController.startGameCounterPrizeText.text = app.gameController.prizeText.text;

            // Accumulated coins
            app.gameController.accumulatedText.text = "$" + app.mainController.currentRoom.accumulatedCoins;
            app.gameController.startGameCounterAccumulatedText.text = app.gameController.accumulatedText.text;

            // If showing victory panel
            if (app.gameController.victoryPanel.activeSelf)
            {
                // -- Check if show an emblem -- //
                if (!string.IsNullOrEmpty(app.mainController.currentRoom.lastWinner.emblemType))
                {
                    if (app.gameController.lastWinStreak != app.mainController.currentRoom.lastWinner.winStreak ||
                        app.gameController.lastWinnerId != app.mainController.currentRoom.lastWinner.playerId)
                    {
                        app.gameController.lastWinStreak = app.mainController.currentRoom.lastWinner.winStreak;
                        app.gameController.lastWinnerId = app.mainController.currentRoom.lastWinner.playerId;

                        // show victory emblem
                        app.gameController.emblemsParent.SetActive(true);

                        // show the emblem
                        float emblemExp = 0;
                        int lvlsUp = 0;
                        switch (app.mainController.currentRoom.lastWinner.emblemType)
                        {
                            case "WinStreak":
                                app.gameController.streakEmblemObj.SetActive(true);
                                app.gameController.streakEmblemCountText.text = app.mainController.currentRoom.lastWinner.winStreak.ToString();

                                // Check if im the one with the streak AND not playing alone
                                if (app.mainController.myPlayer.id == app.mainController.currentRoom.lastWinner.playerId && app.mainController.currentRoom.playerCount > 1)
                                {
                                    // Update stats
                                    UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.OnUpdatePlayerStats(new Globals.PlayerStats()
                                    {
                                        maxStreak = app.mainController.currentRoom.lastWinner.winStreak
                                    }, "Streak"));

                                    // gain experience
                                    float _per = 0.05f + ((float)app.mainController.currentRoom.lastWinner.winStreak / 6f);
                                    emblemExp = app.gameController.victoryExperience[app.gameController.buenas.victoryType] * _per;
                                    lvlsUp = app.profileController.UpdateExperience((int)Mathf.Round(emblemExp));
                                }
                                break;
                            case "StreakKiller":
                                app.gameController.streakKillerEmblemObj.SetActive(true);

                                // Check if im the one with the kill streak AND not playing alone
                                if (app.mainController.myPlayer.id == app.mainController.currentRoom.lastWinner.playerId && app.mainController.currentRoom.playerCount > 1)
                                {
                                    // gain experience
                                    emblemExp = app.gameController.victoryExperience[app.gameController.buenas.victoryType] * 0.3f;
                                    lvlsUp = app.profileController.UpdateExperience((int)Mathf.Round(emblemExp));
                                }
                                break;
                            default:
                                break;
                        }

                        // Update player stats
                        UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.OnUpdatePlayerStats(new Globals.PlayerStats()
                        {
                            totalExperience = emblemExp,
                            level = lvlsUp
                        }, null));
                    }
                }
                else // hide all emblems
                {
                    app.gameController.emblemsParent.SetActive(false);
                    app.gameController.streakEmblemObj.SetActive(false);
                    app.gameController.streakKillerEmblemObj.SetActive(false);
                }

                // -- Winners -- //
                if (app.gameController.lastWinners != app.mainController.currentRoom.winners)
                {
                    app.gameController.lastWinners = new Dictionary<string, object>(app.mainController.currentRoom.winners);

                    app.gameController.winnerPlayers = new List<WinnerPlayerComponent>();
                    // Destroy old winners
                    foreach (Transform child in app.gameController.winnersParent)
                        Destroy(child.gameObject);
                    GameObject newWinner;
                    Dictionary<string, int> winnersToShow = new Dictionary<string, int>();
                    string _playerId;
                    foreach (var winner in app.mainController.currentRoom.winners)
                    {
                        _playerId = ((Globals.Winner)winner.Value).playerId;
                        if (!winnersToShow.ContainsKey(_playerId))
                            winnersToShow.Add(_playerId, 1);
                        else
                            winnersToShow[_playerId]++;
                    }
                    // Get only playing players
                    List<string> keysToRemove = new List<string>();
                    foreach (var winner in winnersToShow)
                        if (playersList.Count(x => x.id == winner.Key) == 0)
                            keysToRemove.Add(winner.Key);
                    foreach (var key in keysToRemove)
                        winnersToShow.Remove(key);
                    // Get only top 10
                    winnersToShow = winnersToShow.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
                    while (winnersToShow.Count > 10)
                        winnersToShow.Remove(winnersToShow.Last().Key);
                    winnersToShow = winnersToShow.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
                    // Create winners on list
                    foreach (var winner in winnersToShow)
                    {
                        newWinner = Instantiate(app.gameController.winnerPlayerPrefab, app.gameController.winnersParent);
                        app.gameController.winnerPlayers.Add(newWinner.AddComponent<WinnerPlayerComponent>());
                        app.gameController.winnerPlayers.Last().Init(
                            newWinner,
                            playersList.First(x => x.id == winner.Key).name,
                            winner.Value.ToString(),
                            playersOnListDic[winner.Key].avatarImage.texture,
                            winner.Key.Substring(0, 4).Contains("Bot_") ? app.botWindowComponent.GetDificultyColorFromBotId(winner.Key) : Color.white
                        );
                    }
                }
            }
        }
        catch(Exception e)
        {
            /*app.gameController.ExitPrivateRoom();
            app.privateRoomLobbyComponent.ClosePrivateRoomLobby();
            UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("Error de conexión", "Se perdió comunicación con la sala",
                        Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow));*/
            throw new Exception("Error: something in the room does not anymore");
        }

        yield return null;
    }

    void Handle_PrivateRoomState_ValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null || args.Snapshot == null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        var _val = args.Snapshot.GetValue(false);
        if(_val == null)
        {
            print("Null input");
            return;
        }

        string state = _val.ToString();
        print(state);


        if(app.gameController.gameInitialized)
        {
            if (state.Contains("GameStartCountdown"))
            {
                string[] parts = state.Split(',');
                app.gameController.startGameCounterText.text = parts[1];

                //
                if (int.Parse(parts[1]) == 3)
                {
                    // Fill countdown panel
                    List<string> victoryParts = app.mainController.currentRoom.victoryTypes.Split(',').ToList();
                    for (int i = 0; i < app.gameController.startGameCounterVictoryTypesImages.Length; i++)
                        app.gameController.startGameCounterVictoryTypesImages[i].color = victoryParts.Contains(i.ToString()) ? app.newPrivateRoomComponent.selectionColor : Color.white;
                    // Line victory animation
                    if (app.gameController.startGameCounterLineTableAnimCoroutine == null)
                        app.gameController.startGameCounterLineTableAnimCoroutine =
                            app.newPrivateRoomComponent.StartCoroutine(app.newPrivateRoomComponent.LineVictoryTableAnimation(app.gameController.startGameCounterLineTable));

                    // Check if there are AFK players, then kick them
                    foreach (var p in playersInRoomList)
                        if(p.isAway)
                            OnKickPlayer(playersOnListDic[p.playerId].fbId);
                }
            }
            else if (state == "InGame")
            {
                // hide countdown panel
                app.gameController.startGameCounterPanel.SetActive(false);

                // disable table selection
                tableSelectionButton.interactable = !app.gameController.isPlaying;
                tableSelectionPanel.SetActive(!app.gameController.isPlaying);

                // hide lobby panel if open >> rly necesary?
                //if (!app.gameController.isPlaying && lobbyWindowRectT.localPosition.y > 0)
                    //app.privateRoomLobbyComponent.StartCoroutine(app.privateRoomLobbyComponent.ShowHideLobbyWindow(false));

                //ads
                app.adsController.playedGames++;
                PlayerPrefs.SetInt("PlayedGames", app.adsController.playedGames);

                // check if is only full table game
                app.gameController.victoryIndex = -1;
                SetIfFullTableGame();

                if (!app.gameController.inGame) // Start game
                    app.gameController.StartCoroutine(app.gameController.InitGame(false));
            }
            else if (state.Contains("WaitingNextGame"))
            {
                string[] parts = state.Split(',');
                app.gameController.victoryNextGameTitleText.text = "SIGUIENTE PARTIDA EN:";
                app.gameController.victoryNextGameCounterText.text = parts[1];

                // Reset rank
                if(int.Parse(parts[1]) == 15)
                {
                    StartCoroutine(app.gameController.UpdateCheckedPanelsFb(curretRoomPlayerFbId, new List<int>(), 0, 4.20f));
                    tableSelectionButton.interactable = true;
                    pausePanel.SetActive(false);

                    foreach (var t in app.gameController.tablePageButtonList)
                        t.HideMissedCardIndicator();

                    // Mark played once
                    if (!app.adsController.atLeastPlayedOnce) app.adsController.atLeastPlayedOnce = true;
                }
            }
            else if (state.Contains("WaitingFullTableGame"))
            {
                string[] parts = state.Split(',');
                app.gameController.victoryNextGameTitleText.text = "CARTA LLENA EN:";
                app.gameController.victoryNextGameCounterText.text = parts[1];

                tableSelectionButton.interactable = false;
                app.gameController.selectTableButton.interactable = false;
                pausePanel.SetActive(false);
            }
            else if (state.Contains("FullTableGame"))
            {
                app.gameController.StartFullTableGame();
            }
            else if (state.Contains("GamePaused"))
            {
                string[] parts = state.Split(',');
                
                if(parts[1] == "ChangingRules")
                {
                    pausePanel.SetActive(true);
                    pauseText.text = "<u>JUEGO EN PAUSA</u>\n\nEl anfitrión se encuentra cambiando las reglas.";
                }
            }
        }
        else
        {
            if(state.Contains("GameStartCountdown"))
            {
                // Start game
                app.mainController.ChangeGameState(MainController.GameState.Playing);
            }
            else
            {
                switch (state)
                {
                    case "InGame":
                    case "WaitingNextGame":
                    case "PreparingGame":
                        // check if is only full table game
                        SetIfFullTableGame();

                        // Start game
                        app.mainController.ChangeGameState(MainController.GameState.Playing);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    void SetIfFullTableGame()
    {
        List<string> _parts = app.mainController.currentRoom.victoryTypes.Split(',').ToList();
        app.gameController.playingFullTableVictory = _parts.Contains("3") && _parts.Count == 1;
    }

    public void OnExitPrivateRoom()
    {
        if(app.gameController.inGame)
        {
            if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
                app.promptComponent.ShowPrompt("Cerrar mesa",
                    "¿Seguro que desea cerrar la mesa?\nSólo tú perderás la EXP y la apue$ta",
                    Globals.PromptType.YesNoMessage,
                    "ExitPrivateRoom,inGame,playerId,exp,apuesta",
                    Globals.PromptColors.Red
                );
            else
                app.promptComponent.ShowPrompt("Abandonar mesa",
                    "¿Seguro que desea abandonar la mesa?\nPerderá la EXP y la apue$ta",
                    Globals.PromptType.YesNoMessage,
                    "ExitPrivateRoom,inGame,playerId,exp,apuesta",
                    Globals.PromptColors.Red
                );
        }
        else
        {
            if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
                app.promptComponent.ShowPrompt("Cerrar mesa",
                    "¿Seguro que desea cerrar la mesa?",
                    Globals.PromptType.YesNoMessage,
                    "ExitPrivateRoom,",
                    Globals.PromptColors.Yellow
                );
            else
                app.promptComponent.ShowPrompt("Abandonar mesa",
                    "¿Seguro que desea abandonar la mesa?",
                    Globals.PromptType.YesNoMessage,
                    "ExitPrivateRoom,",
                    Globals.PromptColors.Yellow
                );
        }
    }

    public void ClosePrivateRoomLobby()
    {
        closedLobby = true;

        app.gameController.audioSource.Stop();
        app.gameController.audioSource.volume = 0;

        // HOST
        if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
        {
            _room_ref.RemoveValueAsync().ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Room closed");
                    UnityMainThreadDispatcher.Instance().Enqueue(EndPrivateRoomPlayerRefresh(true));
                }
            });
        }
        else // CLIENT
        {
            _player_room_ref.RemoveValueAsync().ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Player leaved room");
                    UnityMainThreadDispatcher.Instance().Enqueue(EndPrivateRoomPlayerRefresh(true));
                }
            });
        }
    }

    IEnumerator EndPrivateRoomPlayerRefresh(bool endVoiceChat)
    {
        app.gameController.inGame = false;
        app.gameController.gameInitialized = false;

        // Line victory animation
        if (roomInfo_VictoryTypeButtons.lineTableAnimCoroutine != null)
        {
            app.newPrivateRoomComponent.StopCoroutine(roomInfo_VictoryTypeButtons.lineTableAnimCoroutine);
            roomInfo_VictoryTypeButtons.lineTableAnimCoroutine = null;
        }

        // If friend window is open
        if(app.friendsComponent.friendWindow.activeSelf)
        {
            app.friendsComponent.OnCloseFriendWindow();
        }

        // Bots
        if(myBots != null)
        {
            if (myBots.Count > 0)
            {
                foreach (var bot in myBots)
                {
                    bot.DestroyBot(false);
                    yield return new WaitForSeconds(0.05f);
                }
                myBots = null;
            }
        }

        try
        {
            _players_ref.ChildAdded -= Handle_PrivateRoomPlayers_ChildAdded;
            _players_ref.ChildRemoved -= Handle_PrivateRoomPlayers_ChildRemoved;

            _room_state_ref.ValueChanged -= Handle_PrivateRoomState_ValueChanged;

            _room_ref.ValueChanged -= Handle_RoomValueChanged;
            _room_ref.ChildRemoved -= Handle_RoomRemoved;

            _players_ref = null;
            _room_ref = null;
            _player_room_ref = null;
            _room_state_ref = null;

            if(_accumulativePlyer_ref != null)
            {
                _accumulativePlyer_ref.ChildAdded -= Handle_AccumulativePlayerCoinInput_ChildAdded;
                _accumulativePlyer_ref.RemoveValueAsync();
                _accumulativePlyer_ref = null;
            }

            // reactions
            app.reactionsController.EndReactions();

            // Stop player
            app.gameController.EndPlayerTablesHandler();
            app.gameController.RemoveReferences(false);

            // End voice chat
            if (endVoiceChat)
            {
                voiceChatLobbyComponent.LogoutOfVivoxChannels();
                Destroy(voiceChatLobbyComponent.gameObject);
            }
        }
        catch(Exception e)
        {
            print(e.Message);
        }

        yield return null;

        app.gameController.gameInitialized = false;

        app.mainController.gameWindow.SetActive(false);
        app.mainController.privateRoomLobbyWindow.SetActive(false);
        app.mainController.gameState = MainController.GameState.LoggedIn;
        app.mainController.StartCoroutine(app.mainController.ChangeWindow());

        print("Ending private room lobby");
    }

    IEnumerator CreatePlayerOnList(string fbKey, Globals.Player player, int tablesQty, string botDificulty)
    {
        playersList.Add(player);

        GameObject newRoL = Instantiate(playerOnListPrefab, playerOnListParent);
        playersOnListDic.Add(player.id, new PlayerOnList(newRoL));
        SetPlayerOnListData(fbKey, player, tablesQty, botDificulty);

        // check if loaded all players since entered
        if (!notificationsEnabled)
        {
            initialPlayerCount--;
            if (initialPlayerCount < 0)
                notificationsEnabled = true;
        }

        // resize grid
        playerOnListParent.sizeDelta = new Vector2(playerOnListParent.sizeDelta.x, playerOnListParent.childCount * 95);

        UpdatePlayersCount(playersList.Count);

        yield return null;
    }

    void SetPlayerOnListData(string fbKey, Globals.Player player, int tablesQty, string botDificulty)
    {
        PlayerOnList pol = playersOnListDic[player.id];

        // Info
        pol.fbId = fbKey;
        pol.statusImage.color = playersInRoomList.First(x => x.playerId == player.id).tablesQty > 0 ?
            app.promptComponent.promptColors[1] : app.promptComponent.promptColors[4];
        pol.levelText.text = player.level.ToString();
        pol.nameText.text = player.name;

        // host indicator
        pol.hostImage.gameObject.SetActive(player.id == app.mainController.currentRoom.playerHostId);

        // BOT
        bool isBot = false;
        if(player.id.Length >= 4)
        {
            if (player.id.Substring(0, 4).Contains("Bot_"))
            {
                isBot = true;

                // Profile photo
                StartCoroutine(SetPlayerPhoto(player.id, app.tools.TextureToTexture2D(avatarBotTexture)));
                int _dificulty;
                if (int.TryParse(botDificulty, out _dificulty))
                    playersOnListDic[player.id].avatarImage.color = app.botWindowComponent.dificultyColors[_dificulty];

                pol.voiceChatButton.gameObject.SetActive(false);
                pol.addFriendButton.gameObject.SetActive(false);

                pol.betText.text = "---";
            }
        }
        if(!isBot)
        {
            pol.betText.text = "$" + (tablesQty * app.mainController.currentRoom.betPerTable);

            // Default image
            if (string.IsNullOrEmpty(player.profileImageUrl))
                playersOnListDic[player.id].avatarImage.texture = defaultAvatar;
            else
                app.tools.StartCoroutine(app.tools.DownloadFirebasePhoto(player.profileImageUrl, player.id, Globals.PhotoDownloadType.LobbyPhoto));
            playersOnListDic[player.id].avatarImage.color = Color.white;

            // Voice chat button
            pol.voiceChatButton.onClick.RemoveAllListeners();
            pol.voiceChatButton.onClick.AddListener(delegate {
                if (mutePlayerCoroutine != null)
                    StopCoroutine(mutePlayerCoroutine);
                mutePlayerCoroutine = StartCoroutine(TogglePlayerMute(pol, player.id));
            });
            // Mute playing player
            if (app.mainController.myPlayer.id == player.id)
            {
                StartCoroutine(TogglePlayerMute(playersOnListDic.First(x => x.Key == player.id).Value, player.id)); ;
            }

            // Add friend button
            if (player.id != app.mainController.myPlayer.id)
            {
                pol.addFriendButton.onClick.RemoveAllListeners();
                pol.addFriendButton.onClick.AddListener(delegate {

                });
            }
            else
            {
                pol.addFriendButton.gameObject.SetActive(false);
            }
            pol.addFriendButton.interactable = false; // HASTA QUE QUEDE IMPLEMENTADO
        }

        // Profile button
        pol.avatarButton.onClick.RemoveAllListeners();
        if (!isBot && player.id != app.mainController.myPlayer.id)
        {
            pol.avatarButton.onClick.AddListener(delegate
            {
                app.globalChatComponent.OnOpenPlayerProfile(player.id, player.name, Globals.ProfileOrigin.Lobby);
            });
        }
        else
        {
            pol.avatarButton.enabled = false;
        }

        // FOR HOST - Kick button
        if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id && player.id != app.mainController.myPlayer.id)
        {
            pol.kickButton.onClick.RemoveAllListeners();
            pol.kickButton.onClick.AddListener(delegate {
                app.promptComponent.ShowPrompt(
                    isBot ? "RemoverBot" : "Sacar a jugador",
                    isBot ? "¿Está seguro que desea remover al bot de la partida?" : "¿Está seguro que desea sacar al jugador " + player.name + " de la partida?",
                    Globals.PromptType.YesNoMessage,
                    (isBot ? "RemoveBot," : "KickPlayer,") + fbKey,
                    isBot ? Globals.PromptColors.Yellow : Globals.PromptColors.Red
                );
            });
        }
        else
        {
            pol.kickButton.gameObject.SetActive(false);
        }

        // Order players and bots
        List<string> orderedPlayersKeys = playersOnListDic.Keys.OrderByDescending(x => x).ToList();
        orderedPlayersKeys = orderedPlayersKeys.OrderBy(x => x.Substring(0, 4).Contains("Bot_")).ToList();
        for (int i = 0; i < orderedPlayersKeys.Count; i++)
            playersOnListDic[orderedPlayersKeys[i]].myObj.transform.SetAsLastSibling();
        if(playersOnListDic.ContainsKey(app.mainController.myPlayer.id))
            playersOnListDic[app.mainController.myPlayer.id].myObj.transform.SetAsFirstSibling();
        // Host first
        playersOnListDic[app.mainController.currentRoom.playerHostId].myObj.transform.SetAsFirstSibling();

        // Show notification bubble
        if (app.mainController.myPlayer.id != player.id && notificationsEnabled)
        {
            app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
                Globals.NotificationType.PlayerIn,
                player.id.Substring(0, 4).Contains("Bot_") ? avatarBotTexture : null,
                player.name,
                player.id
            ));
        }
    }

    public IEnumerator TogglePlayerMute(PlayerOnList pol, string playerId)
    {
        while (!voiceChatLobbyComponent.participantDict.ContainsKey(playerId))
        {
            yield return null;
        }
        
        pol.isMute = !pol.isMute;
        int _host = app.mainController.myPlayer.id == playerId ? 2 : 0;
        pol.voiceChatImage.sprite = voiceChatButtonSprites[(pol.isMute ? 1 : 0) + _host];

        IParticipant _p = voiceChatLobbyComponent.participantDict[playerId];
        if (_p.IsSelf)
        {
            voiceChatLobbyComponent._vivoxVoiceManager.AudioInputDevices.Muted = pol.isMute;
        }
        else
        {
            if (_p.InAudio)
                _p.LocalMute = pol.isMute;
        }

        //print("Player " + playerId + (pol.isMute ? " MUTED" : " UNMUTED"));
    }

    public IEnumerator SetPlayerPhoto(string _playerId, Texture2D _tex)
    {
        if (_tex != null)
        {
            int scale = Math.Min(_tex.width / 200, _tex.height / 200);
            List<RawImage> updateImages = new List<RawImage>();
            updateImages.Add(playersOnListDic[_playerId].avatarImage);
            
            // Notification bubble
            if (pendingPhotoNotifications.ContainsKey(_playerId))
            {
                if (pendingPhotoNotifications[_playerId] != null)
                {
                    updateImages.Add(pendingPhotoNotifications[_playerId].mainIcon);
                }
            }

            foreach (var i in updateImages)
            {
                i.texture = _tex;
            }
        }

        yield return new WaitForSeconds(0.5f);
        pendingPhotoNotifications.Remove(_playerId);

        yield return null;
    }

    IEnumerator DeletePlayerOnList(string playerId)
    {
        yield return new WaitForSeconds(1);

        if (closedLobby || playersList.Count(x => x.id == playerId) == 0)
        {

        }
        else
        {
            var _player = playersList.First(x => x.id == playerId);

            // Show notification bubble
            if (app.mainController.myPlayer.id != playerId && notificationsEnabled)
            {
                app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
                    Globals.NotificationType.PlayerOut,
                    playersOnListDic[playerId].avatarImage.texture,
                    _player.name,
                    playerId
                ));
            }

            playersList.RemoveAt(playersList.IndexOf(_player));

            DestroyImmediate(playersOnListDic[playerId].myObj);
            playersOnListDic.Remove(playerId);

            UpdatePlayersCount(playersList.Count);

            playerOnListParent.sizeDelta = new Vector2(playerOnListParent.sizeDelta.x, playerOnListParent.childCount * 95);
        }
    }

    void Handle_PrivateRoomPlayers_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());
        Globals.PlayerInRoom _playerInRoom = JsonUtility.FromJson<Globals.PlayerInRoom>(args.Snapshot.GetRawJsonValue());

        if (_playerInRoom == null || string.IsNullOrEmpty(args.Snapshot.GetRawJsonValue()))
            throw new Exception("Error getting player from room");
        
        playersInRoomList.Add(_playerInRoom);

        if(_playerInRoom.playerId.Substring(0, 4).Contains("Bot_"))
        {
            Globals.Player newBotPlayer = new Globals.Player() {
                id = _playerInRoom.playerId,
                name = "Bot_" + _playerInRoom.playerId.Substring(_playerInRoom.playerId.Length - 2),
            };

            string[] parts = _playerInRoom.playerId.Split('_');

            StartCoroutine(CreatePlayerOnList(args.Snapshot.Key, newBotPlayer, _playerInRoom.tablesQty, parts[2]));
        }
        else
        {
            FirebaseDatabase.DefaultInstance
            .GetReference("Players/" + _playerInRoom.playerId)
            .GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    Globals.Player newPlayer = JsonUtility.FromJson<Globals.Player>(snapshot.GetRawJsonValue());
                    UnityMainThreadDispatcher.Instance().Enqueue(CreatePlayerOnList(args.Snapshot.Key, newPlayer, _playerInRoom.tablesQty, null));
                }
            });
        }
    }

    void Handle_PrivateRoomPlayers_ChildChanged(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());
        Globals.PlayerInRoom _playerInRoom = JsonUtility.FromJson<Globals.PlayerInRoom>(args.Snapshot.GetRawJsonValue());
        if (_playerInRoom == null)
            return;
        if (playersInRoomList.Count(x => x.playerId == _playerInRoom.playerId) == 0)
            return;
        playersInRoomList[playersInRoomList.IndexOf(playersInRoomList.First(x => x.playerId == _playerInRoom.playerId))] = _playerInRoom;
        //Update prize
        playersOnListDic[_playerInRoom.playerId].betText.text =
            _playerInRoom.playerId.Substring(0, 4).Contains("Bot_") ? "---" :
            ("$" + (_playerInRoom.tablesQty * app.mainController.currentRoom.betPerTable));
        //Update state
        playersOnListDic[_playerInRoom.playerId].statusImage.color = _playerInRoom.tablesQty > 0 ?
            app.promptComponent.promptColors[1] : app.promptComponent.promptColors[4];

        // Set player rank 5
        List<Globals.PlayerInRoom> orderedPlayers = playersInRoomList.OrderByDescending(x => x.rankPoints).ToList();
        for (int i = 0; i < app.gameController.rankPanelsList.Count; i++)
        {
            if(orderedPlayers.Count > i)
            {
                if(orderedPlayers[i].checkedPanels != null)
                {
                    if(orderedPlayers[i].checkedPanels.Count > 0)
                    {
                        app.gameController.rankPanelsList[i].Show();
                        app.gameController.rankPanelsList[i].PaintTable(orderedPlayers[i].checkedPanels, orderedPlayers[i].playerId == app.mainController.myPlayer.id);

                        app.gameController.rankPanelsList[i].playerImage.texture = playersOnListDic[orderedPlayers[i].playerId].avatarImage.texture;

                        //if bot
                        if (orderedPlayers[i].playerId.Substring(0, 4).Contains("Bot_"))
                            app.gameController.rankPanelsList[i].playerImage.color = app.botWindowComponent.GetDificultyColorFromBotId(orderedPlayers[i].playerId);
                        else
                            app.gameController.rankPanelsList[i].playerImage.color = Color.white;
                    }
                    else
                    {
                        app.gameController.rankPanelsList[i].Hide();
                    }
                }
            }
            else
            {
                app.gameController.rankPanelsList[i].Hide();
            }
        }

        // If host, update total bet <<< ???
        if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
        {

        }
    }

    void Handle_PrivateRoomPlayers_ChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        Globals.PlayerInRoom _playerInRoom = JsonUtility.FromJson<Globals.PlayerInRoom>(args.Snapshot.GetRawJsonValue());
        playersInRoomList.RemoveAt(playersInRoomList.IndexOf(playersInRoomList.First(x => x.playerId == _playerInRoom.playerId)));

        // HOST
        if(app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
        {
            // Check if disconected player had a win streak and its not him
            if(_playerInRoom.playerId == app.mainController.currentRoom.lastWinner.playerId &&
                app.mainController.currentRoom.playerHostId != _playerInRoom.playerId)
            {
                // remove streak
                app.gameController.InsertGameWinner(string.Empty, 0, string.Empty);
            }
        }

        // if its me
        if (app.mainController.myPlayer.id == _playerInRoom.playerId && !closedLobby)
        {
            app.gameController.audioSource.Stop();
            app.gameController.audioSource.volume = 0;
            notificationsEnabled = false;
            StartCoroutine(EndPrivateRoomPlayerRefresh(true));
            app.promptComponent.ShowPrompt("Partida terminada", "El anfitrión ha cerrado la mesa", Globals.PromptType.ConfirmMessage, string.Empty, Globals.PromptColors.Red);
        }
        else if(!closedLobby)
        {
            // Check if was inGame when player disconnected
            if (app.gameController.inGame)
                app.gameController.inGameDisconnectedPlayers.Add(_playerInRoom.playerId, _playerInRoom.tablesQty * app.mainController.currentRoom.betPerTable);

            // Delete player from list
            StartCoroutine(DeletePlayerOnList(_playerInRoom.playerId));
        }
    }

    public void OpenTableSelectionPanel()
    {
        // Update total bet text
        tableSelectionTotalBetText.text = "Tu apuesta será de: <color=#e91e63><b><size=62>$" + app.mainController.currentRoom.betPerTable * selectedTablesIdList.Count;

        tableSelectionPanel.SetActive(true);
        confirmTableSelectionButton.interactable = true;//selectedTablesIdList.Count > 0;

        tableSelectionGrid.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 1.0f;
        app.reactionsController.reactionsButton.SetActive(false);
    }

    public void OpenTableSelectionPanel_FromGame()
    {
        OpenTableSelectionPanel();
        StartCoroutine(ShowHideLobbyWindow(true));
    }

    public void CreateTableSelection(GameController.Game_Table table)
    {
        GameObject newTableSelection = Instantiate(tableSelectionPrefab, tableSelectionGrid);
        TableSelection ts = new TableSelection(newTableSelection, table);
        newTableSelection.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate {
            SelectTable(table);// newTableSelection.transform.GetSiblingIndex());
        });
        tableSelectionsList.Add(ts);

        //print("===NEW ORDERED TABLES===");
        List<TableSelection> _ordered = tableSelectionsList.OrderByDescending(x => x.myTable.myTable.isFavorite).ToList();
        Dictionary<int, List<GameController.Game_Table>> groupTables = new Dictionary<int, List<GameController.Game_Table>>();
        for (int i = 0; i < _ordered.Count; i++)
        {
            var tableObj = _ordered[i].myObj;
            tableObj.transform.SetSiblingIndex(i);
            //print(_ordered[i].myTable.myTable.name + "  FAV: " + _ordered[i].myTable.myTable.isFavorite + " --- obj name: " + tableObj.name);

            // Add to group
            if (!string.IsNullOrEmpty(_ordered[i].myTable.myTable.groupIds))
            {
                string[] parts = _ordered[i].myTable.myTable.groupIds.Split(',');
                int groupId;
                foreach (var p in parts)
                {
                    groupId = int.Parse(p);

                    if (!groupTables.ContainsKey(groupId))
                        groupTables.Add(groupId, new List<GameController.Game_Table>());

                    groupTables[groupId].Add(_ordered[i].myTable);
                }
            }
        }

        // Create group buttons
        for (int i = 0; i < tableGroupsParent.childCount; i++)
        {
            if (groupTables.ContainsKey(i))
            {
                tableGroupsList[i].FillGroup(groupTables[i]);
            }
        }
    }

    public void SelectTable(GameController.Game_Table table)
    {
        var selectedTable = app.gameController.myTablesDict.First(x => x.Value == table);

        if (selectedTablesIdList.Contains(table.firebaseId)) // REMOVE TABLE
        {
            for (int i = selectedTablesIdList.IndexOf(table.firebaseId); i < selectedTablesIdList.Count; i++)
            {
                tableSelectionsList.First(x => x.myTable.firebaseId == selectedTablesIdList[i]).selectedIndexText.text = i.ToString();
            }

            selectedTablesIdList.Remove(table.firebaseId);

            var tableSelection = tableSelectionsList.First(x => x.myTable.firebaseId == table.firebaseId);
            tableSelection.selectedPanel.SetActive(false);

            SetTableSelectionColors();

            //print("Deselected table " + tableIndex);

            // TABLE GROUP
            if(currentGroupSelected >= 0)
            {
                tableGroupsList[currentGroupSelected].RemoveTable(tableSelection.myTable);
            }
        }
        else // ADD TABLE
        {
            if (app.mainController.myPlayer.coins < app.mainController.currentRoom.betPerTable * (selectedTablesIdList.Count + 1))
            {
                print("CAN'T AFFORD");
                return;
            }

            selectedTablesIdList.Add(table.firebaseId);

            var tableSelection = tableSelectionsList.First(x => x.myTable.firebaseId == table.firebaseId);
            tableSelection.selectedPanel.SetActive(true);
            tableSelection.selectedIndexText.text = selectedTablesIdList.Count.ToString();

            SetTableSelectionColors();

            //print("Selected table " + tableIndex);

            // TABLE GROUP
            if (currentGroupSelected >= 0)
            {
                tableGroupsList[currentGroupSelected].AddTable(tableSelection.myTable);
            }
        }

        confirmTableSelectionButton.interactable = true;//selectedTablesIdList.Count > 0;

        // Update total bet text
        tableSelectionTotalBetText.text = "Tu apuesta será de: <color=#e91e63><b><size=62>$" + app.mainController.currentRoom.betPerTable * selectedTablesIdList.Count;
    }

    public void UnselectAllTables()
    {
        if (selectedTablesIdList == null)
            return;

        List<string> keys = new List<string>(selectedTablesIdList);
        foreach (var tableId in keys)
        {
            SelectTable(app.gameController.myTablesDict.First(x => x.Value.firebaseId == tableId).Value);
        }
    }

    void SetTableSelectionColors()
    {
        for (int i = 0; i < tableSelectionGrid.childCount; i++)
        {
            Image tableBackground = tableSelectionGrid.GetChild(i).gameObject.GetComponent<Image>();
            tableBackground.color = Color.white;
        }

        tablesPagesColors = new List<Color>();
        int colorPerc = 0;
        int totalPages = (int)Math.Ceiling((float)selectedTablesIdList.Count / 2f) - 1;
        float _grad;
        int gradientId;
        float gradientVal;
        for (int i = 0; i < selectedTablesIdList.Count; i++)
        {
            if (i % 2 == 0)
            {
                if(i > 0)
                    colorPerc++;

                _grad = totalPages == 0 ? 0 : ((float)colorPerc / (float)totalPages);
                
                if(totalPages > 0)
                {
                    gradientId = _grad > .5f ? 1 : 0;
                    gradientVal = _grad > .5f ? (_grad - .5f) * 2f : _grad * 2f;
                }
                else
                {
                    gradientId = 0;
                    gradientVal = 0;
                }

                //print(colorPerc + " / " + totalPages + " == color: " + _grad + "  == gradientId: " + gradientId + "  --  gradientVal: " + gradientVal);

                tablesPagesColors.Add(gradients[gradientId].Evaluate(gradientVal));
            }

            //backgroundImage
            tableSelectionsList.First(x => x.myTable.firebaseId == selectedTablesIdList[i]).backgroundImage.color = tablesPagesColors.Last();
        }
    }

    public void OnSelectGroup(int _groupId)
    {
        if (currentGroupSelected == _groupId) // Same, unselect
        {
            tableGroupsList[currentGroupSelected].DeselectGroup();
            currentGroupSelected = -1;
            UnselectAllTables();
            return;
        }
        else if (currentGroupSelected >= 0) // Other selected, unselect first
        {
            tableGroupsList[currentGroupSelected].DeselectGroup();
        }

        // Unselect tables
        currentGroupSelected = -1;
        UnselectAllTables();

        // Select new group
        currentGroupSelected = _groupId;
        tableGroupsList[currentGroupSelected].SelectGroup();

        // Select tables on grid
        for (int i = 0; i < tableGroupsList[currentGroupSelected].groupTables.Count; i++)
        {
            SelectTable(tableGroupsList[currentGroupSelected].groupTables[i]);
        }
    }

    public void OnConfirmTableSelection()
    {
        app.reactionsController.reactionsButton.SetActive(true);
        selectedTablesObjList = new List<GameObject>();

        GameObject _table;
        string _selectedTables = string.Empty;

        // Destroy old select buttons
        foreach (Transform child in app.gameController.changeButtonParent)
            Destroy(child.gameObject);

        app.gameController.tablePageButtonList = new List<GameController.TablePageButton>();

        foreach (var table in app.gameController.myTablesDict)
            table.Value.isSelected = false;

        // set tables in game colors
        int curColor = 0;
        GameObject newPageButton;
        int _pages = 0;
        for (int i = 0; i < selectedTablesIdList.Count; i++)
        {
            if (i % 2 == 0)
            {
                if (i > 0)
                    curColor++;

                newPageButton = Instantiate(app.gameController.changeTableButtonPrefab, app.gameController.changeButtonParent);

                app.gameController.tablePageButtonList.Add(new GameController.TablePageButton(newPageButton));
                app.gameController.tablePageButtonList.Last().colorIndicatorImage.color = tablesPagesColors[curColor];

                _pages++;
            }

            GameController.Game_Table game_table = app.gameController.myTablesDict.First(x => x.Value.firebaseId == selectedTablesIdList[i]).Value;

            game_table.isSelected = true;
            _selectedTables += game_table.firebaseId + ",";

            _table = app.gameController.myTablesDict.First(x => x.Value.firebaseId == game_table.firebaseId).Key;
            _table.transform.SetParent(i % 2 == 0 ? app.gameController.tableUpContainer : app.gameController.tableDownContainer);
            _table.transform.localPosition = Vector3.zero;
            selectedTablesObjList.Add(_table);

            game_table.backgroundImage.color = tablesPagesColors[curColor];
        }

        //fit change page buttons size
        StartCoroutine(FitTablePageButtons(_pages));

        //Create chips parents
        foreach (Transform child in app.gameController.chipsParents) Destroy(child.gameObject);
        app.gameController.chipsParents = new List<Transform>();
        GameObject newChipParent;
        for (int p = 0; p < _pages; p++)
        {
            newChipParent = Instantiate(app.gameController.chipParentPrefab, app.gameController.chipParentsContainer);
            app.gameController.chipsParents.Add(newChipParent.transform);
            newChipParent.SetActive(false);
        }

        //
        startGameButton.interactable = selectedTablesObjList.Count > 0;

        tableSelectionPanel.SetActive(false);

        app.gameController.currentTablesPage = -1;
        app.gameController.ChangeTablesPage(0);

        if (selectedTablesObjList.Count >= 0 && (
            app.gameController.isPlaying ||
            app.mainController.currentRoom.state.Contains("InLobby") ||
            app.mainController.currentRoom.state.Contains("GamePaused")
            ))
        {
            UpdateTablesQty(selectedTablesIdList.Count);
            _selectedTables = string.IsNullOrEmpty(_selectedTables) ? string.Empty : _selectedTables.Remove(_selectedTables.Length - 1);
        }
        else
        {
            UpdateTablesQty(0);

        }
        //print(_selectedTables);

        // Set tables count on indicators
        foreach (var t in selectedTableCountTexts)
            t.text = selectedTablesObjList.Count.ToString();

        // Save table groups
        Dictionary<string, string> tableGroup = new Dictionary<string, string>();
        foreach (var group in tableGroupsList)
        {
            foreach (var table in group.groupTables)
            {
                if (!tableGroup.ContainsKey(table.firebaseId))
                    tableGroup.Add(table.firebaseId, string.Empty);

                tableGroup[table.firebaseId] += tableGroupsList.IndexOf(group) + ",";
            }

            Dictionary<string, object> childUpdates = new Dictionary<string, object>();

            foreach (var tg in tableGroup)
                childUpdates["/" + tg.Key + "/groupIds"] = tg.Value.Remove(tg.Value.Length - 1);

            app.gameController._playersTables_ref
                .UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                    if (t.IsFaulted)
                    {
                        Debug.Log("Faulted.." + t.Exception.Message);
                    }

                    if (t.IsCanceled)
                    {
                        Debug.Log("Cancelled..");
                    }

                    if (t.IsCompleted)
                    {
                        //Debug.Log("Completed! - Tables group update");
                    }
                });
        }
    }

    IEnumerator FitTablePageButtons(int _pages)
    {
        float _size = 1f - (_pages / 16f);
        _size = (_size * (125 - 59.6875f)) + 59.6875f;
        //print("_size: " + _size);
        app.gameController.changeButtonParentGridLayout.cellSize = Vector2.one * _size;
        yield return new WaitForSeconds(1);
        
        // Fit missed panels
        foreach (var table in app.gameController.tablePageButtonList)
        {
            table.FitMissedPanel(_pages);
            table.HideMissedCardIndicator();
        }

        yield return new WaitForEndOfFrame();
    }

    public void UpdateTablesQty(int newTablesQty)
    {
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        childUpdates["/tablesQty"] = newTablesQty;
        _players_ref.Child(curretRoomPlayerFbId).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Player in room registered");
            }
        });
    }

    public void OnStartGame()
    {
        // Hide play button
        startGameButton.gameObject.SetActive(false);

        // Change room status
        _room_state_ref.SetValueAsync("PreparingGame", 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Room state changed");
            }
        });
    }

    public void PauseGame(string reason)
    {
        // Change room status
        _room_state_ref.SetValueAsync("GamePaused," + reason, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Room state changed");
            }
        });
    }

    public void OnKickPlayer(string fbKey)
    {
        // Change room status
        var _kicked_players_ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms/" + app.mainController.currentRoom.id + "/playersIdList");
        _kicked_players_ref.Child(fbKey).RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Player kicked");
            }
        });
    }

    void UpdatePlayersCount(int counter)
    {
        if (app.mainController.myPlayer.id != app.mainController.currentRoom.playerHostId || counter == 0)
            return;

        _room_ref.Child("playerCount").SetValueAsync(counter).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Room player count updated!");
            }
        });
    }

#region BOTS
    public void CreateBot(int _dificulty, int _tableQty, float _eyeSpeed)
    {
        GameObject newBotObj = new GameObject("Bot_" + myBots.Count);
        BotComponent newBot = newBotObj.AddComponent<BotComponent>();
        newBot.InitBot(myBots.Count, _dificulty, _tableQty, _eyeSpeed);
        myBots.Add(newBot);

        // Create accumulative player holder
        if (myBots.Count == 1 && _accumulativePlyer_ref == null)
        {
            _accumulativePlyer_ref = FirebaseDatabase.DefaultInstance.GetReference("Players");
            botsAccumulativePlayerFbId = _accumulativePlyer_ref.Push().Key;
            _accumulativePlyer_ref = _accumulativePlyer_ref.Child(botsAccumulativePlayerFbId);

            _accumulativePlyer_ref.OnDisconnect().RemoveValue();

            _accumulativePlyer_ref.Child("coinsInput").ChildAdded += Handle_AccumulativePlayerCoinInput_ChildAdded;

            Dictionary<string, object> accumulativePlayer = new Dictionary<string, object>();
            accumulativePlayer["/id"] = botsAccumulativePlayerFbId;
            accumulativePlayer["/coins"] = 0;

            _accumulativePlyer_ref.UpdateChildrenAsync(accumulativePlayer).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("===== ***** Completed! - Accumulative player created ***** =====");
                }
            });
        }
    }

    void Handle_AccumulativePlayerCoinInput_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        string json = args.Snapshot.GetRawJsonValue();
        print(json);
        Globals.ReceivedCoinTransaction rct = null;
        if(json.Contains("quantity"))
            rct = JsonUtility.FromJson<Globals.ReceivedCoinTransaction>(json);
        else
            rct = new Globals.ReceivedCoinTransaction(){
                quantity = (long)args.Snapshot.Value
            };

        // Remove it
        _accumulativePlyer_ref.Child("coinsInput").Child(args.Snapshot.Key).RemoveValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Removed coin input");

                // add to old coins
                UnityMainThreadDispatcher.Instance().Enqueue(GetAccumulativePlayerCoins(rct));
            }
        });
    }

    IEnumerator GetAccumulativePlayerCoins(Globals.ReceivedCoinTransaction newCoins)
    {
        yield return null;

        // get old coins
        _accumulativePlyer_ref.Child("coins").GetValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                DataSnapshot snapshot = t.Result;
                Debug.Log("Completed! - Current coins " + snapshot.Value);

                // update coins
                UnityMainThreadDispatcher.Instance().Enqueue(UpdateAccumulativePlayerCoins(string.IsNullOrEmpty(snapshot.GetRawJsonValue()) ? 0 : (long)snapshot.Value, newCoins));
            }
        });
    }

    IEnumerator UpdateAccumulativePlayerCoins(long oldCoins, Globals.ReceivedCoinTransaction newCoins)
    {
        yield return null;

        _accumulativePlyer_ref.Child("coins").SetValueAsync(oldCoins + newCoins.quantity).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Updated accumulative coins: " + (oldCoins + newCoins.quantity));
                UnityMainThreadDispatcher.Instance().Enqueue(UpdateAccumulativeCoinsInRoom(oldCoins + newCoins.quantity));
            }
        });
    }

    IEnumerator UpdateAccumulativeCoinsInRoom(long totalAccumulatedCoins)
    {
        yield return null;

        _room_ref.Child("accumulatedCoins").SetValueAsync(totalAccumulatedCoins).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Updated accumulative coins: " + totalAccumulatedCoins + " in room");
            }
        });
    }

    public void DestroyBot(string botFbId)
    {
        var _botToRemove = myBots.First(x => x.myFbKey == botFbId);
        myBots.Remove(_botToRemove);
        _botToRemove.DestroyBot(true);
    }

    public IEnumerator OnUpdateAccumulatedCoins(long newCoins)
    {
        yield return null;

        var _accumulatedCoins_ref = _room_ref.Child("accumulatedCoins");
        _accumulatedCoins_ref.GetValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Got accumulated coins");
                DataSnapshot snapshot = t.Result;
                print(snapshot.GetRawJsonValue());
                UnityMainThreadDispatcher.Instance().Enqueue(UpdateAccumulatedCoins(long.Parse(snapshot.GetRawJsonValue()), newCoins));
            }
        });
    }

    IEnumerator UpdateAccumulatedCoins(long oldCoins, long newCoins)
    {
        yield return null;

        var _accumulatedCoins_ref = _room_ref.Child("accumulatedCoins");
        _accumulatedCoins_ref.SetValueAsync(oldCoins + newCoins).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Total accumulated coins now: " + (oldCoins + newCoins));
            }
        });
    }
#endregion

    public void OnShowHideLobbyWindow(bool isShow)
    {
        StartCoroutine(ShowHideLobbyWindow(isShow));
    }
    
    IEnumerator ShowHideLobbyWindow(bool isShow)
    {
        Vector3 upPos = Vector3.up * lobbyWindowRectT.rect.size.y;
        Vector3 downPos = Vector3.zero;

        float animTime = 0.25f;
        float currentAnimTime = 0;

        while (currentAnimTime < animTime)
        {
            currentAnimTime += Time.deltaTime;

            lobbyWindowRectT.localPosition = Vector3.Lerp(isShow ? upPos : downPos, isShow ? downPos : upPos, currentAnimTime / animTime);

            yield return null;
        }

        lobbyWindowRectT.localPosition = isShow ? downPos : upPos;
    }

    void CreateTablesGradient()
    {
        gradients = new Gradient[2];
        gradients[0] = new Gradient();
        gradients[1] = new Gradient();

        List<string> _gradientColors1 = new List<string>() {
            "#F44336FF", "#E91E63FF", "#9C27B0FF", "#673AB7FF", "#3F51B5FF", "#2196F3FF", "#03A9F4FF", "#00BCD4FF"
        };
        List<string> _gradientColors2 = new List<string>() {
            "#FF5722FF", "#FF9800FF", "#FFC107FF", "#FFEB3BFF", "#CDDC39FF", "#8BC34AFF", "#4CAF50FF", "#009688FF"
        };

        colorKey = new GradientColorKey[_gradientColors1.Count];
        alphaKey = new GradientAlphaKey[_gradientColors1.Count];

        Color color;
        for (int i = 0; i < _gradientColors1.Count; i++)
        {
            if (ColorUtility.TryParseHtmlString(_gradientColors1[i], out color))
            {
                colorKey[i].color = color;
                colorKey[i].time = (float)i / ((float)_gradientColors1.Count - 1f);

                alphaKey[i].alpha = 1.0f;
                alphaKey[i].time = (float)i / ((float)_gradientColors1.Count - 1f);
            }
        }
        gradients[0].SetKeys(colorKey, alphaKey);

        colorKey = new GradientColorKey[_gradientColors2.Count];
        alphaKey = new GradientAlphaKey[_gradientColors2.Count];
        for (int i = 0; i < _gradientColors2.Count; i++)
        {
            if (ColorUtility.TryParseHtmlString(_gradientColors2[i], out color))
            {
                colorKey[i].color = color;
                colorKey[i].time = (float)i / ((float)_gradientColors1.Count - 1f);

                alphaKey[i].alpha = 1.0f;
                alphaKey[i].time = (float)i / ((float)_gradientColors1.Count - 1f);
            }
        }
        gradients[1].SetKeys(colorKey, alphaKey);

        // What's the color at the relative time 0.25 (25 %) ?
        //Debug.Log(gradient.Evaluate(0.25f));
    }

    private void OnApplicationPause(bool pause)
    {
        if (app.mainController.gameState == MainController.GameState.Playing && pause)
        {
            //Application.Quit();

            /*Dictionary<string, object> childUpdates = new Dictionary<string, object>();
            childUpdates["/isAway"] = pause;
            _players_ref.Child(curretRoomPlayerFbId).UpdateChildrenAsync(childUpdates).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Player pause updated");
                }
            });*/
        }
    }

    private void OnApplicationQuit()
    {
        StartCoroutine(EndPrivateRoomPlayerRefresh(false));
    }
}
