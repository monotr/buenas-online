﻿using UnityEngine;

public class eShopComponent : LoteriaOnlineElement
{
    public GameObject eShopWindow;

    public void OpeneShopWindow()
    {
        // Start puschases
        app.inAppPurchasesController.Init();

        eShopWindow.SetActive(true);
    }

    public void CloseeShopWindow()
    {
        eShopWindow.SetActive(false);
    }

    public void PurchaseCoins(int coinItemId)
    {
        app.promptComponent.ShowPrompt("Confirmar compra", "Está a punto de comprar " + (coinItemId == 0 ? "1,000" : "10,000") + " monedas virtuales por $" +
            (coinItemId == 0 ? "20MXN" : "50MXN"), Globals.PromptType.YesNoMessage, "BuyCoins," + coinItemId.ToString(), Globals.PromptColors.Blue);
    }
}
