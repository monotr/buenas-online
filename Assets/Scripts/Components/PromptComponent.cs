﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PromptComponent : LoteriaOnlineElement
{
    public GameObject promptPanel;
    public Image[] colorChangeImages;
    public TMP_Text titleText;
    public TMP_Text messageText;
    public TMP_InputField passwordInput;
    public TMP_InputField othersInput;
    public Button[] buttons;
    public Color[] promptColors; //Globals.PromptColors

    internal Globals.PromptType promptType;
    internal string args;

    private void Start()
    {
        promptPanel.SetActive(false);
    }

    public IEnumerator ShowPrompt_Routine(string _title, string _message, Globals.PromptType _promptType, string _args, Globals.PromptColors color)
    {
        yield return null;
        ShowPrompt(_title, _message, _promptType, _args, color);
    }
    
    public void ShowPrompt(string _title, string _message, Globals.PromptType _promptType, string _args, Globals.PromptColors color)
    {
        args = _args;
        if (args == null) args = string.Empty;

        if(args.Contains("OfferCreateRoom"))
            app.mainController.loadingPanel.SetActive(false);

        promptPanel.SetActive(true);
        buttons[1].gameObject.SetActive(_promptType == Globals.PromptType.YesNoMessage);
        messageText.gameObject.SetActive(!args.Contains("Password"));
        othersInput.gameObject.SetActive(args.Contains("ChangeName"));
        passwordInput.gameObject.SetActive(args.Contains("Password"));
        passwordInput.text = string.Empty;
        passwordInput.Select();

        titleText.text = _title;
        messageText.text = _message;

        foreach (var p in colorChangeImages)
            p.color = promptColors[(int)color];

        promptType = _promptType;

        app.mainController.loadingPanel.SetActive(false);
    }

    public void OnAcceptPrompt()
    {
        promptPanel.SetActive(false);

        if (promptType == Globals.PromptType.YesNoMessage)
        {
            string[] parts;
            if(args.Contains("ExitPrivateRoom"))
            {
                if (args.Contains("inGame") && app.mainController.currentRoom.playerCount > 1) // If not playing alone
                {
                    // Update player stats - Abandoned games
                    UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.OnUpdatePlayerStats(new Globals.PlayerStats()
                    {
                        abandonedGame = 1
                    }, null));
                }

                // Update player stats - Host time
                if (app.mainController.currentRoom.playerHostId == app.mainController.myPlayer.id)
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.OnUpdatePlayerStats(new Globals.PlayerStats()
                    {
                        hostTime = Time.time - app.newPrivateRoomComponent.hostingInitTime
                    }, null));
                }

                app.gameController.ExitPrivateRoom();
                app.privateRoomLobbyComponent.ClosePrivateRoomLobby();
            }
            else if (args.Contains("KickPlayer"))
            {
                parts = args.Split(',');
                app.privateRoomLobbyComponent.OnKickPlayer(parts[1]);
            }
            else if (args.Contains("RemoveBot"))
            {
                parts = args.Split(',');
                app.privateRoomLobbyComponent.DestroyBot(parts[1]);
            }
            else if (args.Contains("Password"))
            {
                parts = args.Split(',');
                app.privateRoomsListComponent.EnterRoom(parts[1] + "," + passwordInput.text);
            }
            else if(args.Contains("LogOut"))
            {
                app.loginController.LogOut();
            }
            else if (args.Contains("CloseGame"))
            {
                Application.Quit();
            }
            else if (args.Contains("VideoAd"))
            {
                app.adsController.ShowRewardedVideo();
            }
            else if (args.Contains("BuyTable"))
            {
                if (app.mainController.myPlayer.coins < 500)
                {
                    app.promptComponent.ShowPrompt(
                        "Oops, monedas insuficientes",
                        "Obtén monedas GRATIS viendo un pequeño video o compra un paquete de monedas!",
                        Globals.PromptType.ConfirmMessage,
                        null,
                        Globals.PromptColors.Yellow
                    );
                    app.eShopComponent.OpeneShopWindow();
                    return;
                }
                if (app.tableBuilderComponent.myTablesList.Count >= 32)
                {
                    app.promptComponent.ShowPrompt("Límite de tablas (32)", "Ya no puedes comprar más tablas", Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Red);
                    return;
                }

                app.tableBuilderComponent.OnConfirmBuyNewTable();
            }
            else if (args.Contains("NewAppVersion"))
            {
#if UNITY_ANDROID
                Application.OpenURL("https://play.google.com/store/apps/details?id=com.CerditoStudios.LoteriaMexicanaOnline");
                //#elif UNITY_IPHONE
                //Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
#endif
                Application.Quit();
            }
            else if(args.Contains("BuyCoins"))
            {
                parts = args.Split(',');

                app.inAppPurchasesController.BuyConsumableCoins(int.Parse(parts[1]));
            }
            else if(args.Contains("RemoveAvatarPhoto"))
            {
                app.profileController.RemoveAvatarPhoto();
            }
            else if (args.Contains("CancelFriendRequest"))
            {
                parts = args.Split(',');
                app.globalChatComponent.StartCoroutine(app.globalChatComponent.OnCancelFriendRequest(parts[1]));
            }
            else if (args.Contains("RemoveFriend"))
            {
                parts = args.Split(',');
                app.profileController.OnRemoveFriend(parts[1]);
            }
        }
    }

    public void OnCancelPrompt()
    {
        promptPanel.SetActive(false);

        if (args.Contains("NewAppVersion"))
        {
            Application.Quit();
        }
    }
}
