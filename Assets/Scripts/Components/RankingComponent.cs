﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RankingComponent : LoteriaOnlineElement
{
    public GameObject rankingsPanel;
    public TMP_Text rankingTitleText;

    int playersToLoad = -1;

    Dictionary<string, Dictionary<string, Globals.PlayerStats>> playerStatsDic;
    Dictionary<string, LoadedPlayer> loadedPlayers;

    public class LoadedPlayer
    {
        public string name;
        public string photoUrl;
        public Sprite photoSprite;
    }

    List<string> rankingNames = new List<string>() {
        "playedGames",
        "wonGames",
        "totalCoinsEarned",
        "maxStreak",
        "totalExperience", // incluir lvl en UI
        "hostTime",
    };
    List<string> rankingTitles = new List<string>() {
        "JUEGOS JUGADOS",
        "JUEGOS GANADOS",
        "MONEDAS GANADAS TOTALES",
        "MÁXIMA RACHA",
        "EXPERIENCIA TOTAL / NIVEL", // incluir lvl en UI
        "TIEMPO DE ANFITRIÓN",
    };

    public Image[] tabsImages;
    public Color selectTabColor;
    public Transform firstPlacesParent;
    public Transform otherPlacesParent;

    public class FirstPlacePanel
    {
        public Image playerImage;
        public Button profileButton;
        public GameObject itsYouBackImage;
        public TMP_Text playerName;
        public TMP_Text playerScore;

        public FirstPlacePanel(GameObject obj)
        {
            itsYouBackImage = obj.transform.GetChild(0).GetChild(0).gameObject;
            playerImage = obj.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>();
            profileButton = playerImage.gameObject.GetComponent<Button>();
            playerName = obj.transform.GetChild(1).GetComponent<TMP_Text>();
            playerScore = obj.transform.GetChild(2).GetComponent<TMP_Text>();
        }
    }
    List<FirstPlacePanel> firstPlacePanels;

    public class OtherPlacePanel
    {
        public Image playerImage;
        public Button profileButton;
        public GameObject itsYouBackImage;
        public TMP_Text playerName;
        public TMP_Text playerScore;

        public OtherPlacePanel(GameObject obj)
        {
            itsYouBackImage = obj.transform.GetChild(0).gameObject;
            playerImage = obj.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Image>();
            profileButton = playerImage.gameObject.GetComponent<Button>();
            playerName = obj.transform.GetChild(2).GetChild(1).GetComponent<TMP_Text>();
            playerScore = obj.transform.GetChild(2).GetChild(2).GetComponent<TMP_Text>();
        }
    }
    List<OtherPlacePanel> otherPlacePanels;

    //UI Raycast
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;
    int currentRankingIndex = -1;

    private void Start()
    {
        m_Raycaster = FindObjectOfType<Canvas>().GetComponent<GraphicRaycaster>();
        m_EventSystem = FindObjectOfType<Canvas>().GetComponent<EventSystem>();

        firstPlacePanels = new List<FirstPlacePanel>();
        foreach (Transform child in firstPlacesParent)
            firstPlacePanels.Add(new FirstPlacePanel(child.gameObject));
        var aux = firstPlacePanels[0];
        firstPlacePanels[0] = firstPlacePanels[1];
        firstPlacePanels[1] = aux;

        otherPlacePanels = new List<OtherPlacePanel>();
        foreach (Transform child in otherPlacesParent)
            otherPlacePanels.Add(new OtherPlacePanel(child.gameObject));
    }

    private void Update()
    {
        m_PointerEventData = new PointerEventData(m_EventSystem);

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Touch touch = Input.GetTouch(i);
                    TouchLogic(touch.position, touch.phase == TouchPhase.Began);
                }
            }
#else
        TouchLogic(Input.mousePosition, Input.GetMouseButtonDown(0));
#endif
    }

    void TouchLogic(Vector2 position, bool isDown)
    {
        // Game touch logic
        if (rankingsPanel.activeSelf)
        {
            m_PointerEventData.position = position;

            List<RaycastResult> results = new List<RaycastResult>();

            m_Raycaster.Raycast(m_PointerEventData, results);

            foreach (RaycastResult result in results)
            {
                //Debug.Log("Hit " + result.gameObject.tag);

                if (result.gameObject.tag == "RankingButton")
                {
                    int selected = -1;
                    foreach (var tab in tabsImages)
                    {
                        tab.color = (tab.gameObject != result.gameObject) ? Color.white : selectTabColor;

                        selected = (tab.gameObject != result.gameObject) ? selected : tabsImages.ToList().IndexOf(tab);
                    }

                    if(selected != currentRankingIndex)
                    {
                        currentRankingIndex = selected;
                        FillRank(rankingNames[selected]);
                        //StartCoroutine(FillRank(rankingNames[selected]));
                        //UnityMainThreadDispatcher.Instance().Enqueue(FillRank(rankingNames.First()));
                    }
                }
            }
        }
    }

    public void OnGetRanking()
    {
        app.mainController.ShowLoading("Obteniedo a los mejores jugadores");
        rankingsPanel.SetActive(true);

        playerStatsDic = new Dictionary<string, Dictionary<string, Globals.PlayerStats>>();
        loadedPlayers = new Dictionary<string, LoadedPlayer>();

        StartCoroutine(GetExcludedIds());
    }

    public void OnCloseRankings()
    {
        rankingsPanel.SetActive(false);
    }

    IEnumerator GetExcludedIds()
    {
        yield return null;

        FirebaseDatabase.DefaultInstance
            .GetReference("RankingExcludeIds")
            .GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    //print(_rankingName + " === " + snapshot.GetRawJsonValue());

                    List<string> excludedIds = new List<string>();
                    foreach (var c in snapshot.Children)
                        excludedIds.Add(c.GetRawJsonValue().Replace("\"", ""));

                    UnityMainThreadDispatcher.Instance().Enqueue(GetRanking(excludedIds));
                }
            });
    }

    IEnumerator GetRanking(List<string> excludedIds)
    {
        yield return null;

        foreach (var _rankingName in rankingNames)
        {
            playerStatsDic.Add(_rankingName, new Dictionary<string, Globals.PlayerStats>());
            Globals.PlayerStats ps;

            FirebaseDatabase.DefaultInstance
                .GetReference("PlayerStats").OrderByChild(_rankingName).LimitToLast(10 + excludedIds.Count)
                .GetValueAsync().ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                    // Handle the error...
                    }
                    else if (task.IsCompleted)
                    {
                        DataSnapshot snapshot = task.Result;
                        //print(_rankingName + " === " + snapshot.GetRawJsonValue());

                        foreach (var c in snapshot.Children)
                        {
                            if (excludedIds.Contains(c.Key))
                                continue;

                            ps = JsonUtility.FromJson<Globals.PlayerStats>(c.GetRawJsonValue());
                            playerStatsDic[_rankingName].Add(c.Key, ps);

                            if (!loadedPlayers.ContainsKey(c.Key))
                            {
                                loadedPlayers.Add(c.Key, new LoadedPlayer());

                                playersToLoad++;
                            }
                        }
                        playerStatsDic[_rankingName] = playerStatsDic[_rankingName].Reverse().ToDictionary(x => x.Key, x => x.Value);
                        while (playerStatsDic.Count > 10)
                            playerStatsDic.Remove(playerStatsDic.Last().Key);
                        if (rankingNames.IndexOf(_rankingName) == rankingNames.Count - 1)
                            UnityMainThreadDispatcher.Instance().Enqueue(LoadPlayersInfo());
                    }
                });

            yield return new WaitForSeconds(0.15f);
        }
    }

    IEnumerator LoadPlayersInfo()
    {
        playersToLoad = 0;

        foreach (var _rankingName in rankingNames)
        {
            //print(_rankingName + " loaded players: " + playerStatsDic[_rankingName].Count);
            playersToLoad += playerStatsDic[_rankingName].Count;

            string _playerId;
            for (int i = 0; i < playerStatsDic[_rankingName].Count; i++)
            {
                _playerId = playerStatsDic[_rankingName].ElementAt(i).Key;

                StartCoroutine(GetPlayerName(_playerId));
                
                if(i < 3)
                    StartCoroutine(GetPlayerPhoto(_playerId));
                else
                {
                    playersToLoad--;
                    loadedPlayers[_playerId].photoSprite = app.profileController.defaultProfileSprite;
                }

                yield return null;// new WaitForSeconds(0.05f);
            }
        }

        StartCoroutine(WaitToLoadEnd());
    }

    IEnumerator WaitToLoadEnd()
    {
        while (playersToLoad > 0)
        {
            yield return null;
        }
        app.mainController.loadingPanel.SetActive(false);

        FillRank(rankingNames.First());
        //UnityMainThreadDispatcher.Instance().Enqueue(FillRank(rankingNames.First()));
        //StartCoroutine(FillRank(rankingNames.First()));
        for (int i = 0; i < tabsImages.Length; i++)
            tabsImages[i].color = i == 0 ? selectTabColor : Color.white;
    }

    void FillRank(string _rankingName)
    {
        rankingTitleText.text = rankingTitles[rankingNames.IndexOf(_rankingName)];

        string _playerId;
        string _playerName;
        string _value;

        for (int i = 0; i < 10; i++)
        {
            if(playerStatsDic[_rankingName].Count >= i)
            {
                _playerId = playerStatsDic[_rankingName].ElementAt(i).Key;
                _playerName = loadedPlayers[_playerId].name;
                _value = GetPlayerRankingScore(_rankingName, i);
            }
            else
            {
                _playerId = "---";
                _playerName = "---";
                _value = "---";
            }

            if (i < 3)
            {
                firstPlacePanels[i].playerName.text = _playerName;
                if(loadedPlayers.ContainsKey(_playerId))
                    firstPlacePanels[i].playerImage.sprite = loadedPlayers[_playerId].photoSprite;
                firstPlacePanels[i].playerScore.text = _value;
                firstPlacePanels[i].itsYouBackImage.SetActive(app.mainController.myPlayer.id == _playerId);

                if(_playerId != "---")
                {
                    var _id = _playerId;
                    var _name = _playerName;
                    firstPlacePanels[i].profileButton.onClick.RemoveAllListeners();
                    firstPlacePanels[i].profileButton.onClick.AddListener(delegate
                    {
                        app.globalChatComponent.OnOpenPlayerProfile(_id, _name, Globals.ProfileOrigin.Rankings);
                    });
                }
            }
            else
            {
                otherPlacePanels[i - 3].playerName.text = _playerName;
                otherPlacePanels[i - 3].playerScore.text = _value;
                otherPlacePanels[i - 3].itsYouBackImage.SetActive(app.mainController.myPlayer.id == _playerId);

                if (_playerId != "---")
                {
                    var _id = _playerId;
                    var _name = _playerName;
                    otherPlacePanels[i - 3].profileButton.onClick.RemoveAllListeners();
                    otherPlacePanels[i - 3].profileButton.onClick.AddListener(delegate
                    {
                        app.globalChatComponent.OnOpenPlayerProfile(_id, _name, Globals.ProfileOrigin.Rankings);
                    });
                }
            }
        }
    }

    string GetPlayerRankingScore(string _rankingName, int index)
    {
        switch (_rankingName)
        {
            case "playedGames":
                return playerStatsDic[_rankingName].ElementAt(index).Value.playedGames.ToString();
            case "wonGames":
                return playerStatsDic[_rankingName].ElementAt(index).Value.wonGames.ToString();
            case "totalCoinsEarned":
                return playerStatsDic[_rankingName].ElementAt(index).Value.totalCoinsEarned.ToString();
            case "maxStreak":
                return playerStatsDic[_rankingName].ElementAt(index).Value.maxStreak.ToString();
            case "totalExperience":
                return playerStatsDic[_rankingName].ElementAt(index).Value.totalExperience.ToString("f0") + " - Nvl: " + playerStatsDic[_rankingName].ElementAt(index).Value.level;
            case "hostTime":
                TimeSpan t = TimeSpan.FromSeconds(playerStatsDic[_rankingName].ElementAt(index).Value.hostTime);
                return string.Format("{0:D2}h:{1:D2}m:{2:D2}s",
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds,
                                    t.Milliseconds);
            default:
                break;
        }

        return null;
    }

    IEnumerator GetPlayerName(string _playerId)
    {
        yield return null;

        FirebaseDatabase.DefaultInstance
            .GetReference("Players").Child(_playerId).Child("name")
            .GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    //print((string)snapshot.Value);

                    loadedPlayers[_playerId].name = (string)snapshot.Value;
                }
            });
    }

    IEnumerator GetPlayerPhoto(string _playerId)
    {
        yield return null;

        FirebaseDatabase.DefaultInstance
            .GetReference("Players").Child(_playerId).Child("profileImageUrl")
            .GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    print("-----FAULTED!!!-----");
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    //print((string)snapshot.Value);

                    loadedPlayers[_playerId].photoUrl = (string)snapshot.Value;

                    playersToLoad--;

                    if (string.IsNullOrEmpty(loadedPlayers[_playerId].photoUrl))
                    {
                        
                        //print("No photo: " + playersToLoad);
                        loadedPlayers[_playerId].photoSprite = app.profileController.defaultProfileSprite;
                    }
                    else
                        UnityMainThreadDispatcher.Instance().Enqueue(app.tools.DownloadFirebasePhoto(loadedPlayers[_playerId].photoUrl, _playerId, Globals.PhotoDownloadType.Ranking));
                }
            });
    }

    public IEnumerator SetPlayerPhoto(string _playerId, Texture2D _tex)
    {
        yield return null;

        loadedPlayers[_playerId].photoSprite = Sprite.Create(_tex, new Rect(0.0f, 0.0f, _tex.width, _tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        //playersToLoad--;
        //print("Loaded photo: " + playersToLoad);
    }

    private void OnApplicationQuit()
    {
        StopAllCoroutines();
    }
}
