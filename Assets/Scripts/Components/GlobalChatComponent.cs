﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GlobalChatComponent : LoteriaOnlineElement
{
    DatabaseReference _globalChat_Messages_ref;
    DatabaseReference _globalChat_Players_ref;
    string myChatFbKey;
    int MAX_MESSAGES = 100;

    public GameObject button;
    public GameObject globalChatWindow;

    public ScrollRect _textChatScrollRect;

    public Button EnterButton;
    public TMP_InputField MessageInputField;

    [Header("Keyboard fit")]
    public RectTransform messagesContainerRectT;
    bool isFittedToKeyboard = false;

    [Header("Messages")]
    public GameObject messageChatSelfPrefab;
    public GameObject messageChatOtherPrefab;
    public GameObject ChatContentObj;
    public class MessageChatSelf
    {
        public TMP_Text messageText;
        public TMP_Text dateTimeText;
        public HorizontalLayoutGroup horizontalLayout;

        public MessageChatSelf(GameObject obj)
        {
            messageText = obj.transform.GetChild(0).GetChild(1).GetComponentInChildren<TMP_Text>();
            dateTimeText = obj.transform.GetChild(1).GetComponent<TMP_Text>();
            horizontalLayout = obj.transform.GetChild(0).GetComponent<HorizontalLayoutGroup>();
        }
    }

    public class MessageChatOther
    {
        public TMP_Text playerNameText;
        public Image playerPhotoImage;
        public TMP_Text messageText;
        public TMP_Text dateTimeText;
        public HorizontalLayoutGroup horizontalLayout;

        public MessageChatOther(GameObject obj)
        {
            playerNameText = obj.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
            playerPhotoImage = obj.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>();
            messageText = obj.transform.GetChild(1).GetChild(1).GetChild(0).GetChild(1).GetComponentInChildren<TMP_Text>();
            dateTimeText = obj.transform.GetChild(1).GetChild(1).GetChild(1).GetComponent<TMP_Text>();
            horizontalLayout = obj.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<HorizontalLayoutGroup>();
        }
    }
    public GameObject playerStatusMessagePrefab;
    Dictionary<string, GameObject> messagesDic;
    Dictionary<string, Globals.ChatMessage> fbMessagesDic;
    internal Dictionary<string, Sprite> playerPhotosDic;
    public GameObject newMessagesIndicator;
    TMP_Text newMessagesCountText;
    Globals.ChatMessage lastChatMessage;

    [Header("Player counter")]
    public TMP_Text playerCountText;
    int playerCount;

    internal List<string> badWords = new List<string>() {
        "chingado", "chingada", "chingao", "chingadazo", "chingazo", "chinga", "chingadera", "chingaderita", "chingar", "chingadamadre", "chingadamare",
        "pendejo", "pendeja", "pendejazo", "pendejito", "pendejita",
        "puto", "putito", "puta", "putita",
        "zorra", "zorrita",
        "joto", "jotito",
        "maricón", "maricon",
        "verga", "vergazo", "vergo", "verguiza",
        "pucha", "panocha",
        "mames", "mamón", "mamon",
        "puñetas", "punetas",
        "culero", "culo",
        "emputado",
        "cabrón", "encabronado", "encabronao", "cabron",
        "pinche",
        "jodido", "jodida",
        "putiza",
        "estupido", "estupida", "idiota", "imbecil",
        "pene", "vagina", "escroto", "vulva",
        "mierda"
    };

    private void Start()
    {
        EnterButton.onClick.RemoveAllListeners();
        EnterButton.onClick.AddListener(delegate { SubmitMessage(MessageInputField.text); });
        MessageInputField.onSelect.AddListener(delegate { StartCoroutine(SendScrollRectToBottom()); });

        newMessagesCountText = newMessagesIndicator.GetComponentInChildren<TMP_Text>();
        newMessagesIndicator.SetActive(false);
    }

    private void Update()
    {
        if (!globalChatWindow.activeSelf)
            return;

        if(MessageInputField.isFocused)
        {
            if (!isFittedToKeyboard)
            {
                isFittedToKeyboard = true;
                StartCoroutine(FitScreenToKeyboard());
            }
        }
        else if(isFittedToKeyboard)
        {
            ResetScreenToNoKeyboard();
        }
    }

    IEnumerator FitScreenToKeyboard()
    {
        yield return new WaitForSeconds(0.5f);

#if !UNITY_EDITOR && UNITY_ANDROID
        int kh = MobileUtilities.GetKeyboardHeight(false);
        messagesContainerRectT.offsetMin = new Vector2(messagesContainerRectT.offsetMin.x, kh);
        messagesContainerRectT.offsetMax = new Vector2(messagesContainerRectT.offsetMax.x, kh);
#endif

        StartCoroutine(SendScrollRectToBottom());
    }

    void ResetScreenToNoKeyboard()
    {
        messagesContainerRectT.offsetMin = new Vector2(messagesContainerRectT.offsetMin.x, 0);
        messagesContainerRectT.offsetMax = new Vector2(messagesContainerRectT.offsetMax.x, 0);
        isFittedToKeyboard = false;
    }

    public void OpenGlobalChatWindow()
    {
        if(app == null) app = FindObjectOfType<LoteriaOnlineApplication>();

        globalChatWindow.SetActive(true);
        button.SetActive(false);

        MessageInputField.text = string.Empty;
        MessageInputField.Select();
        MessageInputField.ActivateInputField();

        ResetScreenToNoKeyboard();
        StartCoroutine(FitScreenToKeyboard());

        // Save last seen message


        // reset msg count
        newMessagesIndicator.SetActive(false);

        // Send online status
        myChatFbKey = _globalChat_Players_ref.Push().Key;

        _globalChat_Players_ref.Child(myChatFbKey).SetValueAsync(app.mainController.myPlayer.id, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Player registered in chat!");
            }
        });

        _globalChat_Players_ref.Child(myChatFbKey).OnDisconnect().RemoveValue().ContinueWith(t => {
            //SubmitMessage("<_!OFFLINE!_>");
        });
    }

    public void CloseGlobalWindow()
    {
        globalChatWindow.SetActive(false);
        button.SetActive(true);

        ResetScreenToNoKeyboard();

        // Send online status
        _globalChat_Players_ref.Child(myChatFbKey).RemoveValueAsync();
    }

    public void Init()
    {
        messagesDic = new Dictionary<string, GameObject>();
        fbMessagesDic = new Dictionary<string, Globals.ChatMessage>();
        playerPhotosDic = new Dictionary<string, Sprite>();
        ClearMessageObjectPool();

        playerCount = 0;

        _globalChat_Messages_ref = FirebaseDatabase.DefaultInstance.GetReference("GlobalChat").Child("Messages");
        _globalChat_Messages_ref.ChildAdded += Handle_GlobalChat_Messages_ChildAdded;
        _globalChat_Messages_ref.ChildRemoved += Handle_GlobalChat_Messages_ChildRemoved;

        _globalChat_Players_ref = FirebaseDatabase.DefaultInstance.GetReference("GlobalChat").Child("Players");
        _globalChat_Players_ref.ChildAdded += Handle_GlobalChat_Players_ChildAdded;
        _globalChat_Players_ref.ChildRemoved += Handle_GlobalChat_Players_ChildRemoved;

        print("=== GLOBAL CHAT STARTED ==");
    }

    void Handle_GlobalChat_Messages_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());
        Globals.ChatMessage chatMessage = JsonUtility.FromJson<Globals.ChatMessage>(args.Snapshot.GetRawJsonValue());

        CreateMessage(args.Snapshot.Key, chatMessage);

        // If you re-enter the room after leaving, and you receive the offline status--- destroy it
        if(globalChatWindow.activeSelf && chatMessage.playerId == app.mainController.myPlayer.id && chatMessage.message == "<_!offline!_>")
        {
            _globalChat_Messages_ref.Child(args.Snapshot.Key).RemoveValueAsync().ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Offline message deleted");
                }
            });
        }
    }

    void Handle_GlobalChat_Messages_ChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        // msg Count
        MessageCount(null);

        //print(args.Snapshot.GetRawJsonValue());
        //Globals.ChatMessage chatMessage = JsonUtility.FromJson<Globals.ChatMessage>(args.Snapshot.GetRawJsonValue());
        Destroy(messagesDic[args.Snapshot.Key]);
        messagesDic.Remove(args.Snapshot.Key);
        fbMessagesDic.Remove(args.Snapshot.Key);

        // First message, show other name
        var chatMessage = fbMessagesDic.OrderBy(x => x.Value.timestamp).First();
        if (app.mainController.myPlayer.id != chatMessage.Value.playerId && chatMessage.Value.message.ToLower() != "<_!online!_>" && chatMessage.Value.message.ToLower() != "<_!offline!_>")
        {
            var playerNameText = messagesDic[chatMessage.Key].transform.GetChild(0).GetComponentInChildren<TMP_Text>();
            playerNameText.text = chatMessage.Value.playerName;
            playerNameText.transform.parent.gameObject.SetActive(true);
        }
        StartCoroutine(SendScrollRectToBottom());
    }

    void Handle_GlobalChat_Players_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        playerCount++;
        playerCountText.text = playerCount.ToString();

        //print(args.Snapshot.GetRawJsonValue());
        string _playerId = (string)args.Snapshot.Value;
        if (_playerId == app.mainController.myPlayer.id)
        {
            SubmitMessage("<_!ONLINE!_>");
        }
    }

    void Handle_GlobalChat_Players_ChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        playerCount--;
        playerCountText.text = playerCount.ToString();

        //print(args.Snapshot.GetRawJsonValue());
        string _playerId = (string)args.Snapshot.Value;
        if (_playerId == app.mainController.myPlayer.id)
        {
            //SubmitMessage("<_!OFFLINE!_>");
        }
    }

    void CreateMessage(string fbKey, Globals.ChatMessage chatMessage)
    {
        if (app == null)
            app = FindObjectOfType<LoteriaOnlineApplication>();

        GameObject newMessageObj = null;
        TMP_Text txt;
        if (chatMessage.message.Equals("<_!online!_>"))
        {
            newMessageObj = Instantiate(playerStatusMessagePrefab, ChatContentObj.transform);
            txt = newMessageObj.GetComponentInChildren<TMP_Text>();
            txt.text = "--- <b>" + chatMessage.playerName + "</b> ha entrado al chat! ---";
            txt.color = Color.white;
            StartCoroutine(FitMessageBubble(newMessageObj.GetComponentInChildren<HorizontalLayoutGroup>()));
            newMessageObj.GetComponent<RectTransform>().position = Vector3.zero;
        }
        else if (chatMessage.message.Equals("<_!offline!_>"))
        {
            newMessageObj = Instantiate(playerStatusMessagePrefab, ChatContentObj.transform);
            txt = newMessageObj.GetComponentInChildren<TMP_Text>();
            txt.text = "--- <b>" + chatMessage.playerName + "</b> salió del chat ---";
            txt.color = Color.red;
            StartCoroutine(FitMessageBubble(newMessageObj.GetComponentInChildren<HorizontalLayoutGroup>()));
            newMessageObj.GetComponent<RectTransform>().position = Vector3.zero;
        }
        else // regular message
        {
            int missingSpaces = 12 - chatMessage.message.Length;
            if (missingSpaces > 0)
                for (int i = 0; i < missingSpaces; i++)
                    chatMessage.message = "  " + chatMessage.message;

            if (app.mainController.myPlayer.id == chatMessage.playerId) // your messages
            {
                newMessageObj = Instantiate(messageChatSelfPrefab, ChatContentObj.transform);
                MessageChatSelf messageChat = new MessageChatSelf(newMessageObj);
                messageChat.messageText.text = chatMessage.message;
                messageChat.dateTimeText.text = DateTimeOffset.FromUnixTimeMilliseconds(chatMessage.timestamp).DateTime.ToString("dd/MM/yyyy hh:mm tt");
                StartCoroutine(FitMessageBubble(messageChat.horizontalLayout));
            }
            else // other player message
            {
                newMessageObj = Instantiate(messageChatOtherPrefab, ChatContentObj.transform);
                MessageChatOther messageChat = new MessageChatOther(newMessageObj);
                messageChat.playerNameText.text = chatMessage.playerName;

                messageChat.messageText.text = chatMessage.message;
                messageChat.dateTimeText.text = DateTimeOffset.FromUnixTimeMilliseconds(chatMessage.timestamp).DateTime.ToString("dd/MM/yyyy hh:mm tt");
                StartCoroutine(FitMessageBubble(messageChat.horizontalLayout));
            }
        }

        messagesDic.Add(fbKey, newMessageObj);
        fbMessagesDic.Add(fbKey, chatMessage);

        // Order messages timestamp
        List<string> orderedKeys = fbMessagesDic.OrderBy(x => x.Value.timestamp).ToDictionary(x => x.Key, x => x.Value).Keys.ToList();
        foreach (var key in orderedKeys)
        {
            // Order message on list
            messagesDic[key].transform.SetSiblingIndex(orderedKeys.IndexOf(key));

            // hide or show player name and photo
            if (app.mainController.myPlayer.id != fbMessagesDic[key].playerId &&
                fbMessagesDic[key].message.ToLower() != "<_!online!_>" &&
                fbMessagesDic[key].message.ToLower() != "<_!offline!_>") // other messages
            {
                if (orderedKeys.IndexOf(key) - 1 < 0)
                    continue;

                string lastKey = orderedKeys.ElementAt(orderedKeys.IndexOf(key) - 1);
                var last = fbMessagesDic[lastKey];

                MessageChatOther messageChat = new MessageChatOther(messagesDic[key]);

                // name
                messageChat.playerNameText.transform.parent.gameObject.SetActive(last.playerId != fbMessagesDic[key].playerId);

                if (last.message.ToLower() == "<_!online!_>" || last.message.ToLower() == "<_!offline!_>")
                    messageChat.playerNameText.transform.parent.gameObject.SetActive(true);

                // Load photo
                if (!playerPhotosDic.ContainsKey(fbMessagesDic[key].playerId))
                {
                    playerPhotosDic.Add(fbMessagesDic[key].playerId, null);
                    StartCoroutine(GetPlayerPhoto(fbMessagesDic[key].playerId, Globals.PhotoDownloadType.GlobalChat));
                }
                else
                {
                    // Set photo
                    messageChat.playerPhotoImage.sprite = playerPhotosDic[fbMessagesDic[key].playerId];
                    // Set profile button action
                    messageChat.playerPhotoImage.transform.GetComponent<Button>().onClick.RemoveAllListeners();
                    messageChat.playerPhotoImage.transform.GetComponent<Button>().onClick.AddListener(delegate {
                        OnOpenPlayerProfile(fbMessagesDic[key].playerId, fbMessagesDic[key].playerName, Globals.ProfileOrigin.GlobalChat);
                    });
                }

                // If last was a message from the same, move the player photo
                if (last.playerId != app.mainController.myPlayer.id && fbMessagesDic[key].playerId == last.playerId &&
                    !last.message.ToLower().Equals("<_!online!_>") && !last.message.ToLower().Equals("<_!offline!_>"))
                {
                    MessageChatOther lastMessageChat = new MessageChatOther(messagesDic[lastKey]);
                    lastMessageChat.playerPhotoImage.transform.parent.gameObject.GetComponent<Image>().color = new Color(0, 0, 0, 0);
                    lastMessageChat.playerPhotoImage.transform.GetComponent<Button>().onClick.RemoveAllListeners();
                    lastMessageChat.playerPhotoImage.transform.GetComponent<Button>().interactable = false;
                }
            }
        }
        // Save last message
        lastChatMessage = fbMessagesDic[orderedKeys.Last()];
        if(globalChatWindow.activeSelf)
        {
            PlayerPrefs.SetString("lastGlobalChatMessage", orderedKeys.Last());
        }
        // msg Count
        MessageCount(orderedKeys);

        // If you are the creator of the message, destroy the oldest if msg list is full
        if (chatMessage.playerId == app.mainController.myPlayer.id)
        {
            //print("messagesDic.Count: " + messagesDic.Count);
            if (messagesDic.Count > MAX_MESSAGES)
            {
                // Delete oldest message
                var _firstmessageid = fbMessagesDic.OrderBy(x => x.Value.timestamp).First().Key;
                _globalChat_Messages_ref.Child(_firstmessageid).RemoveValueAsync().ContinueWith(t => {
                    if (t.IsFaulted)
                    {
                        Debug.Log("Faulted.." + t.Exception.Message);
                    }

                    if (t.IsCanceled)
                    {
                        Debug.Log("Cancelled..");
                    }

                    if (t.IsCompleted)
                    {
                        Debug.Log("Completed! - Oldest message");
                    }
                });
            }
        }

        StartCoroutine(SendScrollRectToBottom());
    }

    void MessageCount(List<string> orderedKeys)
    {
        if(orderedKeys == null)
            orderedKeys = fbMessagesDic.OrderBy(x => x.Value.timestamp).ToDictionary(x => x.Key, x => x.Value).Keys.ToList();

        string _lastMsgId = PlayerPrefs.GetString("lastGlobalChatMessage", null);
        if (!string.IsNullOrEmpty(_lastMsgId))
        {
            if (orderedKeys.Contains(_lastMsgId))
            {
                int index = orderedKeys.IndexOf(_lastMsgId);
                int lastIndex = orderedKeys.Count - 2;
                int msgCount = lastIndex - index;
                msgCount = msgCount < 0 ? 0 : msgCount;

                newMessagesCountText.text = msgCount >= 99 ? "99+" : msgCount.ToString();
                newMessagesIndicator.SetActive(msgCount > 0);
            }
            else
            {
                newMessagesCountText.text = orderedKeys.Count >= 99 ? "99+" : orderedKeys.Count.ToString();
                newMessagesIndicator.SetActive(orderedKeys.Count > 0);
            }
        }
        else
        {
            newMessagesCountText.text = orderedKeys.Count >= 99 ? "99+" : orderedKeys.Count.ToString();
            newMessagesIndicator.SetActive(orderedKeys.Count > 0);
        }
    }

    IEnumerator FitMessageBubble(HorizontalLayoutGroup _horizontalLayout)
    {
        yield return new WaitForEndOfFrame();

        if(_horizontalLayout != null)
            _horizontalLayout.enabled = false;

        yield return new WaitForEndOfFrame();
        
        if (_horizontalLayout != null)
            _horizontalLayout.enabled = true;

        yield return null;
    }

    private void ClearMessageObjectPool()
    {
        foreach (Transform child in ChatContentObj.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void SubmitMessage(string sendMessage)
    {
        if (string.IsNullOrEmpty(sendMessage))
        {
            return;
        }

        string send_msg = BadWordValidator(sendMessage.ToLower());

        MessageInputField.text = string.Empty;
        MessageInputField.Select();
        MessageInputField.ActivateInputField();

        string key = _globalChat_Messages_ref.Push().Key;

        if(app == null)
            app = FindObjectOfType<LoteriaOnlineApplication>();

        Globals.ChatMessage newMessage = new Globals.ChatMessage()
        {
            playerId = app.mainController.myPlayer.id,
            playerName = app.mainController.myPlayer.name,
            message = send_msg
        };
        string json = JsonUtility.ToJson(newMessage);
        json = json.Substring(0, json.Length - 1);
        json += @" , ""timestamp"": {"".sv"" : ""timestamp""} } ";

        _globalChat_Messages_ref.Child(key).SetRawJsonValueAsync(json, 1).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Meesage sent");
            }
        });
    }

    public string BadWordValidator(string text)
    {
        string send_msg = text;
        string replaced = string.Empty;
        foreach (var bw in badWords)
        {
            if (send_msg.Contains(bw))
            {
                for (int i = 0; i < bw.Length; i++)
                {
                    replaced += ((i < bw.Length / 3) ? bw[i].ToString() : "*");
                }
                while (send_msg.Contains(bw))
                {
                    send_msg = send_msg.Replace(bw, replaced);
                }
            }
        }
        return send_msg;
    }

    private IEnumerator SendScrollRectToBottom()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        TouchScreenKeyboard.hideInput = true;
#endif

        var _vertical = ChatContentObj.GetComponent<VerticalLayoutGroup>();
        _vertical.childForceExpandHeight = false;

        yield return new WaitForEndOfFrame();

        _vertical.childForceExpandHeight = true;

        yield return new WaitForEndOfFrame();
        // We need to wait for the end of the frame for this to be updated, otherwise it happens too quickly.
        _textChatScrollRect.normalizedPosition = new Vector2(0, 0);

        yield return null;
    }

    public IEnumerator GetPlayerPhoto(string _playerId, Globals.PhotoDownloadType photoDownloadType)
    {
        yield return null;

        FirebaseDatabase.DefaultInstance
            .GetReference("Players").Child(_playerId).Child("profileImageUrl")
            .GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    print("-----FAULTED!!!-----");
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    //print((string)snapshot.Value);

                    string photoUrl = (string)snapshot.Value;

                    if (string.IsNullOrEmpty(photoUrl))
                    {
                        //print("No photo: " + playersToLoad);
                        if(playerPhotosDic.ContainsKey(_playerId))
                            playerPhotosDic[_playerId] = app.profileController.defaultProfileSprite;
                        else
                            playerPhotosDic.Add(_playerId, app.profileController.defaultProfileSprite);

                        var _tex = app.profileController.defaultProfileSprite.texture;
                        switch (photoDownloadType)
                        {
                            case Globals.PhotoDownloadType.LobbyPhoto:
                                UnityMainThreadDispatcher.Instance().Enqueue(app.privateRoomLobbyComponent.SetPlayerPhoto(_playerId, _tex));
                                break;
                            case Globals.PhotoDownloadType.Ranking:
                                UnityMainThreadDispatcher.Instance().Enqueue(app.rankingComponent.SetPlayerPhoto(_playerId, _tex));
                                break;
                            case Globals.PhotoDownloadType.GlobalChat:
                                UnityMainThreadDispatcher.Instance().Enqueue(app.globalChatComponent.SetPlayerPhoto(_playerId, _tex));
                                break;
                            case Globals.PhotoDownloadType.Friends:
                                UnityMainThreadDispatcher.Instance().Enqueue(app.friendsComponent.SetPlayerPhoto(_playerId, _tex));
                                break;
                            case Globals.PhotoDownloadType.GameNotification:
                                UnityMainThreadDispatcher.Instance().Enqueue(app.gameNotificationsComponent.SetPlayerPhoto(_playerId, _tex));
                                break;
                            case Globals.PhotoDownloadType.Profile:
                                UnityMainThreadDispatcher.Instance().Enqueue(app.globalChatComponent.SetOtherPlayerPhoto(_playerId, _tex));
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        UnityMainThreadDispatcher.Instance().Enqueue(app.tools.DownloadFirebasePhoto(photoUrl, _playerId, photoDownloadType));
                }
            });
    }

    public IEnumerator SetOtherPlayerPhoto(string _playerId, Texture2D _tex)
    {
        yield return null;

        app.globalChatComponent.playerPhotosDic[_playerId] = Sprite.Create(_tex, new Rect(0.0f, 0.0f, _tex.width, _tex.height), new Vector2(0.5f, 0.5f), 100.0f);

        app.profileController.profileImage.sprite = app.globalChatComponent.playerPhotosDic[_playerId];
    }

    public IEnumerator SetPlayerPhoto(string _playerId, Texture2D _tex)
    {
        yield return null;
        //print("Setting chat player photos!");

        playerPhotosDic[_playerId] = Sprite.Create(_tex, new Rect(0.0f, 0.0f, _tex.width, _tex.height), new Vector2(0.5f, 0.5f), 100.0f);

        foreach (var chatBubble in fbMessagesDic)
        {
            if(chatBubble.Value.playerId == _playerId &&
                !chatBubble.Value.message.ToLower().Equals("<_!online!_>") && !chatBubble.Value.message.ToLower().Equals("<_!offline!_>"))
            {
                MessageChatOther messageChat = new MessageChatOther(messagesDic[chatBubble.Key]);
                messageChat.playerPhotoImage.sprite = playerPhotosDic[_playerId];
            }
        }
    }

    public void OnOpenPlayerProfile(string _playerId, string _playerName, Globals.ProfileOrigin _profileOrigin)
    {
        app.profileController.isOtherProfile = true;

        // Show loading
        app.mainController.ShowLoading("Obteniendo información de jugador");

        app.profileController.profileOrigin = _profileOrigin;
        switch (_profileOrigin)
        {
            case Globals.ProfileOrigin.GlobalChat:
                app.globalChatComponent.globalChatWindow.SetActive(false);
            break;
            case Globals.ProfileOrigin.FriendsPanel:
                app.friendsComponent.OnCloseFriendWindow();
                break;
            case Globals.ProfileOrigin.Lobby:

                break;
            case Globals.ProfileOrigin.Rankings:

                break;
            case Globals.ProfileOrigin.WinnerPanel:

                break;
        }

        // Show profile window
        app.profileController.uploadingImagePanel.SetActive(false);
        app.mainController.profileWindow.SetActive(true);

        // photo buttons
        app.profileController.deletePhotoButton.SetActive(false);
        app.profileController.uploadPhotoButton.gameObject.SetActive(false);

        // photo
        if (!playerPhotosDic.ContainsKey(_playerId))
        {
            app.profileController.profileImage.sprite = app.profileController.defaultProfileSprite;
            playerPhotosDic.Add(_playerId, null);
            StartCoroutine(GetPlayerPhoto(_playerId, Globals.PhotoDownloadType.Profile));
        }
        else
        {
            // Set photo
            app.profileController.profileImage.sprite = playerPhotosDic[_playerId];
        }

        // nickname - button
        app.profileController.editNicknameButton.SetActive(false);
        app.profileController.nicknameInput.gameObject.SetActive(false);
        app.profileController.changeNameButton.gameObject.SetActive(false);
        app.profileController.playerNicknameText.gameObject.SetActive(true);

        // nickname
        app.profileController.playerNicknameText.text = _playerName;

        // friend buttons
        //  friend buttons actions
        var addFriendBut = app.profileController.addFriendButton.GetComponent<Button>();
        addFriendBut.onClick.RemoveAllListeners();
        addFriendBut.onClick.AddListener(delegate {
            app.profileController.addFriendButton.SetActive(false);
            app.profileController.OnSendFriendRequest(0, app.mainController.myPlayer.id, _playerId, app.mainController.myPlayer.name);
        });

        var removeFriendBut = app.profileController.removeFriendButton.GetComponent<Button>();
        removeFriendBut.onClick.RemoveAllListeners();
        removeFriendBut.onClick.AddListener(delegate {
            app.promptComponent.ShowPrompt("Remover amigo",
                "¿Seguro que deseas remover tu amistad con \"" + app.profileController.playerNicknameText.text + "\"?",
                Globals.PromptType.YesNoMessage, "RemoveFriend," + _playerId, Globals.PromptColors.Red);
        });

        var waitingFriendBut = app.profileController.waitingFriendButton.GetComponent<Button>();
        waitingFriendBut.onClick.RemoveAllListeners();
        waitingFriendBut.onClick.AddListener(delegate {
            app.promptComponent.ShowPrompt("Cancelar solicitud",
                "¿Seguro que deseas cancelar la solicitud de amistad con \"" + app.profileController.playerNicknameText.text + "\"?",
                Globals.PromptType.YesNoMessage, "CancelFriendRequest," + _playerId, Globals.PromptColors.Red);
        });

        // Toggle buttons
        UnityMainThreadDispatcher.Instance().Enqueue(SetFriendRequestButtons(_playerId));

        // send coins
        if(app.mainController.myPlayer.friendList.Count(x => ((Globals.Friend)x.Value).playerId == _playerId) > 0)
        {
            app.profileController.sendCoinsButton.SetActive(true);
            var sendCoinsBtn = app.profileController.sendCoinsButton.GetComponent<Button>();
            sendCoinsBtn.onClick.RemoveAllListeners();
            sendCoinsBtn.onClick.AddListener(delegate
            {
                app.sendCoinsComponent.OpenSendCoinsWindow(_playerId);
            });
        }
        else
        {
            app.profileController.sendCoinsButton.SetActive(false);
        }

        app.profileController.sendMessageButton.SetActive(false); // TODO

        //other buttons
        app.profileController.infoButton.SetActive(false);
        app.profileController.closeSessionButton.SetActive(false);

        // Get player stats
        DatabaseReference _playerStats_ref = FirebaseDatabase.DefaultInstance.GetReference("PlayerStats/" + _playerId);
        _playerStats_ref.GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                string json = snapshot.GetRawJsonValue();
                //print(json);

                Globals.PlayerStats playerStats = JsonUtility.FromJson<Globals.PlayerStats>(json);
                if (playerStats == null) playerStats = new Globals.PlayerStats();

                UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.FillProfilePlayerStats(playerStats));
            }
        });
    }

    internal IEnumerator SetFriendRequestButtons(string _playerId)
    {
        app.profileController.addFriendButton.SetActive(false);
        app.profileController.removeFriendButton.SetActive(false);
        app.profileController.waitingFriendButton.SetActive(false);
        app.profileController.sendCoinsButton.SetActive(false);

        // Toggle buttons
        bool isFriend = app.mainController.myPlayer.friendList.Count(x => ((Globals.Friend)x.Value).playerId == _playerId) > 0;
        app.profileController.addFriendButton.SetActive(!isFriend);
        if (isFriend)
        {
            Globals.Friend friend = (Globals.Friend)app.mainController.myPlayer.friendList.First(x => ((Globals.Friend)x.Value).playerId == _playerId).Value;

            if (friend.isConfirmed)
            {
                app.profileController.removeFriendButton.SetActive(true);
                app.profileController.sendCoinsButton.SetActive(true);
            }
            else
            {
                app.profileController.waitingFriendButton.SetActive(true);
            }
        }

        yield return null;
    }

    public IEnumerator OnCancelFriendRequest(string _destinyId)
    {
        // Friend buttons
        app.profileController.waitingFriendButton.SetActive(false);
        app.profileController.addFriendButton.SetActive(true);

        // Send friend request remove
        var friendId = app.mainController.myPlayer.friendList.First(x => ((Globals.Friend)x.Value).playerId == _destinyId).Key;
        app.profileController.OnSendFriendRequest(1, app.mainController.myPlayer.id, _destinyId, friendId);

        var _addFriendButton = app.profileController.addFriendButton.GetComponent<Button>();
        _addFriendButton.interactable = false;

        yield return null;
    }

    public IEnumerator FinishRemoveFriendRequest(string key)
    {
        if (app.mainController.myPlayer.friendList.ContainsKey(key))
            app.mainController.myPlayer.friendList.Remove(key);

        app.profileController.addFriendButton.GetComponent<Button>().interactable = true;
        yield return null;
    }

    private void OnApplicationQuit()
    {
        //if(globalChatWindow.activeSelf)
            //SubmitMessage("<_!OFFLINE!_>");
    }
}
