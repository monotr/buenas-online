﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DeckCreatorComponent : LoteriaOnlineElement
{
    bool createdDeck = false;

    private void Update()
    {
        if (app.mainController.gameState != MainController.GameState.LoggedIn)
            return;

        if (createdDeck)
            return;

        createdDeck = true;

        //StartCoroutine(GenerateDeck("D:/Git/Loteria_Online/Assets/Textures/Loteria_Decks/Clásica"));
    }

    public IEnumerator GenerateDeck(string deckPath)
    {
        yield return new WaitForSeconds(5);

        // Set this before calling into the realtime database.
        app.mainController.fbApp = FirebaseApp.DefaultInstance;
        app.mainController.fbApp.SetEditorDatabaseUrl("https://loteriamexicanaonline.firebaseio.com/");

        // Get the root reference location of the database.
        app.mainController.mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        Globals.Deck deck = new Globals.Deck();
        deck.cardList = new List<Globals.Card>();
        Globals.Card newCard;

        string[] dirs = Directory.GetFiles(deckPath, "*.png");
        FileInfo fi;
        string name;
        string[] parts;

        deck.id = Guid.NewGuid().ToString();
        deck.name = Path.GetFileName(deckPath);

        foreach (var dir in dirs)
        {
            fi = new FileInfo(dir);
            name = fi.Name.Replace(fi.Extension, "");
            parts = name.Split(' ');

            name = string.Empty;
            for (int i = 1; i < parts.Length; i++)
                name += parts[i] + " ";
            name = name.Substring(0, name.Length - 1);

            newCard = new Globals.Card()
            {
                id = parts[0],
                name = name,
                imageBase64 = "",//PNGfile_to_base64(dir)
            };
            deck.cardList.Add(newCard);
        }

        string json = JsonUtility.ToJson(deck);
        print(json);

        var playersDB = app.mainController.mDatabaseRef.Child("Decks");
        try
        {
            playersDB.Child(deck.id).SetRawJsonValueAsync(json, 1).ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Deck created");
                    Destroy(gameObject);
                    //UnityMainThreadDispatcher.Instance().Enqueue(app.mainController.ChangeWindow());
                }
            });
        }
        catch(Exception e)
        {
            print(e.Message);
        }
    }

    public static string PNGfile_to_base64(string filePath)
    {
        byte[] fileDataBytes = File.ReadAllBytes(filePath);
        string base64 = Convert.ToBase64String(fileDataBytes);

        return base64;
    }
}
