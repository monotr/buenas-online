﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FriendsComponent : LoteriaOnlineElement
{
    public GameObject friendWindow;

    public GameObject friendOnListPrefab;
    public Color[] playerStateColors;
    public Transform friendsOnListGrid;

    public TMP_Text[] playerCountTexts;
    internal bool isFromTable;

    public class FriendOnList
    {
        public Image playerPhotoImage;
        public Image stateImage;
        public TMP_Text playerNameText;
        public Button profileButton;
        public Button messageButton;
        public Button inviteButton;
        public Button sendCoinsButton;
        public int playerState;
        public Globals.Player myPlayer;
        public Globals.Friend friend;
        public GameObject myObject;

        public FriendOnList(GameObject obj, Globals.Player _player)
        {
            myObject = obj;
            myPlayer = _player;

            playerPhotoImage = obj.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>();
            stateImage = obj.transform.GetChild(0).GetChild(1).GetComponent<Image>();
            playerNameText = obj.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>();
            sendCoinsButton = obj.transform.GetChild(2).GetChild(0).GetComponent<Button>();
            profileButton = obj.transform.GetChild(2).GetChild(1).GetComponent<Button>();
            messageButton = obj.transform.GetChild(2).GetChild(2).GetComponent<Button>();
            inviteButton = obj.transform.GetChild(2).GetChild(3).GetComponent<Button>();
        }
    }
    internal List<FriendOnList> friendOnListList;

    DatabaseReference _playerFriends_ref;
    DatabaseReference searchPlayer_ref;
    Coroutine refreshRoutine;

    [Header("Search Friend")]
    public GameObject serachFriendButton;
    public GameObject searchiendPanel;
    public TMP_InputField searchFriendInput;
    Globals.PlayerSearchType playerSearchType;

    public void Init()
    {
        app.mainController.myPlayer.friendList = new Dictionary<string, object>();
        // Destroy old friends
        foreach (Transform child in friendsOnListGrid)
            Destroy(child.gameObject);
        friendOnListList = new List<FriendOnList>();
        UpdateCounters();

        _playerFriends_ref = FirebaseDatabase.DefaultInstance.GetReference("Players").Child(app.mainController.myPlayer.id).Child("friendList");
        _playerFriends_ref.ChildAdded += Handle_Friends_ChildAdded;
        _playerFriends_ref.ChildChanged += Handle_Friends_ChildChanged;
        _playerFriends_ref.ChildRemoved += Handle_Friends_ChildRemoved;

        // Enable menu buttons
        foreach (var btn in app.mainController.waitLoadMenuButtons)
            btn.interactable = true;
    }

    void Handle_Friends_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        DataSnapshot snapshot = args.Snapshot;
        string json = snapshot.GetRawJsonValue();
        //print("FRIEND ADDED: " + json);

        var _friend = JsonUtility.FromJson<Globals.Friend>(json);
        app.mainController.myPlayer.friendList.Add(snapshot.Key, _friend);

        LoadFriendInfo(_friend, false);
    }

    void Handle_Friends_ChildChanged(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        DataSnapshot snapshot = args.Snapshot;
        string json = snapshot.GetRawJsonValue();
        print("FRIEND CHANGED: " + json);

        var _friend = JsonUtility.FromJson<Globals.Friend>(json);
        app.mainController.myPlayer.friendList.Remove(snapshot.Key);
        app.mainController.myPlayer.friendList.Add(snapshot.Key, _friend);

        LoadFriendInfo(_friend, true);
    }

    void Handle_Friends_ChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        DataSnapshot snapshot = args.Snapshot;
        string json = snapshot.GetRawJsonValue();
        print("FRIEND REMOVED: " + json);

        var _friend = JsonUtility.FromJson<Globals.Friend>(json);
        app.mainController.myPlayer.friendList.Remove(snapshot.Key);

        if(friendOnListList.Count(x => x.myPlayer.id == _friend.playerId) > 0)
        {
            var fol = friendOnListList.First(x => x.myPlayer.id == _friend.playerId);
            Destroy(fol.myObject);
            friendOnListList.Remove(fol);
        }

        UpdateCounters();

        // If on profile, update
        if (app.mainController.profileWindow.activeSelf && app.profileController.isOtherProfile)
        {
            app.globalChatComponent.StartCoroutine(app.globalChatComponent.SetFriendRequestButtons(_friend.playerId));
        }
    }

    public void OnOpenFriendWindow(bool _fromTable)
    {
        isFromTable = _fromTable;

        friendWindow.SetActive(true);

        serachFriendButton.SetActive(!isFromTable);

        if (refreshRoutine != null)
            StopCoroutine(refreshRoutine);
        refreshRoutine = StartCoroutine(RefreshRoutine());
    }

    public void OnCloseFriendWindow()
    {
        friendWindow.SetActive(false);

        if (refreshRoutine != null)
            StopCoroutine(refreshRoutine);
    }

    IEnumerator RefreshRoutine()
    {
        while (true)
        {
            foreach (var friend in app.mainController.myPlayer.friendList)
            {
                LoadFriendInfo((Globals.Friend)friend.Value, true);
                yield return new WaitForSeconds(0.01f);
            }

            yield return new WaitForSeconds(1f);
        }
    }

    void LoadFriendInfo(Globals.Friend friend, bool isUpdate)
    {
        FirebaseDatabase.DefaultInstance
            .GetReference("Players").Child(friend.playerId)
            .GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    print("-----FAULTED!!!-----");
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    //Debug.Log("Completed! - Got friend info");

                    DataSnapshot snapshot = task.Result;
                    //print(snapshot.GetRawJsonValue());

                    Globals.Player _player = JsonUtility.FromJson<Globals.Player>(snapshot.GetRawJsonValue());

                    if (!isUpdate)
                        UnityMainThreadDispatcher.Instance().Enqueue(CreateFriendOnList(friend, _player));
                    else
                        UnityMainThreadDispatcher.Instance().Enqueue(UpdateFriendOnList(friend, _player));
                }
            });
    }

    IEnumerator CreateFriendOnList(Globals.Friend _friend, Globals.Player _player)
    {
        yield return null;

        GameObject newPlayer = Instantiate(friendOnListPrefab, friendsOnListGrid);
        FriendOnList fol = new FriendOnList(newPlayer, _player);
        fol.friend = _friend;
        fol.playerNameText.text = _player.name;

        // set player photo
        if (app.globalChatComponent.playerPhotosDic.ContainsKey(_player.id))
        {
            if(app.globalChatComponent.playerPhotosDic[_player.id] != null)
                fol.playerPhotoImage.sprite = app.globalChatComponent.playerPhotosDic[_player.id];
            else
                app.globalChatComponent.StartCoroutine(app.globalChatComponent.GetPlayerPhoto(_player.id, Globals.PhotoDownloadType.Friends));
        }
        else
        {
            app.globalChatComponent.playerPhotosDic.Add(_player.id, null);
            app.globalChatComponent.StartCoroutine(app.globalChatComponent.GetPlayerPhoto(_player.id, Globals.PhotoDownloadType.Friends));
        }

        // Invite button
        fol.inviteButton.gameObject.SetActive(isFromTable);

        // Message button
        fol.messageButton.gameObject.SetActive(false); /// TODO all the logic of this shit

        // Profile button
        fol.profileButton.onClick.RemoveAllListeners();
        fol.profileButton.onClick.AddListener(delegate {
            app.globalChatComponent.OnOpenPlayerProfile(_player.id, _player.name, Globals.ProfileOrigin.FriendsPanel);
        });

        // Send coins button
        fol.sendCoinsButton.gameObject.SetActive(_friend.isConfirmed);
        fol.sendCoinsButton.onClick.RemoveAllListeners();
        fol.sendCoinsButton.onClick.AddListener(delegate
        {
            app.sendCoinsComponent.OpenSendCoinsWindow(_player.id);
        });

        SetFriendStatus(fol);

        friendOnListList.Add(fol);

        UpdateCounters();
    }

    private void SetFriendStatus(FriendOnList fol)
    {
        // FRIENDSHIP NOT ACCEPTED
        if(fol.friend.isConfirmed)
        {
            bool onTable = false;
            fol.inviteButton.gameObject.SetActive(true);
            if (app.mainController.currentRoom != null && isFromTable)
            {
                if (app.privateRoomLobbyComponent.playersInRoomList.Count(x => x.playerId == fol.myPlayer.id) > 0)
                {
                    fol.stateImage.color = playerStateColors[0];
                    fol.playerState = 0;
                    onTable = true;

                    fol.inviteButton.gameObject.SetActive(false);
                }
            }
            else
            {
                fol.inviteButton.gameObject.SetActive(false);
            }

            if (!onTable)
            {
                switch (fol.myPlayer.status)
                {
                    case "Online":
                        fol.stateImage.color = playerStateColors[1];
                        fol.playerState = 1;

                        // Invite button
                        // If table has password, only host can invite
                        if (isFromTable && !string.IsNullOrEmpty(app.mainController.currentRoom.password))
                        {
                            fol.inviteButton.gameObject.SetActive(app.mainController.myPlayer.id == app.mainController.currentRoom.playerHostId);
                        }
                        fol.inviteButton.onClick.RemoveAllListeners();
                        fol.inviteButton.onClick.AddListener(delegate
                        {
                            StartCoroutine(SendTableInvitation(fol));
                        });
                        break;
                    case "Offline":
                        fol.stateImage.color = playerStateColors[2];
                        fol.playerState = 2;
                        fol.inviteButton.gameObject.SetActive(false);
                        break;
                    default:
                        break;
                }
            }
        }
        else
        {
            if(isFromTable)
            {
                fol.myObject.SetActive(false);
            }
            else
            {
                fol.myObject.SetActive(true);
                fol.stateImage.color = playerStateColors[3];
                fol.playerState = 3;
                fol.inviteButton.gameObject.SetActive(false);
            }
        }

        // If on profile, update
        if (app.mainController.profileWindow.activeSelf && app.profileController.isOtherProfile)
        {
            app.globalChatComponent.StartCoroutine(app.globalChatComponent.SetFriendRequestButtons(fol.myPlayer.id));
        }
    }

    IEnumerator UpdateFriendOnList(Globals.Friend _friend, Globals.Player _player)
    {
        yield return null;

        if(friendOnListList.Count(x => x.myPlayer.id == _player.id) > 0)
        {
            var update = friendOnListList.First(x => x.myPlayer.id == _player.id);
            update.myPlayer = _player;
            update.friend = _friend;
            update.sendCoinsButton.gameObject.SetActive(_friend.isConfirmed);
            SetFriendStatus(update);
            UpdateCounters();
        }
        else
        {
            if(((Globals.Friend)app.mainController.myPlayer.friendList.First(x => ((Globals.Friend)x.Value).playerId == _player.id).Value).isConfirmed)
                StartCoroutine(CreateFriendOnList(_friend, _player));
        }
    }

    void UpdateCounters()
    {
        playerCountTexts[0].text = "EN MESA: " + friendOnListList.Count(x => x.playerState == 0);
        playerCountTexts[0].gameObject.SetActive(isFromTable);

        playerCountTexts[1].text = "EN LINEA: " + friendOnListList.Count(x => x.playerState == 1);
        playerCountTexts[2].text = "DESCONECTADOS: " + friendOnListList.Count(x => x.playerState == 2);

        playerCountTexts[3].text = "PENDIENTES: " + friendOnListList.Count(x => x.playerState == 3);
        playerCountTexts[3].gameObject.SetActive(!isFromTable);

        if(!searchiendPanel.activeSelf)
            app.mainController.loadingPanel.SetActive(false);

        // Order friends list by name, then status
        List<FriendOnList> _ordered = friendOnListList.OrderBy(x => x.myPlayer.name).ToList();
        _ordered = _ordered.OrderBy(x => x.playerState).ToList();
        for (int i = 0; i < _ordered.Count; i++)
        {
            friendOnListList.First(x => x.myPlayer.id == _ordered[i].myPlayer.id).myObject.transform.SetSiblingIndex(i);
        }
    }

    IEnumerator SendTableInvitation(FriendOnList fol)
    {
        //Send invitation
        var _invitationsRef = FirebaseDatabase.DefaultInstance.RootReference.Child("FriendRequests");
        var key = _invitationsRef.Push().Key;

        Dictionary<string, object> newFriend = new Dictionary<string, object>();
        newFriend["/type"] = 3;
        newFriend["/originId"] = app.mainController.myPlayer.id;
        newFriend["/destinyId"] = fol.myPlayer.id;
        newFriend["/content"] = app.mainController.currentRoom.id + "," + app.mainController.currentRoom.name + "," + app.mainController.myPlayer.name;
        newFriend["/timestamp"] = ServerValue.Timestamp;

        _invitationsRef.Child(key).UpdateChildrenAsync(newFriend).ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Game invitation sent!");
            }
        });

        // Temporaly disable player invite button
        fol.inviteButton.interactable = false;
        yield return new WaitForSeconds(10);
        fol.inviteButton.interactable = true;
    }

    public IEnumerator SetPlayerPhoto(string _playerId, Texture2D _tex)
    {
        yield return null;
        //print("Setting friend photo!");

        app.globalChatComponent.playerPhotosDic[_playerId] =
            Sprite.Create(_tex, new Rect(0.0f, 0.0f, _tex.width, _tex.height), new Vector2(0.5f, 0.5f), 100.0f);

        friendOnListList.First(x => x.myPlayer.id == _playerId).playerPhotoImage.sprite = app.globalChatComponent.playerPhotosDic[_playerId];
    }

    #region SEARCH_FRIEND
    public void OpenSearchFriendPanel()
    {
        searchiendPanel.SetActive(true);
        searchFriendInput.text = string.Empty;
        searchFriendInput.Select();
    }

    public void OnSearchFriend()
    {
        playerSearchType = Globals.PlayerSearchType.FriendSearch;
        SearchPlayerName();
    }

    public void OnSearchNameFOrChange()
    {
        playerSearchType = Globals.PlayerSearchType.NameChange;
        SearchPlayerName();
    }

    public void SearchPlayerName()
    {
        string queryText = string.Empty;

        switch (playerSearchType)
        {
            case Globals.PlayerSearchType.FriendSearch:
                queryText = searchFriendInput.text.ToLower();
                app.mainController.ShowLoading("Buscando jugador");
                break;
            case Globals.PlayerSearchType.NameChange:
                queryText = app.profileController.nicknameInput.text.ToLower();
                app.mainController.ShowLoading("Revisando diponibilidad de nombre");
                break;
        }

        if(string.IsNullOrEmpty(queryText))
        {
            app.mainController.loadingPanel.SetActive(false);
            return;
        }

        if (app.mainController.myPlayer.name.ToLower() == queryText)
        {
            OnCloseFriendWindow();
            searchiendPanel.SetActive(false);
            app.mainController.loadingPanel.SetActive(false);
            app.profileController.OpenProfileWindow();
            return;
        }

        Dictionary<string, object> childUpdates = new Dictionary<string, object>();

        string key = System.Guid.NewGuid().ToString();
        childUpdates["/searchName"] = queryText;

        if(searchPlayer_ref != null)
        {
            searchPlayer_ref.ValueChanged -= Handle_NameSearcher_ValueChanged;
            searchPlayer_ref.RemoveValueAsync();
            searchPlayer_ref = null;
        }
        searchPlayer_ref = FirebaseDatabase.DefaultInstance.GetReference("SearchedPlayers").Child(key);
        searchPlayer_ref.ValueChanged += Handle_NameSearcher_ValueChanged;

        searchPlayer_ref.UpdateChildrenAsync(childUpdates).ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Started player name search");
                }
            });
    }

    void Handle_NameSearcher_ValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        string json = args.Snapshot.GetRawJsonValue();
        print(json);

        if(json.Contains("foundPlayer"))
        {
            Globals.SearchName sn = JsonUtility.FromJson<Globals.SearchName>(json);
            if(sn.foundPlayer)
            {
                app.mainController.ShowLoading("Obteniendo información de jugador");
                switch (playerSearchType)
                {
                    case Globals.PlayerSearchType.FriendSearch:
                        FirebaseDatabase.DefaultInstance.GetReference("Players").Child(sn.playerId)
                            .GetValueAsync().ContinueWith(task =>
                            {
                                if (task.IsFaulted)
                                {
                                    print("-----FAULTED!!!-----");
                                    // Handle the error...
                                }
                                else if (task.IsCompleted)
                                {
                                    DataSnapshot snapshot = task.Result;

                                    Globals.Player foundPlayer = JsonUtility.FromJson<Globals.Player>(snapshot.GetRawJsonValue());
                                    UnityMainThreadDispatcher.Instance().Enqueue(OpenFoundPlayerProfile(foundPlayer));

                                }
                            });
                        break;
                    case Globals.PlayerSearchType.NameChange:
                        UnityMainThreadDispatcher.Instance().Enqueue(
                            app.promptComponent.ShowPrompt_Routine("Oops", "Ese nombre ya se está usando",
                            Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Pink));

                        UnityMainThreadDispatcher.Instance().Enqueue(FinishSearchPlayerHandler());
                        break;
                }
            }
            else
            {
                switch (playerSearchType)
                {
                    case Globals.PlayerSearchType.FriendSearch:
                        app.promptComponent.ShowPrompt("Jugador no encontrado", "No se encontró un jugador con ese nombre",
                            Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow);

                        app.mainController.loadingPanel.SetActive(false);
                        break;
                    case Globals.PlayerSearchType.NameChange:
                        app.profileController.StartCoroutine(app.profileController.ChangeName());
                        break;
                }

                UnityMainThreadDispatcher.Instance().Enqueue(FinishSearchPlayerHandler());
            }
        }
        else
        {
            switch (playerSearchType)
            {
                case Globals.PlayerSearchType.FriendSearch:
                    app.mainController.ShowLoading("Buscando jugador...");
                    break;
                case Globals.PlayerSearchType.NameChange:
                    app.mainController.ShowLoading("Revisando si nombre ya está en uso...");
                    break;
            }
        }
    }

    IEnumerator OpenFoundPlayerProfile(Globals.Player _foundPlayer)
    {
        if (_foundPlayer != null)
        {
            //print("GOT YOU!: " + _foundPlayer.name);
            app.globalChatComponent.OnOpenPlayerProfile(_foundPlayer.id, _foundPlayer.name, Globals.ProfileOrigin.PlayerSearch);
            searchiendPanel.SetActive(false);
            OnCloseFriendWindow();
        }
        else
        {
            app.promptComponent.StartCoroutine(app.promptComponent.ShowPrompt_Routine("DEBUG ERROR", "Dafuq error",
                    Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Red));
        }

        yield return null;

        UnityMainThreadDispatcher.Instance().Enqueue(FinishSearchPlayerHandler());
    }

    IEnumerator FinishSearchPlayerHandler()
    {
        yield return null;

        searchPlayer_ref.ValueChanged -= Handle_NameSearcher_ValueChanged;
        searchPlayer_ref.RemoveValueAsync();
    }
    #endregion
}
