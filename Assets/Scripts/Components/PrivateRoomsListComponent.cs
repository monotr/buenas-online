﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class PrivateRoomsListComponent : LoteriaOnlineElement
{
    [Header("Rooms")]
    public GameObject roomOnListPrefab;
    public RectTransform roomOnListParent;

    [Header("Search table")]
    public TMP_InputField tableSearchInput;
    Button tableSearchClearButton;

    DatabaseReference _ref;

    List<Globals.Room> privateRoomList;

    public class CantadaSlider
    {
        public GameObject myObj;

        public Slider cantadaSpeedSlider;
        public Image cantadaSpeedTimerPanel;
        public TMP_Text cantadaSpeedTimerText;

        public CantadaSlider(GameObject obj)
        {
            myObj = obj;

            cantadaSpeedSlider = obj.GetComponent<Slider>();
            cantadaSpeedTimerPanel = obj.transform.GetChild(2).GetChild(0).gameObject.GetComponent<Image>();
            cantadaSpeedTimerText = cantadaSpeedTimerPanel.GetComponentInChildren<TMP_Text>();
        }
    }
    public class VictoryTypeButtons
    {
        public GameObject myObj;

        public GameObject lineTable;
        public Coroutine lineTableAnimCoroutine;
        public List<Image> victoryTypesImageList;

        public VictoryTypeButtons(GameObject obj)
        {
            myObj = obj;

            lineTable = obj.transform.GetChild(0).GetChild(0).gameObject;
            victoryTypesImageList = new List<Image>();
            foreach (Transform c in obj.transform)
                victoryTypesImageList.Add(c.gameObject.GetComponent<Image>());
        }
    }
    public class PrivateRoomOnList
    {
        public GameObject myObj;
        public Image statusImage;
        public Image passwordLockImage;
        public TMP_Text nameText;
        public TMP_Text inPText;
        public TMP_Text maxPText;
        public Button joinButton;

        public CantadaSlider cantadaSlider;
        public TMP_Text betPerTableText;
        public VictoryTypeButtons victoryTypeButtons;

        public PrivateRoomOnList(GameObject obj)
        {
            myObj = obj;

            statusImage = obj.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Image>();
            passwordLockImage = obj.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Image>();
            nameText = obj.transform.GetChild(0).GetChild(2).gameObject.GetComponent<TMP_Text>();
            inPText = obj.transform.GetChild(0).GetChild(3).gameObject.GetComponent<TMP_Text>();
            maxPText = obj.transform.GetChild(0).GetChild(4).gameObject.GetComponent<TMP_Text>();
            joinButton = obj.transform.GetChild(0).GetChild(5).gameObject.GetComponent<Button>();

            cantadaSlider = new CantadaSlider(obj.transform.GetChild(1).GetChild(0).gameObject);
            betPerTableText = obj.transform.GetChild(1).GetChild(1).GetChild(1).GetComponent<TMP_Text>();
            victoryTypeButtons = new VictoryTypeButtons(obj.transform.GetChild(1).GetChild(2).gameObject);
        }
    }
    Dictionary<string, PrivateRoomOnList> privateRoomsOnListDic;
    public Sprite[] lockSprites;
    public Color[] statusColors;

    private void Start()
    {
        tableSearchClearButton = tableSearchInput.gameObject.GetComponentInChildren<Button>();

        //DEBUG
        //StartCoroutine(Debug_CreateRooms());
    }

    public void StartFillPrivateRoomsList()
    {
        privateRoomList = new List<Globals.Room>();
        privateRoomsOnListDic = new Dictionary<string, PrivateRoomOnList>();

        // Search input
        ClearSearchInput();

        foreach (Transform child in roomOnListParent)
            if (!child.name.Contains("Header"))
                Destroy(child.gameObject);

        _ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms");
        /// TODO limitar a n cantidad, ir cargando de poco en poc... esto cuando haya muchisimas salas (lol)
        _ref.ChildAdded += Handle_Room_ChildAdded;
        _ref.ChildChanged += Handle_Room_ChildChanged;
        _ref.ChildRemoved += Handle_Room_ChildRemoved;
    }

    public void OnClosePrivateRoomsList()
    {
        EndPrivateRoomsRefresh();

        app.mainController.privateRoomsWindow.SetActive(false);
        app.mainController.gameState = MainController.GameState.LoggedIn;
        app.mainController.StartCoroutine(app.mainController.ChangeWindow());
    }

    void EndPrivateRoomsRefresh()
    {
        try
        {
            var keys = privateRoomsOnListDic.Keys;
            foreach (var k in keys)
            {
                if (privateRoomsOnListDic[k].victoryTypeButtons.lineTableAnimCoroutine != null)
                {
                    app.newPrivateRoomComponent.StopCoroutine(privateRoomsOnListDic[k].victoryTypeButtons.lineTableAnimCoroutine);
                    privateRoomsOnListDic[k].victoryTypeButtons.lineTableAnimCoroutine = null;
                }
            }
        }
        catch(System.Exception e)
        {
            print(e.Message);
        }

        try
        {
            _ref.ChildAdded -= Handle_Room_ChildAdded;
            _ref.ChildChanged -= Handle_Room_ChildChanged;
            _ref.ChildRemoved -= Handle_Room_ChildRemoved;
            _ref = null;
        }
        catch (System.Exception e)
        {
            print(e.Message);
        }
    }

    void CreateRoomOnList(Globals.Room room)
    {
        if (string.IsNullOrEmpty(room.id))
            return;

        privateRoomList.Add(room);

        GameObject newRoL = Instantiate(roomOnListPrefab, roomOnListParent);
        privateRoomsOnListDic.Add(room.id, new PrivateRoomOnList(newRoL));
        SetRoomOnListData(room);

        // resize grid
        roomOnListParent.sizeDelta = new Vector2(roomOnListParent.sizeDelta.x, roomOnListParent.childCount * 195+20);
    }

    void SetRoomOnListData(Globals.Room room)
    {
        PrivateRoomOnList prol = privateRoomsOnListDic[room.id];

        // Status, name, password
        prol.statusImage.color = statusColors[room.state.Contains("InGame") ? 1 : 0];
        prol.passwordLockImage.sprite = lockSprites[string.IsNullOrEmpty(room.password) ? 1 : 0];
        prol.passwordLockImage.color = new Color(1, 1, 1, string.IsNullOrEmpty(room.password) ? 0.5f : 1);
        prol.nameText.text = room.name;

        // players
        prol.inPText.text = room.playerCount + "";
        prol.maxPText.text = room.maxPlayers + "";

        // Cantada speed slider
        prol.cantadaSlider.cantadaSpeedSlider.value = app.newPrivateRoomComponent.cantadaSpeedsList.IndexOf((int)room.cantadaSpeed);
        prol.cantadaSlider.cantadaSpeedTimerText.text = app.newPrivateRoomComponent.cantadaSpeedsList[(int)prol.cantadaSlider.cantadaSpeedSlider.value] + " s";
        prol.cantadaSlider.cantadaSpeedTimerPanel.color = app.newPrivateRoomComponent.cantadaSpeedColors[(int)prol.cantadaSlider.cantadaSpeedSlider.value];
        
        // One time set, no update required
        if(prol.victoryTypeButtons.lineTableAnimCoroutine == null)
        {
            prol.victoryTypeButtons.lineTableAnimCoroutine =
                app.newPrivateRoomComponent.StartCoroutine(app.newPrivateRoomComponent.LineVictoryTableAnimation(prol.victoryTypeButtons.lineTable));

            prol.joinButton.onClick.RemoveAllListeners();
            prol.joinButton.onClick.AddListener(delegate {
                if (string.IsNullOrEmpty(room.password))
                    EnterRoom(room.id);
                else
                    app.promptComponent.ShowPrompt("Ingrese contraseña para entrar", string.Empty, Globals.PromptType.YesNoMessage, "Password," + room.id, Globals.PromptColors.Pink);
            });
        }
        // Check max players
        prol.joinButton.interactable = room.playerCount < room.maxPlayers;

        // Victory types
        List<string> parts = room.victoryTypes.Split(',').ToList();
        for (int i = 0; i < prol.victoryTypeButtons.victoryTypesImageList.Count; i++)
            prol.victoryTypeButtons.victoryTypesImageList[i].color = parts.Contains(i.ToString()) ? app.newPrivateRoomComponent.victoryTypeSelectionColor : Color.white;

        // Bet
        prol.betPerTableText.text = "$" + room.betPerTable;
    }

    public void EnterRoom(string args)
    {
        string[] parts = args.Split(',');
        bool canEnter = parts.Length == 1;

        if (parts.Length == 2)
        {
            if(privateRoomList.Count(x => x.id == parts[0] && x.password == parts[1]) > 0)
                canEnter = true;
        }

        if(canEnter)
        {
            EndPrivateRoomsRefresh();

            app.mainController.currentRoom = privateRoomList.First(x => x.id == parts[0]);
            app.mainController.StartCoroutine(app.mainController.OpenPrivateRoomLobbyWindow());
        }
        else
        {
            app.promptComponent.ShowPrompt("Error al entrar a mesa", "La contraseña es incorrecta", Globals.PromptType.ConfirmMessage, string.Empty, Globals.PromptColors.Yellow);
        }
    }

    void UpdateRoomOnList(Globals.Room room)
    {
        privateRoomList[privateRoomList.IndexOf(privateRoomList.First(x => x.id == room.id))] = room;

        SetRoomOnListData(room);
    }

    void DeleteRoomOnList(Globals.Room room)
    {
        privateRoomList.RemoveAt(privateRoomList.IndexOf(privateRoomList.First(x => x.id == room.id)));

        app.newPrivateRoomComponent.StopCoroutine(privateRoomsOnListDic[room.id].victoryTypeButtons.lineTableAnimCoroutine);
        privateRoomsOnListDic[room.id].victoryTypeButtons.lineTableAnimCoroutine = null;

        Destroy(privateRoomsOnListDic[room.id].myObj);
        privateRoomsOnListDic.Remove(room.id);

        // resize grid
        roomOnListParent.sizeDelta = new Vector2(roomOnListParent.sizeDelta.x, roomOnListParent.childCount * 195 + 20);
    }

    void Handle_Room_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        print(args.Snapshot.GetRawJsonValue());
        Globals.Room r = JsonUtility.FromJson<Globals.Room>(args.Snapshot.GetRawJsonValue());
        CreateRoomOnList(r);
    }

    void Handle_Room_ChildChanged(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        Globals.Room r = JsonUtility.FromJson<Globals.Room>(args.Snapshot.GetRawJsonValue());
        UpdateRoomOnList(r);
    }

    void Handle_Room_ChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        Globals.Room r = JsonUtility.FromJson<Globals.Room>(args.Snapshot.GetRawJsonValue());
        DeleteRoomOnList(r);
    }

    private void OnApplicationQuit()
    {
        EndPrivateRoomsRefresh();
    }

    /// <summary>
    /// 
    /// </summary>
    #region AUTO-FIND-MATCH
    public void OnPlayNow()
    {
        _ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms");
        app.mainController.ShowLoading("Buscando partidas");
        StartCoroutine(SearchRooms(3, 0));
    }

    IEnumerator SearchRooms(int tries, float delay)
    {
        yield return new WaitForSeconds(delay);

        tries--;

        _ref.GetValueAsync().ContinueWith(t => {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Got private rooms list");
                DataSnapshot snapshot = t.Result;
                print(snapshot.GetRawJsonValue());

                if (string.IsNullOrEmpty(snapshot.GetRawJsonValue())) // No rooms
                {
                    if (tries > 0) // Try again
                    {
                        UnityMainThreadDispatcher.Instance().Enqueue(SearchRooms(tries, 0.75f));
                    }
                    else // No more tries, create a room with default settings
                    {
                        UnityMainThreadDispatcher.Instance().Enqueue(CreateDefaultRoom());
                    }
                }
                else // Found rooms, choose the best
                {
                    print("\t\t\t===== Available rooms: " + snapshot.ChildrenCount);
                    
                    UnityMainThreadDispatcher.Instance().Enqueue(ChooseBestRoom(snapshot.Children.ToList()));
                }
            }
        });   
    }

    IEnumerator ChooseBestRoom(List<DataSnapshot> rooms)
    {
        yield return null;

        privateRoomList = new List<Globals.Room>();
        Globals.Room newRoom;
        foreach (var room in rooms)
        {
            newRoom = JsonUtility.FromJson<Globals.Room>(room.GetRawJsonValue());
            privateRoomList.Add(newRoom);
        }
        // Rooms without password, not full and order them by player count and bet
        privateRoomList = privateRoomList.Where(x => string.IsNullOrEmpty(x.password)).ToList();
        privateRoomList = privateRoomList.Where(x => x.playerCount < x.maxPlayers).ToList();
        privateRoomList = privateRoomList.OrderBy(x => x.playerCount).ToList();
        privateRoomList = privateRoomList.OrderBy(x => x.betPerTable).ToList();
        
        if(privateRoomList.Count > 0)
        {
            // Takes the one in the middle
            int selectedRoomIndex = privateRoomList.Count / 2;
            // Enter room
            app.gameController.tablePageButtonList = new List<GameController.TablePageButton>();
            app.privateRoomLobbyComponent.selectedTablesObjList = new List<GameObject>();
            yield return new WaitForSeconds(0.1f);
            app.mainController.currentRoom = privateRoomList[selectedRoomIndex];
            app.mainController.StartCoroutine(app.mainController.OpenPrivateRoomLobbyWindow());
        }
        else // no avairooms found
        {
            StartCoroutine(CreateDefaultRoom());
        }
    }

    public IEnumerator CreateDefaultRoom()
    {
        app.promptComponent.StartCoroutine(app.promptComponent.ShowPrompt_Routine(
            "Creada nueva mesa",
            "No encontramos una mesa disponible, pero te creamos una con 4 bots para jugar YA!",
            Globals.PromptType.ConfirmMessage,
            "OfferCreateRoom",
            Globals.PromptColors.Yellow
        ));

        yield return null;

        app.mainController.currentRoom = new Globals.Room()
        {
            playerHostId = app.mainController.myPlayer.id,
            maxPlayers = 16,
            cantadaSpeed = 6,
            state = "InLobby",
            calleddCardsDic = new Dictionary<string, object>(),
            playersIdDict = new Dictionary<string, object>(),
            accumulatedCoins = 0,
            betPerTable = 20,
            name = app.mainController.myPlayer.name + "_" + Guid.NewGuid().ToString().Substring(0, 4),
            victoryTypes = "0"
        };
#if UNITY_EDITOR
        app.mainController.currentRoom.cantadaSpeed = 2;
#endif

        app.newPrivateRoomComponent.CreateNewRoom(true);
    }
    #endregion

    public void OnSearchTableInputChange(string _input)
    {
        tableSearchClearButton.interactable = _input.Length > 0;
        var matchTables = privateRoomList.Where(x => x.name.ToLower().Contains(_input.ToLower())).ToList();
        foreach (var table in privateRoomsOnListDic)
        {
            table.Value.myObj.SetActive(matchTables.Count(x => x.id == table.Key) > 0);
        }
    }
    
    public void ClearSearchInput()
    {
        tableSearchClearButton.interactable = false;
        tableSearchInput.text = string.Empty;
        tableSearchInput.Select();
    }
}
