﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SendCoinsComponent : LoteriaOnlineElement
{
    public GameObject sendCoinsWindow;
    public Image playerPhotoImage;
    public TMP_InputField coinsInput;
    public Button confirmButton;

    internal long sendingCoins;
    string destinyPlayerId;

    //UI Raycast
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;


    private void Start()
    {
        sendCoinsWindow.SetActive(false);
        m_Raycaster = FindObjectOfType<Canvas>().GetComponent<GraphicRaycaster>();
        m_EventSystem = FindObjectOfType<Canvas>().GetComponent<EventSystem>();
    }
    
    private void Update()
    {
        if (sendCoinsWindow.activeSelf)
        {
            m_PointerEventData = new PointerEventData(m_EventSystem);

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                app.botWindowComponent.TouchLogic(m_PointerEventData, touch.position, touch.phase == TouchPhase.Began, touch.phase == TouchPhase.Stationary, touch.phase == TouchPhase.Ended,
                    BotWindowComponent.AutoIncreaseType.SendCoins);
            }
#else
            app.botWindowComponent.TouchLogic(m_PointerEventData, Input.mousePosition, Input.GetMouseButtonDown(0), Input.GetMouseButton(0), Input.GetMouseButtonUp(0),
                BotWindowComponent.AutoIncreaseType.SendCoins);
#endif
        }
    }

    public void OpenSendCoinsWindow(string _destinyPlayerId)
    {
        sendCoinsWindow.SetActive(true);
        sendingCoins = 0;
        coinsInput.text = "0";
        coinsInput.Select();

        destinyPlayerId = _destinyPlayerId;

        // Set photo
        playerPhotoImage.sprite = app.globalChatComponent.playerPhotosDic[_destinyPlayerId];
    }

    public void OnSendingCoinsInputChange(string _input)
    {
        long _val = 0;
        if(long.TryParse(_input, out _val))
        {
            sendingCoins = _val;
            if(sendingCoins > app.mainController.myPlayer.coins)
            {
                confirmButton.interactable = false;
                coinsInput.textComponent.color = app.newPrivateRoomComponent.cantadaSpeedColors[4];
            }
            else if (sendingCoins <= 0)
            {
                confirmButton.interactable = false;
                coinsInput.textComponent.color = app.newPrivateRoomComponent.cantadaSpeedColors[4];
            }
            else
            {
                confirmButton.interactable = true;
                coinsInput.textComponent.color = Color.black;
            }
        }
        else
        {
            sendingCoins = 0;
            confirmButton.interactable = false;
            coinsInput.textComponent.color = app.newPrivateRoomComponent.cantadaSpeedColors[4];
        }
    }

    public void OnConfirmSendCoins()
    {
        // Make transaction
        app.coinController.SetCoinTransaction(
            app.mainController.myPlayer.id,
            destinyPlayerId,
            sendingCoins
        );

        // Close send coins window
        OnCloseSendCoinsWindow();
    }

    public void OnCloseSendCoinsWindow()
    {
        sendCoinsWindow.SetActive(false);
    }
}
