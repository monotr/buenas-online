﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VivoxUnity;

public class VoiceChatLobbyComponent : LoteriaOnlineElement
{
    internal VivoxVoiceManager _vivoxVoiceManager;

    public string channelName;
    public Dictionary<string, IParticipant> participantDict;

    public void Init(string _channelName)
    {
        participantDict = new Dictionary<string, IParticipant>();

        channelName = _channelName;

        _vivoxVoiceManager = VivoxVoiceManager.Instance;
        _vivoxVoiceManager.OnUserLoggedInEvent += OnUserLoggedIn;
        _vivoxVoiceManager.OnUserLoggedOutEvent += OnUserLoggedOut;

        _vivoxVoiceManager.OnParticipantAddedEvent += OnParticipantAdded;
        _vivoxVoiceManager.OnParticipantRemovedEvent += OnParticipantRemoved;

        if (_vivoxVoiceManager.LoginState == LoginState.LoggedIn)
        {
            OnUserLoggedIn();
        }
    }

    private void OnDestroy()
    {
        _vivoxVoiceManager.OnUserLoggedInEvent -= OnUserLoggedIn;
        _vivoxVoiceManager.OnUserLoggedOutEvent -= OnUserLoggedOut;

        _vivoxVoiceManager.OnParticipantAddedEvent -= OnParticipantAdded;
        _vivoxVoiceManager.OnParticipantRemovedEvent -= OnParticipantRemoved;
    }

    private void JoinLobbyChannel()
    {
        _vivoxVoiceManager.JoinChannel(channelName, ChannelType.NonPositional, VivoxVoiceManager.ChatCapability.TextAndAudio);
    }

    public void LogoutOfVivoxChannels()
    {
        _vivoxVoiceManager.DisconnectAllChannels();
    }


    #region Vivox Callbacks

    private void OnUserLoggedIn()
    {
        var lobbychannel = _vivoxVoiceManager.ActiveChannels.FirstOrDefault(ac => ac.Channel.Name == channelName);
        if ((_vivoxVoiceManager && _vivoxVoiceManager.ActiveChannels.Count == 0) || lobbychannel == null)
        {
            JoinLobbyChannel();
        }
        else
        {
            lobbychannel.BeginSetAudioConnected(true, TransmitPolicy.Yes, ar =>
            {
                Debug.Log("Now transmitting into lobby channel");
            });

        }
    }

    private void OnUserLoggedOut()
    {
        _vivoxVoiceManager.DisconnectAllChannels();
    }

    void OnParticipantAdded(string userName, ChannelId channel, IParticipant participant)
    {
        Debug.Log("OnPartAdded: " + userName);
        participantDict.Add(userName, participant);
        //UpdateParticipantRoster(participant, channel, true);
    }

    void OnParticipantRemoved(string userName, ChannelId channel, IParticipant participant)
    {
        Debug.Log("OnPartRemoved: " + participant.Account.Name);
        participantDict.Remove(userName);
        //UpdateParticipantRoster(participant, channel, false);
    }

    #endregion
}
