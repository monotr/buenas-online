﻿using UnityEngine;
using UnityEngine.UI;

public class LoadingComponent : MonoBehaviour
{
    public Image spinnerImage;

    void Update()
    {
        spinnerImage.rectTransform.Rotate(Vector3.forward, Time.deltaTime * 360);
    }
}
