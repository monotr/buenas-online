﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameNotificationsComponent : LoteriaOnlineElement
{
    DatabaseReference _gameNotifications_ref;

    public GameObject gameNotificationsWindow;
    Coroutine refreshRoutine;

    public GameObject notificationsCounterIndicator;
    TMP_Text notificationsCounterText;

    public GameObject gameNotificationPrefab;
    public Transform gameNotificarionsGrid;

    public class GameNotificationOnList
    {
        public Image playerPhotoImage;
        public TMP_Text dateTimeText;
        public TMP_Text messageText;
        public Button deleteButton;
        public Button acceptButton;
        public GameObject myObj;
        public Globals.GameNotification myNotification;
        public string fbId;

        public GameNotificationOnList(GameObject obj, Globals.GameNotification _gameNotification, string _fbId)
        {
            fbId = _fbId;
            myNotification = _gameNotification;
            myObj = obj;

            playerPhotoImage = obj.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>();
            dateTimeText = obj.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>();
            messageText = obj.transform.GetChild(1).GetChild(1).GetComponent<TMP_Text>();
            deleteButton = obj.transform.GetChild(2).GetChild(0).GetComponent<Button>();
            acceptButton = obj.transform.GetChild(2).GetChild(1).GetComponent<Button>();
        }
    }
    List<GameNotificationOnList> gameNotificationOnListList;

    private void Start()
    {
        notificationsCounterText = notificationsCounterIndicator.GetComponentInChildren<TMP_Text>();
        notificationsCounterIndicator.SetActive(false);
    }

    public void Init()
    {
        gameNotificationOnListList = new List<GameNotificationOnList>();

        foreach (Transform child in gameNotificarionsGrid)
            Destroy(child.gameObject);

        _gameNotifications_ref = FirebaseDatabase.DefaultInstance.GetReference("Players").Child(app.mainController.myPlayer.id).Child("gameNotifications");
        _gameNotifications_ref.ChildAdded += Handle_GameNotification_ChildAdded;
        _gameNotifications_ref.ChildRemoved += Handle_GameNotification_ChildRemoved;
    }

    void Handle_GameNotification_ChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        //print(args.Snapshot.GetRawJsonValue());
        Globals.GameNotification newNotification = JsonUtility.FromJson<Globals.GameNotification>(args.Snapshot.GetRawJsonValue());

        GameObject _newGN = Instantiate(gameNotificationPrefab, gameNotificarionsGrid);
        GameNotificationOnList gnol = new GameNotificationOnList(_newGN, newNotification, args.Snapshot.Key);

        System.DateTime dt = System.DateTimeOffset.FromUnixTimeMilliseconds(newNotification.timestamp).DateTime;
        gnol.dateTimeText.text = dt.ToString("dd/MM/yyyy hh:mm tt");

        switch (newNotification.type)
        {
            case 0: // Friend request
                gnol.messageText.text = newNotification.content + " quiere ser tu amigo!";

                // Confirm friend
                gnol.acceptButton.onClick.AddListener(delegate {
                    gnol.acceptButton.interactable = false;
                    gnol.deleteButton.interactable = false;
                    app.profileController.OnSendFriendRequest(2, app.mainController.myPlayer.id, newNotification.originId, args.Snapshot.Key);
                });
                // Decline friend
                gnol.deleteButton.onClick.AddListener(delegate {
                    gnol.acceptButton.interactable = false;
                    gnol.deleteButton.interactable = false;
                    app.profileController.OnSendFriendRequest(1, app.mainController.myPlayer.id, newNotification.originId, args.Snapshot.Key);
                });

                // Get origin player photo
                if (app.globalChatComponent.playerPhotosDic.ContainsKey(newNotification.originId))
                    gnol.playerPhotoImage.sprite = app.globalChatComponent.playerPhotosDic[newNotification.originId];
                else
                {
                    app.globalChatComponent.playerPhotosDic.Add(newNotification.originId, null);
                    app.globalChatComponent.StartCoroutine(app.globalChatComponent.GetPlayerPhoto(newNotification.originId, Globals.PhotoDownloadType.GameNotification));
                }

                // Create bubble
                app.notificationsController.StartCoroutine(app.notificationsController.ReceiveNotificationRoutine(
                    Globals.NotificationType.FriendRequest,
                    null,
                    newNotification.content,
                    null
                ));
                break;
            case 1: // Friend request remove
            case 2: // Friend request confirm
                DestroyImmediate(_newGN);
                GetFriendInfo(newNotification.originId);
                OnDeleteGameNotification(args.Snapshot.Key);
                break;
            case 3: // Table invitation
                string[] parts = newNotification.content.Split(',');
                string roomid = parts[0];
                string roomName = parts[1];
                string originPlayerName = parts[2];
                gnol.messageText.text = "<b>" + originPlayerName + "</b> te ha invitado a jugar en la mesa <i>\"" + roomName + "\"</i>!";

                // Check if table expired
                StartCoroutine(CheckTableExists(args.Snapshot.Key, newNotification, true));

                gnol.acceptButton.onClick.AddListener(delegate {
                    StartCoroutine(JoinRoomById(roomid, 2));
                    CloseGameNotificationsWindow();
                    OnDeleteGameNotification(args.Snapshot.Key);
                });

                // Get origin player photo
                if (app.globalChatComponent.playerPhotosDic.ContainsKey(newNotification.originId))
                    gnol.playerPhotoImage.sprite = app.globalChatComponent.playerPhotosDic[newNotification.originId];
                else
                {
                    app.globalChatComponent.playerPhotosDic.Add(newNotification.originId, null);
                    app.globalChatComponent.StartCoroutine(app.globalChatComponent.GetPlayerPhoto(newNotification.originId, Globals.PhotoDownloadType.GameNotification));
                }

                gnol.deleteButton.onClick.AddListener(delegate {
                    gnol.deleteButton.interactable = false;
                    OnDeleteGameNotification(args.Snapshot.Key);
                });
                break;
            default:
                break;
        }

        if (_newGN != null)
        {
            gameNotificationOnListList.Add(gnol);

            // counter Indicator
            notificationsCounterIndicator.SetActive(true);
            notificationsCounterText.text = gameNotificationOnListList.Count.ToString();
        }
    }

    IEnumerator DestroyExpiredNotification(string _notificationId)
    {
        OnDeleteGameNotification(_notificationId);
        yield return null;
    }

    void GetFriendInfo(string _playerId)
    {
        FirebaseDatabase.DefaultInstance
            .GetReference("Players").Child(_playerId)
            .GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    print("-----FAULTED!!!-----");
                    // Handle the error...
                }
                else if (task.IsCompleted)
                {
                    //Debug.Log("Completed! - Got friend info");

                    DataSnapshot snapshot = task.Result;
                    //print(snapshot.GetRawJsonValue());

                    Globals.Player _player = JsonUtility.FromJson<Globals.Player>(snapshot.GetRawJsonValue());

                    UnityMainThreadDispatcher.Instance().Enqueue(app.notificationsController.ReceiveNotificationRoutine(
                        Globals.NotificationType.FriendRequestAccepted, null, _player.name, null));
                }
            });
    }

    IEnumerator JoinRoomById(string _roomId, int _tries)
    {
        print("=== Joining room id: " + _roomId + " ===");
        var _ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms").Child(_roomId);
        app.mainController.ShowLoading("Entrando a mesa");

        yield return new WaitForSeconds(0.1f);

        if(_tries > 0)
        {
            _tries--;

            _ref.GetValueAsync().ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                    UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("Error al unirse a mesa", "No se ha podido ingresar a la mesa (0x00)",
                            Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow));
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                    UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("Error al unirse a mesa", "No se ha podido ingresar a la mesa (0x01)",
                            Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow));
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Got private room");
                    DataSnapshot snapshot = t.Result;
                    string json = snapshot.GetRawJsonValue();
                    print(json);

                    if (string.IsNullOrEmpty(json))
                    {
                        UnityMainThreadDispatcher.Instance().Enqueue(JoinRoomById(_roomId, _tries));
                    }
                    else
                    {
                        Globals.Room _room = JsonUtility.FromJson<Globals.Room>(json);
                        UnityMainThreadDispatcher.Instance().Enqueue(CompleteJoinRoom(_room));
                    }
                }
            });
        }
        else
        {
            UnityMainThreadDispatcher.Instance().Enqueue(app.promptComponent.ShowPrompt_Routine("Error al unirse a mesa", "No se ha podido ingresar a la mesa (0x02)",
                            Globals.PromptType.ConfirmMessage, null, Globals.PromptColors.Yellow));
        }

        yield return null;
    }

    IEnumerator CompleteJoinRoom(Globals.Room _room)
    {
        yield return null;

        app.mainController.currentRoom = _room;
        app.mainController.StartCoroutine(app.mainController.OpenPrivateRoomLobbyWindow());
    }

    public IEnumerator SetPlayerPhoto(string _playerId, Texture2D _tex)
    {
        yield return null;
        //print("Setting chat player photos!");

        app.globalChatComponent.playerPhotosDic[_playerId] = Sprite.Create(_tex, new Rect(0.0f, 0.0f, _tex.width, _tex.height), new Vector2(0.5f, 0.5f), 100.0f);

        foreach (var gameNotification in gameNotificationOnListList)
        {
            if (gameNotification.myNotification.originId == _playerId)
            {
                gameNotification.playerPhotoImage.sprite = app.globalChatComponent.playerPhotosDic[_playerId];
            }
        }
    }

    void Handle_GameNotification_ChildRemoved(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        if(gameNotificationOnListList.Count(x => x.fbId == args.Snapshot.Key) > 0)
        {
            GameNotificationOnList gnol = gameNotificationOnListList.First(x => x.fbId == args.Snapshot.Key);
            gameNotificationOnListList.Remove(gnol);
            Destroy(gnol.myObj);
        }

        // counter Indicator
        notificationsCounterIndicator.SetActive(gameNotificationOnListList.Count > 0);
        notificationsCounterText.text = gameNotificationOnListList.Count.ToString();
    }

    IEnumerator CheckTableExists(string gmKey, Globals.GameNotification gameNotification, bool isNew)
    {
        yield return null;

        string[] parts = gameNotification.content.Split(',');
        string roomid = parts[0];
        string roomName = parts[1];
        string originPlayerName = parts[2];

        // Check if table expired
        var _ref = FirebaseDatabase.DefaultInstance.GetReference("PrivateRooms").Child(roomid);
        _ref.GetValueAsync().ContinueWith(t =>
        {
            if (t.IsFaulted)
            {
                Debug.Log("Faulted.." + t.Exception.Message);
            }

            if (t.IsCanceled)
            {
                Debug.Log("Cancelled..");
            }

            if (t.IsCompleted)
            {
                Debug.Log("Completed! - Got private room");
                DataSnapshot snapshot = t.Result;
                string json = snapshot.GetRawJsonValue();
                //print(json);

                if (string.IsNullOrEmpty(json))
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(DestroyExpiredNotification(gmKey));
                }
                else if(isNew)
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(app.notificationsController.ReceiveNotificationRoutine(
                        Globals.NotificationType.TableInvitation, null, originPlayerName, null));
                }
            }
        });
    }

    public void OpenGameNotificationsWindow()
    {
        gameNotificationsWindow.SetActive(true);

        if(refreshRoutine != null)  StopCoroutine(refreshRoutine);
        refreshRoutine = StartCoroutine(RefreshNotifications());
    }

    IEnumerator RefreshNotifications()
    {
        while (true)
        {
            _gameNotifications_ref.GetValueAsync().ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    Debug.Log("Faulted.." + t.Exception.Message);
                }

                if (t.IsCanceled)
                {
                    Debug.Log("Cancelled..");
                }

                if (t.IsCompleted)
                {
                    Debug.Log("Completed! - Got game notifications");
                    DataSnapshot snapshot = t.Result;
                    string json = snapshot.GetRawJsonValue();
                    //print(json);

                    Globals.GameNotification gameNotification;
                    foreach (var c in snapshot.Children)
                    {
                        gameNotification = JsonUtility.FromJson<Globals.GameNotification>(c.GetRawJsonValue());
                        UnityMainThreadDispatcher.Instance().Enqueue(CheckTableExists(c.Key, gameNotification, false));
                    }

                }
            });

            yield return new WaitForSeconds(1);
        }
    }

    public void CloseGameNotificationsWindow()
    {
        gameNotificationsWindow.SetActive(false);

        if (refreshRoutine != null) StopCoroutine(refreshRoutine);
    }

    void OnDeleteGameNotification(string notificationId)
    {
        _gameNotifications_ref.Child(notificationId).RemoveValueAsync();
    }
}
