﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DraggableCardComponent : LoteriaOnlineElement, IPointerDownHandler, IPointerUpHandler
{
    internal bool isTablePanel;

    internal int cardIndex;

    internal Image draggableImage;
    internal GameObject disabledImage;
    bool isMoving = false;

    int hoverTableCardPanelIndex = -1;
    int hoveringCardIndex;

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    private void Awake()
    {
        m_Raycaster = FindObjectOfType<Canvas>().GetComponent<GraphicRaycaster>();
        m_EventSystem = FindObjectOfType<Canvas>().GetComponent<EventSystem>();

        draggableImage = transform.GetChild(0).GetComponent<Image>();
        disabledImage = transform.GetChild(1).gameObject;
    }

    private void Update()
    {
        if (disabledImage.activeSelf)
            return;

        if (isMoving)
        {
            m_PointerEventData = new PointerEventData(m_EventSystem);
            m_PointerEventData.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>();
            m_Raycaster.Raycast(m_PointerEventData, results);
            
            bool gotOne = false;
            foreach (RaycastResult result in results)
            {
                if(result.gameObject.tag == "TableCardPanel")
                {
                    gotOne = true;
                    var dcc = result.gameObject.GetComponent<DraggableCardComponent>();
                    int _hoverIndex = app.tableBuilderComponent.tablePanelsDraggables.IndexOf(dcc);
                    if(_hoverIndex != hoverTableCardPanelIndex)
                    {
                        //print(result.gameObject.name);
                        ResetLastSelection();
                        //print("Eres tu " + hoverTableCardPanelIndex + " !!!!");
                        hoverTableCardPanelIndex = _hoverIndex;

                        // Preview change
                        draggableImage.rectTransform.position = app.tableBuilderComponent.tablePanelsDraggables[hoverTableCardPanelIndex].draggableImage.rectTransform.position;
                        draggableImage.rectTransform.rotation = Quaternion.Euler(Vector3.forward * -5);
                        draggableImage.rectTransform.localScale = Vector3.one * 1.35f;
                    }
                }
            }

            if (!gotOne)
            {
                if(hoverTableCardPanelIndex != -1)
                {
                    ResetLastSelection();
                    draggableImage.rectTransform.rotation = Quaternion.Euler(Vector3.forward * -12f);
                }

                draggableImage.rectTransform.position = Input.mousePosition;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (disabledImage.activeSelf)
            return;

        if(app == null)
            app = FindObjectOfType<LoteriaOnlineApplication>();

        //print("Click down");
        app.tableBuilderComponent.cardsScrollRect.enabled = false;
        // Disable cardPanel raycast
        draggableImage.transform.parent.gameObject.GetComponent<Image>().raycastTarget = false;

        draggableImage.transform.SetParent(app.tableBuilderComponent.draggableCardDragParent.transform);
        draggableImage.rectTransform.rotation = Quaternion.Euler(Vector3.forward * -12f);
        isMoving = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (disabledImage.activeSelf)
            return;

        if (app == null)
            app = FindObjectOfType<LoteriaOnlineApplication>();

        //print("Click up");
        isMoving = false;
        app.tableBuilderComponent.cardsScrollRect.enabled = true;
        draggableImage.transform.SetParent(transform);
        draggableImage.transform.SetAsFirstSibling();
        draggableImage.rectTransform.localPosition = Vector3.zero;
        draggableImage.rectTransform.rotation = Quaternion.identity;

        // Enable cardPanel raycast
        draggableImage.transform.parent.gameObject.GetComponent<Image>().raycastTarget = true;

        if (hoverTableCardPanelIndex != -1)
        {
            if(isTablePanel)
            {
                int _o = app.tableBuilderComponent.tablePanelsDraggables[hoverTableCardPanelIndex].cardIndex + 0;
                app.tableBuilderComponent.ChangeCardOnTable(hoverTableCardPanelIndex, cardIndex, false);
                app.tableBuilderComponent.ChangeCardOnTable(app.tableBuilderComponent.tablePanelsDraggables.IndexOf(this), _o, false);
            }
            else
            {
                app.tableBuilderComponent.ChangeCardOnTable(hoverTableCardPanelIndex, cardIndex, true);
            }
        }
        ResetLastSelection();
    }

    void ResetLastSelection()
    {
        if(hoverTableCardPanelIndex == -1) return;

        draggableImage.rectTransform.localScale = Vector3.one;
        hoverTableCardPanelIndex = -1;
    }
}
