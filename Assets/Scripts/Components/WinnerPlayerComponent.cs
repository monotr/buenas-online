﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WinnerPlayerComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    RectTransform movingPanelRectT;

    TMP_Text playerNameText;
    TMP_Text winCountText;
    RawImage photoImage;

    Vector2 initialPos = Vector3.zero;
    Vector2 finalPos = Vector2.up * 15;

    public void Init(GameObject obj, string _playerName, string _playerWins, Texture _playerPhoto, Color _imgColor)
    {
        movingPanelRectT = obj.transform.GetChild(0).GetComponent<RectTransform>();
        var texts = obj.GetComponentsInChildren<TMP_Text>();
        playerNameText = texts[0];
        playerNameText.text = _playerName;
        playerNameText.gameObject.SetActive(false);
        winCountText = texts[1];
        winCountText.text = _playerWins;

        photoImage = obj.GetComponentInChildren<RawImage>();
        photoImage.texture = _playerPhoto;
        photoImage.color = _imgColor;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        movingPanelRectT.localPosition = finalPos;
        playerNameText.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        movingPanelRectT.localPosition = initialPos;
        playerNameText.gameObject.SetActive(false);
    }
}
