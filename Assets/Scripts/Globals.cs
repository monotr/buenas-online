﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals
{
    public const string appVersion = "b2.2.20200724.00";

    public enum PromptType { ConfirmMessage, YesNoMessage }
    public enum PromptColors { Pink, Green, Blue, Red, Yellow }

    public enum NotificationType { CoinsIn, CoinsOut, PlayerIn, PlayerOut, Emojee, LevelUp, Experience, Rascada, RulesChange,
        FriendRequest, FriendRequestAccepted, TableInvitation };

    public enum PhotoDownloadType { LobbyPhoto, Ranking, GlobalChat, Friends, GameNotification, Profile }

    public enum PlayerSearchType { NameChange, FriendSearch }
    public enum ProfileOrigin { GlobalChat, Lobby, FriendsPanel, PlayerSearch, Rankings, WinnerPanel }

    [Serializable]
    public class Player
    {
        public string id;
        public string name;
        public string profileImageUrl;
        public int warnings;
        public Dictionary<string, object> friendList;
        public Dictionary<string, object> tablesDic;
        public string status;
        public int level;
        public float experience;
        public long coins;
        public Dictionary<string, object> coinsInput;
        public string lastPhotoUploadDateTime;
        public long lastDailyGiftDateTime;
        public long lastMinutesGiftDateTime;
        public long lastLoginDateTime;
        public long timestampCheck;
        public Dictionary<string, object> gameNotifications;
    }

    [Serializable]
    public class Friend
    {
        public string playerId;
        public long timestamp_Since;
        public bool isConfirmed;
    }

    [Serializable]
    public class GameNotification
    {
        public int type;
        public string originId;
        public string destinyId;
        public string content;
        public long timestamp;
    }

    [Serializable]
    public class PlayerStats
    {
        public long playedGames;
        public long wonGames;
        public long abandonedGame;
        public long totalCoinsEarned;
        public long maxStreak;
        public float totalExperience;
        public int level;
        public float hostTime;
    }

    [Serializable]
    public class CoinTransaction
    {
        public string id;
        public string originId;
        public string destinyId;
        public long quantity;
        public long timestamp;
    }

    [Serializable]
    public class ReceivedCoinTransaction
    {
        public string originId;
        public long quantity;
    }


    [Serializable]
    public class Deck
    {
        public string id;
        public string name;
        public List<Card> cardList;
    }

    [Serializable]
    public class Card
    {
        public string id;
        public string name;
        public string imageBase64;
    }

    [Serializable]
    public class Table
    {
        public string name;
        public bool isFavorite;
        public string tablePanels;
        public string dateTime;
        public int victories;
        public string groupIds;
        public string timestamp;
    }

    [Serializable]
    public class Room
    {
        public string id;
        public string playerHostId;
        public string name;
        public string password;
        public int maxPlayers;
        public float cantadaSpeed;
        public string victoryTypes;
        public long betPerTable;
        public string state;
        public int playerCount;
        public Dictionary<string, object> playersIdDict;
        public Dictionary<string, object> calleddCardsDic;
        public long accumulatedCoins;
        public LastWinner lastWinner;
        public Dictionary<string, object> winners;
    }

    [Serializable]
    public class Winner
    {
        public string playerId;
    }

    [Serializable]
    public class LastWinner
    {
        public string playerId;
        public int winStreak;
        public string emblemType;
    }

        [Serializable]
    public class PlayerInRoom
    {
        public string playerId;
        public int tablesQty;
        public List<int> checkedPanels;
        public float rankPoints;
        public bool isAway;
    }

    [Serializable]
    public class Buenas
    {
        public string playerId;
        public string winningTable;
        public string winningTableId;
        public int victoryIndex;
        public int victoryType;
        public string botName;
        public string accumulativeCoinId;
    }

    #region Image Moderation - SightEngine
    [Serializable]
    public class SightengineResponse
    {
        public string status;
        public Request request;
        public Scam scam;
        public Nudity nudity;
        public Offensive offensive;
    }

    [Serializable]
    public class Request
    {
        public string id;
        public double timestamp;
        public int operations;
    }

    [Serializable]
    public class Scam
    {
        public double prob;
    }

    [Serializable]
    public class Nudity
    {
        public double raw;
        public double safe;
        public double partial;
    }

    [Serializable]
    public class Offensive
    {
        public double prob;
    }
    #endregion

    [Serializable]
    public class Emojee
    {
        public string playerId;
        public int emojeeIndex;
    }

    [Serializable]
    public class Rascada
    {
        public string playerId;
        public int cardIndex;
    }

    [Serializable]
    public class RuleChanged
    {
        public string ruleType;
        public string val;
    }

    [Serializable]
    public class ChatMessage
    {
        public string playerId;
        public string playerName;
        public string message;
        public long timestamp;
    }

    [Serializable]
    public class PlayerNamesCollection
    {
        public List<string> allPlayerNames;
    }

    //{"foundPlayer":true,"searchName":"monotr_"}
    [Serializable]
    public class SearchName
    {
        public string searchName;
        public string playerId;
        public bool foundPlayer;
    }
}
