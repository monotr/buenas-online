﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class Tools : LoteriaOnlineElement
{
    public Texture2D TextureToTexture2D(Texture texture)
    {
        Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture renderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 32);
        Graphics.Blit(texture, renderTexture);

        RenderTexture.active = renderTexture;
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();

        RenderTexture.active = currentRT;
        RenderTexture.ReleaseTemporary(renderTexture);
        return texture2D;
    }

    public IEnumerator DownloadFirebasePhoto(string photoUrl, string playerId, Globals.PhotoDownloadType photoDownloadType)
    {
        if (!string.IsNullOrEmpty(photoUrl))
        {
            Texture2D _tex = null;

            UnityWebRequest www = UnityWebRequestTexture.GetTexture(photoUrl);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogError(www.error);
            }
            else
            {
                Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                _tex = TextureToTexture2D(myTexture);

                if (string.IsNullOrEmpty(playerId))
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(app.profileController.SetNewPhoto(_tex));
                }
                else
                {
                    switch (photoDownloadType)
                    {
                        case Globals.PhotoDownloadType.LobbyPhoto:
                            UnityMainThreadDispatcher.Instance().Enqueue(app.privateRoomLobbyComponent.SetPlayerPhoto(playerId, _tex));
                            break;
                        case Globals.PhotoDownloadType.Ranking:
                            UnityMainThreadDispatcher.Instance().Enqueue(app.rankingComponent.SetPlayerPhoto(playerId, _tex));
                            break;
                        case Globals.PhotoDownloadType.GlobalChat:
                            UnityMainThreadDispatcher.Instance().Enqueue(app.globalChatComponent.SetPlayerPhoto(playerId, _tex));
                            break;
                        case Globals.PhotoDownloadType.Friends:
                            UnityMainThreadDispatcher.Instance().Enqueue(app.friendsComponent.SetPlayerPhoto(playerId, _tex));
                            break;
                        case Globals.PhotoDownloadType.GameNotification:
                            UnityMainThreadDispatcher.Instance().Enqueue(app.gameNotificationsComponent.SetPlayerPhoto(playerId, _tex));
                            break;
                        case Globals.PhotoDownloadType.Profile:
                            UnityMainThreadDispatcher.Instance().Enqueue(app.globalChatComponent.SetOtherPlayerPhoto(playerId, _tex));
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    public float Round(float number)
    {
        if (number < 0)
            return Mathf.Ceil(number - 0.5f);
        else
            return Mathf.Floor(number + 0.5f);
    }

    public float NextLevel(int level)
    {
        //return Round(0.04f * (Mathf.Pow(level, 3)) + 0.4f * (Mathf.Pow(level, 2)) + 1 * level);
        //return Round(0.04f * (Mathf.Pow(level, 3)) + 0.8f * (Mathf.Pow(level, 2)) + 2 * level);
        return Round((4 * (Mathf.Pow(level, 3))) / 5);
    }

    public long ConvertToTimestamp(DateTime value)
    {
        long epoch = (value.Ticks - 621355968000000000) / 10000000;
        return epoch;
    }
}
